﻿
namespace RouteMaker
{
    class Ctes
    {
        public const string VERSION = "1.1.1";

        public static string COMMENTARIES_DIR()
        {
            return "input\\" + ProjectMngr.getInstance().getProjName() + "\\commentaries\\";
        }

        public static string MUSIC_DIR()
        {
            return "input\\" + ProjectMngr.getInstance().getProjName() + "\\music\\";
        }

        public static string CURRENT_PROJECT_FILE;

        public const string MARKERS_DIRECTION_OVERLAY = "markers_direction";
        public const string MARKERS_OVERLAY = "markers";
        public const string POLYGONS_OVERLAY = "polygons";
    }
}
