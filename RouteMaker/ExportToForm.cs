﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteMaker
{
    public partial class ExportToForm : Form
    {
        public ExportToForm()
        {
            InitializeComponent();
        }

        private void btnSelectUnit_Click(object sender, EventArgs e)
        {        
            if(folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                labelSelectedUnit.Text = folderBrowserDialog.SelectedPath;
            } 
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (labelSelectedUnit.Text == "")
            {
                MessageBox.Show("A unit must be selected");
                return;
            }

            if(comboBoxTarget.SelectedIndex == -1)
            {
                MessageBox.Show("A target must be selected");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            labelExporting.Text = "Exporting project...";

            int lSelectedIndex = comboBoxTarget.SelectedIndex;

            Thread lThExport = new Thread(() =>
            {
                string lProjName = ProjectMngr.getInstance().getProjName();

                switch(lSelectedIndex)
                {
                    case 0: // USB
                        Utils.CopyFolder(new DirectoryInfo("output\\" + lProjName + "\\USB"),
                                         new DirectoryInfo(labelSelectedUnit.Text));
                        break;
                    case 1: // CF ext
                        Utils.CopyFolder(new DirectoryInfo("output\\" + lProjName + "\\CF_ext"),
                                         new DirectoryInfo(labelSelectedUnit.Text));
                        break;
                    case 2: // CF 1
                        Utils.CopyFolder(new DirectoryInfo("output\\" + lProjName + "\\CF1"),
                                         new DirectoryInfo(labelSelectedUnit.Text));
                        break;
                    case 3: // CF 2
                        Utils.CopyFolder(new DirectoryInfo("output\\" + lProjName + "\\CF2"),
                                         new DirectoryInfo(labelSelectedUnit.Text));
                        break;
                    case 4: // MTG
                        exportMTG_files(labelSelectedUnit.Text);
                        break;
                }
            });
            lThExport.Start();
            lThExport.Join();

            Cursor.Current = Cursors.Default; // restore the cursor
            labelExporting.Text = string.Empty;

            MessageBox.Show("Export done");
            Close();
        }

        private void exportMTG_files(string aDestFolder)
        {
            string lProjName = ProjectMngr.getInstance().getProjName();

            // creating output comments dir
            Directory.CreateDirectory(aDestFolder + "\\commentaries");
            Directory.CreateDirectory(aDestFolder + "\\music");

            try
            {
                ProjectMngr.getInstance().populateMTG(aDestFolder + "\\");
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception merging wav files [" + e.Message + "]");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
