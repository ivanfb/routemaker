﻿namespace RouteMaker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.labOperatorName = new System.Windows.Forms.Label();
            this.TXTB_OperatorName = new System.Windows.Forms.TextBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.labTreeContent = new System.Windows.Forms.Label();
            this.btnSaveProject = new System.Windows.Forms.Button();
            this.btnLoadProject = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelProjectName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxAutoplay = new System.Windows.Forms.CheckBox();
            this.btnAddEloc = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxStopName = new System.Windows.Forms.TextBox();
            this.btnAddStop = new System.Windows.Forms.Button();
            this.btnAddMusictoStop = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxRouteName = new System.Windows.Forms.TextBox();
            this.btnAddRoute = new System.Windows.Forms.Button();
            this.btnAddMusicToRoute = new System.Windows.Forms.Button();
            this.numLanguages = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxElocutionName = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDown16 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown15 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown14 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown13 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown12 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown11 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown10 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.labNumChannel = new System.Windows.Forms.Label();
            this.labelNumCh1 = new System.Windows.Forms.Label();
            this.labelNumCh2 = new System.Windows.Forms.Label();
            this.labelNumCh3 = new System.Windows.Forms.Label();
            this.labelNumCh4 = new System.Windows.Forms.Label();
            this.labelNumCh5 = new System.Windows.Forms.Label();
            this.labelNumCh6 = new System.Windows.Forms.Label();
            this.labelNumCh7 = new System.Windows.Forms.Label();
            this.labelNumCh8 = new System.Windows.Forms.Label();
            this.labelNumCh9 = new System.Windows.Forms.Label();
            this.labelNumCh10 = new System.Windows.Forms.Label();
            this.labelNumCh11 = new System.Windows.Forms.Label();
            this.labelNumCh12 = new System.Windows.Forms.Label();
            this.labelNumCh13 = new System.Windows.Forms.Label();
            this.labelNumCh14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelNumCh15 = new System.Windows.Forms.Label();
            this.labelNumCh16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownMusicVol = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.cmbBoxModuleType = new System.Windows.Forms.ComboBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.numMTGChannels = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBoxMTGRightCh16 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh15 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh14 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh13 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh12 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh11 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh10 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh9 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh8 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh7 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh6 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh5 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh4 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh3 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGRightCh2 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh16 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh15 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh14 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh13 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh12 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh11 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh10 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh9 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh8 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh7 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh6 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh5 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh4 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh3 = new System.Windows.Forms.ComboBox();
            this.comboBoxMTGLeftCh2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelMTG_CH1 = new System.Windows.Forms.Label();
            this.labelMTG_CH2 = new System.Windows.Forms.Label();
            this.labelMTG_CH3 = new System.Windows.Forms.Label();
            this.labelMTG_CH4 = new System.Windows.Forms.Label();
            this.labelMTG_CH5 = new System.Windows.Forms.Label();
            this.labelMTG_CH6 = new System.Windows.Forms.Label();
            this.labelMTG_CH7 = new System.Windows.Forms.Label();
            this.labelMTG_CH8 = new System.Windows.Forms.Label();
            this.labelMTG_CH9 = new System.Windows.Forms.Label();
            this.labelMTG_CH10 = new System.Windows.Forms.Label();
            this.labelMTG_CH11 = new System.Windows.Forms.Label();
            this.labelMTG_CH12 = new System.Windows.Forms.Label();
            this.labelMTG_CH13 = new System.Windows.Forms.Label();
            this.labelMTG_CH14 = new System.Windows.Forms.Label();
            this.labelMTG_CH15 = new System.Windows.Forms.Label();
            this.labelMTG_CH16 = new System.Windows.Forms.Label();
            this.comboBoxMTGLeftCh1 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBoxMTGRightCh1 = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.NUD_MarginElocs = new System.Windows.Forms.NumericUpDown();
            this.LBL_MarginElocs = new System.Windows.Forms.Label();
            this.BTN_showRoute = new System.Windows.Forms.Button();
            this.BTN_DeletePoint = new System.Windows.Forms.Button();
            this.BTN_Go = new System.Windows.Forms.Button();
            this.BTN_AssignPosToCmt = new System.Windows.Forms.Button();
            this.TXTB_Location = new System.Windows.Forms.TextBox();
            this.LBL_Place = new System.Windows.Forms.Label();
            this.PanelPointInfo = new System.Windows.Forms.Panel();
            this.LBL_PointData = new System.Windows.Forms.Label();
            this.MC_map = new GMap.NET.WindowsForms.GMapControl();
            this.labelSelectedStop = new System.Windows.Forms.Label();
            this.labelSelectedRoute = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveTop = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveBottom = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editElocutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoplayAutostopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnNewProject = new System.Windows.Forms.Button();
            this.btnExportTo = new System.Windows.Forms.Button();
            this.btnReload = new System.Windows.Forms.Button();
            this.btnFormat = new System.Windows.Forms.Button();
            this.CMS_MapMarkers = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.selectedAngleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.angleErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CB_AngleError = new System.Windows.Forms.ToolStripComboBox();
            this.radiousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CB_Radius = new System.Windows.Forms.ToolStripComboBox();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxCurrentRouteStopComment = new System.Windows.Forms.GroupBox();
            this.labelRoute = new System.Windows.Forms.Label();
            this.labelComment = new System.Windows.Forms.Label();
            this.labelSelectedComment = new System.Windows.Forms.Label();
            this.labelStop = new System.Windows.Forms.Label();
            this.BTN_ImportInfs = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.BTN_ImportGpsInf = new System.Windows.Forms.Button();
            this.toolTipExpandCollapseButtons = new System.Windows.Forms.ToolTip(this.components);
            this.BTN_CollapseAll = new System.Windows.Forms.Button();
            this.BTN_ExpandAll = new System.Windows.Forms.Button();
            this.labelCopySavingStatus = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLanguages)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMusicVol)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMTGChannels)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_MarginElocs)).BeginInit();
            this.PanelPointInfo.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.CMS_MapMarkers.SuspendLayout();
            this.groupBoxCurrentRouteStopComment.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // labOperatorName
            // 
            this.labOperatorName.AutoSize = true;
            this.labOperatorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labOperatorName.Location = new System.Drawing.Point(3, 38);
            this.labOperatorName.Name = "labOperatorName";
            this.labOperatorName.Size = new System.Drawing.Size(101, 16);
            this.labOperatorName.TabIndex = 0;
            this.labOperatorName.Text = "Operator Name";
            // 
            // TXTB_OperatorName
            // 
            this.TXTB_OperatorName.Location = new System.Drawing.Point(136, 41);
            this.TXTB_OperatorName.Name = "TXTB_OperatorName";
            this.TXTB_OperatorName.Size = new System.Drawing.Size(209, 20);
            this.TXTB_OperatorName.TabIndex = 1;
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView.LabelEdit = true;
            this.treeView.Location = new System.Drawing.Point(12, 34);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(272, 457);
            this.treeView.TabIndex = 2;
            this.treeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.treeView_AfterLabelEdit);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseClick);
            // 
            // labTreeContent
            // 
            this.labTreeContent.AutoSize = true;
            this.labTreeContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTreeContent.Location = new System.Drawing.Point(9, 12);
            this.labTreeContent.Name = "labTreeContent";
            this.labTreeContent.Size = new System.Drawing.Size(51, 16);
            this.labTreeContent.TabIndex = 3;
            this.labTreeContent.Text = "Routes";
            // 
            // btnSaveProject
            // 
            this.btnSaveProject.Location = new System.Drawing.Point(604, 3);
            this.btnSaveProject.Name = "btnSaveProject";
            this.btnSaveProject.Size = new System.Drawing.Size(66, 23);
            this.btnSaveProject.TabIndex = 6;
            this.btnSaveProject.Text = "Save";
            this.btnSaveProject.UseVisualStyleBackColor = true;
            this.btnSaveProject.Click += new System.EventHandler(this.btnSaveProject_Click);
            // 
            // btnLoadProject
            // 
            this.btnLoadProject.Location = new System.Drawing.Point(518, 3);
            this.btnLoadProject.Name = "btnLoadProject";
            this.btnLoadProject.Size = new System.Drawing.Size(75, 23);
            this.btnLoadProject.TabIndex = 7;
            this.btnLoadProject.Text = "Load Project";
            this.btnLoadProject.UseVisualStyleBackColor = true;
            this.btnLoadProject.Click += new System.EventHandler(this.btnLoadProject_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(690, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(290, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1161, 605);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1153, 579);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Route Edition";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.35227F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.64773F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 142F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 341F));
            this.tableLayoutPanel1.Controls.Add(this.labelProjectName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxAutoplay, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnAddEloc, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxStopName, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnAddStop, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnAddMusictoStop, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxRouteName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnAddRoute, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnAddMusicToRoute, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.numLanguages, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label23, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.TXTB_OperatorName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labOperatorName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxElocutionName, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 49);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(832, 193);
            this.tableLayoutPanel1.TabIndex = 23;
            // 
            // labelProjectName
            // 
            this.labelProjectName.AutoSize = true;
            this.labelProjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProjectName.Location = new System.Drawing.Point(136, 0);
            this.labelProjectName.Name = "labelProjectName";
            this.labelProjectName.Size = new System.Drawing.Size(0, 16);
            this.labelProjectName.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 16);
            this.label4.TabIndex = 27;
            this.label4.Text = "Project Name";
            // 
            // checkBoxAutoplay
            // 
            this.checkBoxAutoplay.AutoSize = true;
            this.checkBoxAutoplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAutoplay.Location = new System.Drawing.Point(351, 155);
            this.checkBoxAutoplay.Name = "checkBoxAutoplay";
            this.checkBoxAutoplay.Size = new System.Drawing.Size(80, 20);
            this.checkBoxAutoplay.TabIndex = 17;
            this.checkBoxAutoplay.Text = "Autoplay";
            this.checkBoxAutoplay.UseVisualStyleBackColor = true;
            // 
            // btnAddEloc
            // 
            this.btnAddEloc.Location = new System.Drawing.Point(493, 155);
            this.btnAddEloc.Name = "btnAddEloc";
            this.btnAddEloc.Size = new System.Drawing.Size(133, 23);
            this.btnAddEloc.TabIndex = 16;
            this.btnAddEloc.Text = "Add commentary file";
            this.btnAddEloc.UseVisualStyleBackColor = true;
            this.btnAddEloc.Click += new System.EventHandler(this.btnAddEloc_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 16);
            this.label20.TabIndex = 9;
            this.label20.Text = "Stop Name";
            // 
            // textBoxStopName
            // 
            this.textBoxStopName.Location = new System.Drawing.Point(136, 117);
            this.textBoxStopName.Name = "textBoxStopName";
            this.textBoxStopName.Size = new System.Drawing.Size(209, 20);
            this.textBoxStopName.TabIndex = 10;
            // 
            // btnAddStop
            // 
            this.btnAddStop.Location = new System.Drawing.Point(351, 117);
            this.btnAddStop.Name = "btnAddStop";
            this.btnAddStop.Size = new System.Drawing.Size(97, 21);
            this.btnAddStop.TabIndex = 11;
            this.btnAddStop.Text = "Add stop/group";
            this.btnAddStop.UseVisualStyleBackColor = true;
            this.btnAddStop.Click += new System.EventHandler(this.btnAddStop_Click);
            // 
            // btnAddMusictoStop
            // 
            this.btnAddMusictoStop.Location = new System.Drawing.Point(493, 117);
            this.btnAddMusictoStop.Name = "btnAddMusictoStop";
            this.btnAddMusictoStop.Size = new System.Drawing.Size(174, 23);
            this.btnAddMusictoStop.TabIndex = 12;
            this.btnAddMusictoStop.Text = "Add background music to stop";
            this.btnAddMusictoStop.UseVisualStyleBackColor = true;
            this.btnAddMusictoStop.Click += new System.EventHandler(this.btnAddMusictoStop_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 16);
            this.label19.TabIndex = 6;
            this.label19.Text = "Route Name";
            // 
            // textBoxRouteName
            // 
            this.textBoxRouteName.Location = new System.Drawing.Point(136, 79);
            this.textBoxRouteName.Name = "textBoxRouteName";
            this.textBoxRouteName.Size = new System.Drawing.Size(209, 20);
            this.textBoxRouteName.TabIndex = 7;
            // 
            // btnAddRoute
            // 
            this.btnAddRoute.Location = new System.Drawing.Point(351, 79);
            this.btnAddRoute.Name = "btnAddRoute";
            this.btnAddRoute.Size = new System.Drawing.Size(97, 20);
            this.btnAddRoute.TabIndex = 8;
            this.btnAddRoute.Text = "Add route";
            this.btnAddRoute.UseVisualStyleBackColor = true;
            this.btnAddRoute.Click += new System.EventHandler(this.btnAddRoute_Click);
            // 
            // btnAddMusicToRoute
            // 
            this.btnAddMusicToRoute.Location = new System.Drawing.Point(493, 79);
            this.btnAddMusicToRoute.Name = "btnAddMusicToRoute";
            this.btnAddMusicToRoute.Size = new System.Drawing.Size(174, 23);
            this.btnAddMusicToRoute.TabIndex = 22;
            this.btnAddMusicToRoute.Text = "Add background music to route";
            this.btnAddMusicToRoute.UseVisualStyleBackColor = true;
            this.btnAddMusicToRoute.Click += new System.EventHandler(this.btnAddMusicToRoute_Click);
            // 
            // numLanguages
            // 
            this.numLanguages.Location = new System.Drawing.Point(493, 41);
            this.numLanguages.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numLanguages.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numLanguages.Name = "numLanguages";
            this.numLanguages.Size = new System.Drawing.Size(51, 20);
            this.numLanguages.TabIndex = 18;
            this.numLanguages.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numLanguages.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numLanguages.ValueChanged += new System.EventHandler(this.numLanguages_ValueChanged);
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(384, 38);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(103, 16);
            this.label23.TabIndex = 19;
            this.label23.Text = "Num languages";
            // 
            // textBoxElocutionName
            // 
            this.textBoxElocutionName.Location = new System.Drawing.Point(136, 155);
            this.textBoxElocutionName.Name = "textBoxElocutionName";
            this.textBoxElocutionName.Size = new System.Drawing.Size(209, 20);
            this.textBoxElocutionName.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 152);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(124, 16);
            this.label21.TabIndex = 13;
            this.label21.Text = "Commentary Name";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1153, 579);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "MTP";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.cmbBoxModuleType);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(653, 507);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Channel configuration";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.11844F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.54822F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown16, 2, 16);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown15, 2, 15);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown14, 2, 14);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown13, 2, 13);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown12, 2, 12);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown11, 2, 11);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown10, 2, 10);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown9, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown8, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown7, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown6, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown5, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown4, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown3, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown2, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.comboBox16, 1, 16);
            this.tableLayoutPanel2.Controls.Add(this.comboBox15, 1, 15);
            this.tableLayoutPanel2.Controls.Add(this.comboBox14, 1, 14);
            this.tableLayoutPanel2.Controls.Add(this.comboBox13, 1, 13);
            this.tableLayoutPanel2.Controls.Add(this.comboBox12, 1, 12);
            this.tableLayoutPanel2.Controls.Add(this.comboBox11, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.comboBox10, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.comboBox9, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.comboBox8, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.comboBox7, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.comboBox6, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.comboBox5, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.comboBox4, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.comboBox3, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.comboBox2, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.labNumChannel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh4, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh5, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh6, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh7, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh8, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh9, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh10, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh11, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh12, 0, 12);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh13, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh14, 0, 14);
            this.tableLayoutPanel2.Controls.Add(this.label18, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh15, 0, 15);
            this.tableLayoutPanel2.Controls.Add(this.labelNumCh16, 0, 16);
            this.tableLayoutPanel2.Controls.Add(this.label17, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.comboBox1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDownMusicVol, 3, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 17;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(640, 455);
            this.tableLayoutPanel2.TabIndex = 56;
            // 
            // numericUpDown16
            // 
            this.numericUpDown16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown16.Location = new System.Drawing.Point(293, 428);
            this.numericUpDown16.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown16.Name = "numericUpDown16";
            this.numericUpDown16.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown16.TabIndex = 86;
            this.numericUpDown16.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown15
            // 
            this.numericUpDown15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown15.Location = new System.Drawing.Point(293, 397);
            this.numericUpDown15.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown15.Name = "numericUpDown15";
            this.numericUpDown15.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown15.TabIndex = 85;
            this.numericUpDown15.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown14
            // 
            this.numericUpDown14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown14.Location = new System.Drawing.Point(293, 370);
            this.numericUpDown14.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown14.Name = "numericUpDown14";
            this.numericUpDown14.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown14.TabIndex = 84;
            this.numericUpDown14.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown13
            // 
            this.numericUpDown13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown13.Location = new System.Drawing.Point(293, 343);
            this.numericUpDown13.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown13.Name = "numericUpDown13";
            this.numericUpDown13.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown13.TabIndex = 83;
            this.numericUpDown13.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown12
            // 
            this.numericUpDown12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown12.Location = new System.Drawing.Point(293, 316);
            this.numericUpDown12.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown12.Name = "numericUpDown12";
            this.numericUpDown12.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown12.TabIndex = 82;
            this.numericUpDown12.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown11
            // 
            this.numericUpDown11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown11.Location = new System.Drawing.Point(293, 289);
            this.numericUpDown11.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown11.Name = "numericUpDown11";
            this.numericUpDown11.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown11.TabIndex = 81;
            this.numericUpDown11.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown10
            // 
            this.numericUpDown10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown10.Location = new System.Drawing.Point(293, 262);
            this.numericUpDown10.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown10.Name = "numericUpDown10";
            this.numericUpDown10.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown10.TabIndex = 80;
            this.numericUpDown10.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown9.Location = new System.Drawing.Point(293, 235);
            this.numericUpDown9.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown9.TabIndex = 79;
            this.numericUpDown9.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown8.Location = new System.Drawing.Point(293, 208);
            this.numericUpDown8.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown8.TabIndex = 78;
            this.numericUpDown8.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown7.Location = new System.Drawing.Point(293, 181);
            this.numericUpDown7.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown7.TabIndex = 77;
            this.numericUpDown7.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown6.Location = new System.Drawing.Point(293, 154);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown6.TabIndex = 76;
            this.numericUpDown6.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown5.Location = new System.Drawing.Point(293, 127);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown5.TabIndex = 75;
            this.numericUpDown5.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown4.Location = new System.Drawing.Point(293, 100);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown4.TabIndex = 74;
            this.numericUpDown4.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown3.Location = new System.Drawing.Point(293, 73);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown3.TabIndex = 73;
            this.numericUpDown3.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown2.Location = new System.Drawing.Point(293, 46);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown2.TabIndex = 72;
            this.numericUpDown2.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // comboBox16
            // 
            this.comboBox16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Location = new System.Drawing.Point(131, 424);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(149, 21);
            this.comboBox16.TabIndex = 70;
            // 
            // comboBox15
            // 
            this.comboBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Location = new System.Drawing.Point(131, 397);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(149, 21);
            this.comboBox15.TabIndex = 69;
            // 
            // comboBox14
            // 
            this.comboBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Location = new System.Drawing.Point(131, 370);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(149, 21);
            this.comboBox14.TabIndex = 68;
            // 
            // comboBox13
            // 
            this.comboBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Location = new System.Drawing.Point(131, 343);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(149, 21);
            this.comboBox13.TabIndex = 67;
            // 
            // comboBox12
            // 
            this.comboBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(131, 316);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(149, 21);
            this.comboBox12.TabIndex = 66;
            // 
            // comboBox11
            // 
            this.comboBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(131, 289);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(149, 21);
            this.comboBox11.TabIndex = 65;
            // 
            // comboBox10
            // 
            this.comboBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(131, 262);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(149, 21);
            this.comboBox10.TabIndex = 64;
            // 
            // comboBox9
            // 
            this.comboBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(131, 235);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(149, 21);
            this.comboBox9.TabIndex = 63;
            // 
            // comboBox8
            // 
            this.comboBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(131, 208);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(149, 21);
            this.comboBox8.TabIndex = 62;
            // 
            // comboBox7
            // 
            this.comboBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(131, 181);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(149, 21);
            this.comboBox7.TabIndex = 61;
            // 
            // comboBox6
            // 
            this.comboBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(131, 154);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(149, 21);
            this.comboBox6.TabIndex = 60;
            // 
            // comboBox5
            // 
            this.comboBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(131, 127);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(149, 21);
            this.comboBox5.TabIndex = 59;
            // 
            // comboBox4
            // 
            this.comboBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(131, 100);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(149, 21);
            this.comboBox4.TabIndex = 58;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(131, 73);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(149, 21);
            this.comboBox3.TabIndex = 57;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(131, 46);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(149, 21);
            this.comboBox2.TabIndex = 56;
            // 
            // labNumChannel
            // 
            this.labNumChannel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labNumChannel.AutoSize = true;
            this.labNumChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNumChannel.Location = new System.Drawing.Point(10, 0);
            this.labNumChannel.Name = "labNumChannel";
            this.labNumChannel.Size = new System.Drawing.Size(108, 16);
            this.labNumChannel.TabIndex = 4;
            this.labNumChannel.Text = "Channel Number";
            // 
            // labelNumCh1
            // 
            this.labelNumCh1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh1.AutoSize = true;
            this.labelNumCh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh1.Location = new System.Drawing.Point(56, 21);
            this.labelNumCh1.Name = "labelNumCh1";
            this.labelNumCh1.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh1.TabIndex = 5;
            this.labelNumCh1.Text = "1";
            // 
            // labelNumCh2
            // 
            this.labelNumCh2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh2.AutoSize = true;
            this.labelNumCh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh2.Location = new System.Drawing.Point(56, 48);
            this.labelNumCh2.Name = "labelNumCh2";
            this.labelNumCh2.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh2.TabIndex = 8;
            this.labelNumCh2.Text = "2";
            // 
            // labelNumCh3
            // 
            this.labelNumCh3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh3.AutoSize = true;
            this.labelNumCh3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh3.Location = new System.Drawing.Point(56, 75);
            this.labelNumCh3.Name = "labelNumCh3";
            this.labelNumCh3.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh3.TabIndex = 9;
            this.labelNumCh3.Text = "3";
            // 
            // labelNumCh4
            // 
            this.labelNumCh4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh4.AutoSize = true;
            this.labelNumCh4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh4.Location = new System.Drawing.Point(56, 102);
            this.labelNumCh4.Name = "labelNumCh4";
            this.labelNumCh4.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh4.TabIndex = 7;
            this.labelNumCh4.Text = "4";
            // 
            // labelNumCh5
            // 
            this.labelNumCh5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh5.AutoSize = true;
            this.labelNumCh5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh5.Location = new System.Drawing.Point(56, 129);
            this.labelNumCh5.Name = "labelNumCh5";
            this.labelNumCh5.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh5.TabIndex = 16;
            this.labelNumCh5.Text = "5";
            // 
            // labelNumCh6
            // 
            this.labelNumCh6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh6.AutoSize = true;
            this.labelNumCh6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh6.Location = new System.Drawing.Point(56, 156);
            this.labelNumCh6.Name = "labelNumCh6";
            this.labelNumCh6.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh6.TabIndex = 18;
            this.labelNumCh6.Text = "6";
            // 
            // labelNumCh7
            // 
            this.labelNumCh7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh7.AutoSize = true;
            this.labelNumCh7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh7.Location = new System.Drawing.Point(56, 183);
            this.labelNumCh7.Name = "labelNumCh7";
            this.labelNumCh7.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh7.TabIndex = 10;
            this.labelNumCh7.Text = "7";
            // 
            // labelNumCh8
            // 
            this.labelNumCh8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh8.AutoSize = true;
            this.labelNumCh8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh8.Location = new System.Drawing.Point(56, 210);
            this.labelNumCh8.Name = "labelNumCh8";
            this.labelNumCh8.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh8.TabIndex = 12;
            this.labelNumCh8.Text = "8";
            // 
            // labelNumCh9
            // 
            this.labelNumCh9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh9.AutoSize = true;
            this.labelNumCh9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh9.Location = new System.Drawing.Point(56, 237);
            this.labelNumCh9.Name = "labelNumCh9";
            this.labelNumCh9.Size = new System.Drawing.Size(15, 16);
            this.labelNumCh9.TabIndex = 13;
            this.labelNumCh9.Text = "9";
            // 
            // labelNumCh10
            // 
            this.labelNumCh10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh10.AutoSize = true;
            this.labelNumCh10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh10.Location = new System.Drawing.Point(53, 264);
            this.labelNumCh10.Name = "labelNumCh10";
            this.labelNumCh10.Size = new System.Drawing.Size(22, 16);
            this.labelNumCh10.TabIndex = 15;
            this.labelNumCh10.Text = "10";
            // 
            // labelNumCh11
            // 
            this.labelNumCh11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh11.AutoSize = true;
            this.labelNumCh11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh11.Location = new System.Drawing.Point(53, 291);
            this.labelNumCh11.Name = "labelNumCh11";
            this.labelNumCh11.Size = new System.Drawing.Size(22, 16);
            this.labelNumCh11.TabIndex = 17;
            this.labelNumCh11.Text = "11";
            // 
            // labelNumCh12
            // 
            this.labelNumCh12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh12.AutoSize = true;
            this.labelNumCh12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh12.Location = new System.Drawing.Point(53, 318);
            this.labelNumCh12.Name = "labelNumCh12";
            this.labelNumCh12.Size = new System.Drawing.Size(22, 16);
            this.labelNumCh12.TabIndex = 14;
            this.labelNumCh12.Text = "12";
            // 
            // labelNumCh13
            // 
            this.labelNumCh13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh13.AutoSize = true;
            this.labelNumCh13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh13.Location = new System.Drawing.Point(53, 345);
            this.labelNumCh13.Name = "labelNumCh13";
            this.labelNumCh13.Size = new System.Drawing.Size(22, 16);
            this.labelNumCh13.TabIndex = 19;
            this.labelNumCh13.Text = "13";
            // 
            // labelNumCh14
            // 
            this.labelNumCh14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh14.AutoSize = true;
            this.labelNumCh14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh14.Location = new System.Drawing.Point(53, 372);
            this.labelNumCh14.Name = "labelNumCh14";
            this.labelNumCh14.Size = new System.Drawing.Size(22, 16);
            this.labelNumCh14.TabIndex = 11;
            this.labelNumCh14.Text = "14";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(300, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 16);
            this.label18.TabIndex = 38;
            this.label18.Text = "Channel Volume";
            // 
            // labelNumCh15
            // 
            this.labelNumCh15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh15.AutoSize = true;
            this.labelNumCh15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh15.Location = new System.Drawing.Point(53, 399);
            this.labelNumCh15.Name = "labelNumCh15";
            this.labelNumCh15.Size = new System.Drawing.Size(22, 16);
            this.labelNumCh15.TabIndex = 6;
            this.labelNumCh15.Text = "15";
            // 
            // labelNumCh16
            // 
            this.labelNumCh16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumCh16.AutoSize = true;
            this.labelNumCh16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumCh16.Location = new System.Drawing.Point(53, 430);
            this.labelNumCh16.Name = "labelNumCh16";
            this.labelNumCh16.Size = new System.Drawing.Size(22, 16);
            this.labelNumCh16.TabIndex = 20;
            this.labelNumCh16.Text = "16";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(171, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 16);
            this.label17.TabIndex = 21;
            this.label17.Text = "Language";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(131, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(149, 21);
            this.comboBox1.TabIndex = 55;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown1.Location = new System.Drawing.Point(293, 19);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 71;
            this.numericUpDown1.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(486, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 16);
            this.label1.TabIndex = 87;
            this.label1.Text = "Music Volume";
            // 
            // numericUpDownMusicVol
            // 
            this.numericUpDownMusicVol.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownMusicVol.Location = new System.Drawing.Point(472, 19);
            this.numericUpDownMusicVol.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDownMusicVol.Name = "numericUpDownMusicVol";
            this.numericUpDownMusicVol.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownMusicVol.TabIndex = 88;
            this.numericUpDownMusicVol.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownMusicVol.ValueChanged += new System.EventHandler(this.numericUpDownMusicVol_ValueChanged);
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(277, 481);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 16);
            this.label22.TabIndex = 55;
            this.label22.Text = "System Model";
            // 
            // cmbBoxModuleType
            // 
            this.cmbBoxModuleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxModuleType.FormattingEnabled = true;
            this.cmbBoxModuleType.Items.AddRange(new object[] {
            "MTP08CM8B",
            "MTP08CM16B",
            "MTP08CM16C",
            "MTP08CM16",
            "MTP08CM4/MTP08CM6/MTP08CM8",
            "MTP08TE/MTP08TEX",
            "MTP08TL/MTP08T/MTP08TX"});
            this.cmbBoxModuleType.Location = new System.Drawing.Point(377, 480);
            this.cmbBoxModuleType.Name = "cmbBoxModuleType";
            this.cmbBoxModuleType.Size = new System.Drawing.Size(270, 21);
            this.cmbBoxModuleType.TabIndex = 1;
            this.cmbBoxModuleType.SelectedIndexChanged += new System.EventHandler(this.cmbBoxModuleType_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel5);
            this.tabPage4.Controls.Add(this.tableLayoutPanel3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1153, 579);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "MTG";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.87331F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.1267F));
            this.tableLayoutPanel5.Controls.Add(this.numMTGChannels, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(18, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(211, 24);
            this.tableLayoutPanel5.TabIndex = 59;
            // 
            // numMTGChannels
            // 
            this.numMTGChannels.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numMTGChannels.Location = new System.Drawing.Point(146, 3);
            this.numMTGChannels.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numMTGChannels.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numMTGChannels.Name = "numMTGChannels";
            this.numMTGChannels.Size = new System.Drawing.Size(51, 20);
            this.numMTGChannels.TabIndex = 58;
            this.numMTGChannels.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numMTGChannels.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numMTGChannels.ValueChanged += new System.EventHandler(this.numMTGChannels_ValueChanged);
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(3, 4);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(128, 16);
            this.label28.TabIndex = 57;
            this.label28.Text = "Num MTG Channels";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.65625F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.59375F));
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh16, 2, 16);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh15, 2, 15);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh14, 2, 14);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh13, 2, 13);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh12, 2, 12);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh11, 2, 11);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh10, 2, 10);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh9, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh8, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh7, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh6, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh5, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh4, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh3, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh16, 1, 16);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh15, 1, 15);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh14, 1, 14);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh13, 1, 13);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh12, 1, 12);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh11, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh10, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh9, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh8, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh7, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh6, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh5, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh4, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh3, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh2, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH3, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH4, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH5, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH6, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH7, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH8, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH9, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH10, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH11, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH12, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH13, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH14, 0, 14);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH15, 0, 15);
            this.tableLayoutPanel3.Controls.Add(this.labelMTG_CH16, 0, 16);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGLeftCh1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label25, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label29, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxMTGRightCh1, 2, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(18, 63);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 17;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(640, 455);
            this.tableLayoutPanel3.TabIndex = 56;
            // 
            // comboBoxMTGRightCh16
            // 
            this.comboBoxMTGRightCh16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh16.FormattingEnabled = true;
            this.comboBoxMTGRightCh16.Location = new System.Drawing.Point(383, 424);
            this.comboBoxMTGRightCh16.Name = "comboBoxMTGRightCh16";
            this.comboBoxMTGRightCh16.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh16.TabIndex = 103;
            this.comboBoxMTGRightCh16.Tag = "15";
            this.comboBoxMTGRightCh16.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh15
            // 
            this.comboBoxMTGRightCh15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh15.FormattingEnabled = true;
            this.comboBoxMTGRightCh15.Location = new System.Drawing.Point(383, 397);
            this.comboBoxMTGRightCh15.Name = "comboBoxMTGRightCh15";
            this.comboBoxMTGRightCh15.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh15.TabIndex = 102;
            this.comboBoxMTGRightCh15.Tag = "14";
            this.comboBoxMTGRightCh15.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh14
            // 
            this.comboBoxMTGRightCh14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh14.FormattingEnabled = true;
            this.comboBoxMTGRightCh14.Location = new System.Drawing.Point(383, 370);
            this.comboBoxMTGRightCh14.Name = "comboBoxMTGRightCh14";
            this.comboBoxMTGRightCh14.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh14.TabIndex = 101;
            this.comboBoxMTGRightCh14.Tag = "13";
            this.comboBoxMTGRightCh14.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh13
            // 
            this.comboBoxMTGRightCh13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh13.FormattingEnabled = true;
            this.comboBoxMTGRightCh13.Location = new System.Drawing.Point(383, 343);
            this.comboBoxMTGRightCh13.Name = "comboBoxMTGRightCh13";
            this.comboBoxMTGRightCh13.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh13.TabIndex = 100;
            this.comboBoxMTGRightCh13.Tag = "12";
            this.comboBoxMTGRightCh13.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh12
            // 
            this.comboBoxMTGRightCh12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh12.FormattingEnabled = true;
            this.comboBoxMTGRightCh12.Location = new System.Drawing.Point(383, 316);
            this.comboBoxMTGRightCh12.Name = "comboBoxMTGRightCh12";
            this.comboBoxMTGRightCh12.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh12.TabIndex = 99;
            this.comboBoxMTGRightCh12.Tag = "11";
            this.comboBoxMTGRightCh12.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh11
            // 
            this.comboBoxMTGRightCh11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh11.FormattingEnabled = true;
            this.comboBoxMTGRightCh11.Location = new System.Drawing.Point(383, 289);
            this.comboBoxMTGRightCh11.Name = "comboBoxMTGRightCh11";
            this.comboBoxMTGRightCh11.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh11.TabIndex = 98;
            this.comboBoxMTGRightCh11.Tag = "10";
            this.comboBoxMTGRightCh11.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh10
            // 
            this.comboBoxMTGRightCh10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh10.FormattingEnabled = true;
            this.comboBoxMTGRightCh10.Location = new System.Drawing.Point(383, 262);
            this.comboBoxMTGRightCh10.Name = "comboBoxMTGRightCh10";
            this.comboBoxMTGRightCh10.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh10.TabIndex = 97;
            this.comboBoxMTGRightCh10.Tag = "9";
            this.comboBoxMTGRightCh10.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh9
            // 
            this.comboBoxMTGRightCh9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh9.FormattingEnabled = true;
            this.comboBoxMTGRightCh9.Location = new System.Drawing.Point(383, 235);
            this.comboBoxMTGRightCh9.Name = "comboBoxMTGRightCh9";
            this.comboBoxMTGRightCh9.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh9.TabIndex = 96;
            this.comboBoxMTGRightCh9.Tag = "8";
            this.comboBoxMTGRightCh9.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh8
            // 
            this.comboBoxMTGRightCh8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh8.FormattingEnabled = true;
            this.comboBoxMTGRightCh8.Location = new System.Drawing.Point(383, 208);
            this.comboBoxMTGRightCh8.Name = "comboBoxMTGRightCh8";
            this.comboBoxMTGRightCh8.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh8.TabIndex = 95;
            this.comboBoxMTGRightCh8.Tag = "7";
            this.comboBoxMTGRightCh8.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh7
            // 
            this.comboBoxMTGRightCh7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh7.FormattingEnabled = true;
            this.comboBoxMTGRightCh7.Location = new System.Drawing.Point(383, 181);
            this.comboBoxMTGRightCh7.Name = "comboBoxMTGRightCh7";
            this.comboBoxMTGRightCh7.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh7.TabIndex = 94;
            this.comboBoxMTGRightCh7.Tag = "6";
            this.comboBoxMTGRightCh7.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh6
            // 
            this.comboBoxMTGRightCh6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh6.FormattingEnabled = true;
            this.comboBoxMTGRightCh6.Location = new System.Drawing.Point(383, 154);
            this.comboBoxMTGRightCh6.Name = "comboBoxMTGRightCh6";
            this.comboBoxMTGRightCh6.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh6.TabIndex = 93;
            this.comboBoxMTGRightCh6.Tag = "5";
            this.comboBoxMTGRightCh6.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh5
            // 
            this.comboBoxMTGRightCh5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh5.FormattingEnabled = true;
            this.comboBoxMTGRightCh5.Location = new System.Drawing.Point(383, 127);
            this.comboBoxMTGRightCh5.Name = "comboBoxMTGRightCh5";
            this.comboBoxMTGRightCh5.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh5.TabIndex = 92;
            this.comboBoxMTGRightCh5.Tag = "4";
            this.comboBoxMTGRightCh5.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh4
            // 
            this.comboBoxMTGRightCh4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh4.FormattingEnabled = true;
            this.comboBoxMTGRightCh4.Location = new System.Drawing.Point(383, 100);
            this.comboBoxMTGRightCh4.Name = "comboBoxMTGRightCh4";
            this.comboBoxMTGRightCh4.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh4.TabIndex = 91;
            this.comboBoxMTGRightCh4.Tag = "3";
            this.comboBoxMTGRightCh4.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh3
            // 
            this.comboBoxMTGRightCh3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh3.FormattingEnabled = true;
            this.comboBoxMTGRightCh3.Location = new System.Drawing.Point(383, 73);
            this.comboBoxMTGRightCh3.Name = "comboBoxMTGRightCh3";
            this.comboBoxMTGRightCh3.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh3.TabIndex = 90;
            this.comboBoxMTGRightCh3.Tag = "2";
            this.comboBoxMTGRightCh3.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGRightCh2
            // 
            this.comboBoxMTGRightCh2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh2.FormattingEnabled = true;
            this.comboBoxMTGRightCh2.Location = new System.Drawing.Point(383, 46);
            this.comboBoxMTGRightCh2.Name = "comboBoxMTGRightCh2";
            this.comboBoxMTGRightCh2.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh2.TabIndex = 89;
            this.comboBoxMTGRightCh2.Tag = "1";
            this.comboBoxMTGRightCh2.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // comboBoxMTGLeftCh16
            // 
            this.comboBoxMTGLeftCh16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh16.FormattingEnabled = true;
            this.comboBoxMTGLeftCh16.Location = new System.Drawing.Point(123, 424);
            this.comboBoxMTGLeftCh16.Name = "comboBoxMTGLeftCh16";
            this.comboBoxMTGLeftCh16.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh16.TabIndex = 70;
            this.comboBoxMTGLeftCh16.Tag = "15";
            this.comboBoxMTGLeftCh16.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh15
            // 
            this.comboBoxMTGLeftCh15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh15.FormattingEnabled = true;
            this.comboBoxMTGLeftCh15.Location = new System.Drawing.Point(123, 397);
            this.comboBoxMTGLeftCh15.Name = "comboBoxMTGLeftCh15";
            this.comboBoxMTGLeftCh15.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh15.TabIndex = 69;
            this.comboBoxMTGLeftCh15.Tag = "14";
            this.comboBoxMTGLeftCh15.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh14
            // 
            this.comboBoxMTGLeftCh14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh14.FormattingEnabled = true;
            this.comboBoxMTGLeftCh14.Location = new System.Drawing.Point(123, 370);
            this.comboBoxMTGLeftCh14.Name = "comboBoxMTGLeftCh14";
            this.comboBoxMTGLeftCh14.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh14.TabIndex = 68;
            this.comboBoxMTGLeftCh14.Tag = "13";
            this.comboBoxMTGLeftCh14.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh13
            // 
            this.comboBoxMTGLeftCh13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh13.FormattingEnabled = true;
            this.comboBoxMTGLeftCh13.Location = new System.Drawing.Point(123, 343);
            this.comboBoxMTGLeftCh13.Name = "comboBoxMTGLeftCh13";
            this.comboBoxMTGLeftCh13.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh13.TabIndex = 67;
            this.comboBoxMTGLeftCh13.Tag = "12";
            this.comboBoxMTGLeftCh13.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh12
            // 
            this.comboBoxMTGLeftCh12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh12.FormattingEnabled = true;
            this.comboBoxMTGLeftCh12.Location = new System.Drawing.Point(123, 316);
            this.comboBoxMTGLeftCh12.Name = "comboBoxMTGLeftCh12";
            this.comboBoxMTGLeftCh12.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh12.TabIndex = 66;
            this.comboBoxMTGLeftCh12.Tag = "11";
            this.comboBoxMTGLeftCh12.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh11
            // 
            this.comboBoxMTGLeftCh11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh11.FormattingEnabled = true;
            this.comboBoxMTGLeftCh11.Location = new System.Drawing.Point(123, 289);
            this.comboBoxMTGLeftCh11.Name = "comboBoxMTGLeftCh11";
            this.comboBoxMTGLeftCh11.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh11.TabIndex = 65;
            this.comboBoxMTGLeftCh11.Tag = "10";
            this.comboBoxMTGLeftCh11.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh10
            // 
            this.comboBoxMTGLeftCh10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh10.FormattingEnabled = true;
            this.comboBoxMTGLeftCh10.Location = new System.Drawing.Point(123, 262);
            this.comboBoxMTGLeftCh10.Name = "comboBoxMTGLeftCh10";
            this.comboBoxMTGLeftCh10.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh10.TabIndex = 64;
            this.comboBoxMTGLeftCh10.Tag = "9";
            this.comboBoxMTGLeftCh10.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh9
            // 
            this.comboBoxMTGLeftCh9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh9.FormattingEnabled = true;
            this.comboBoxMTGLeftCh9.Location = new System.Drawing.Point(123, 235);
            this.comboBoxMTGLeftCh9.Name = "comboBoxMTGLeftCh9";
            this.comboBoxMTGLeftCh9.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh9.TabIndex = 63;
            this.comboBoxMTGLeftCh9.Tag = "8";
            this.comboBoxMTGLeftCh9.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh8
            // 
            this.comboBoxMTGLeftCh8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh8.FormattingEnabled = true;
            this.comboBoxMTGLeftCh8.Location = new System.Drawing.Point(123, 208);
            this.comboBoxMTGLeftCh8.Name = "comboBoxMTGLeftCh8";
            this.comboBoxMTGLeftCh8.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh8.TabIndex = 62;
            this.comboBoxMTGLeftCh8.Tag = "7";
            this.comboBoxMTGLeftCh8.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh7
            // 
            this.comboBoxMTGLeftCh7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh7.FormattingEnabled = true;
            this.comboBoxMTGLeftCh7.Location = new System.Drawing.Point(123, 181);
            this.comboBoxMTGLeftCh7.Name = "comboBoxMTGLeftCh7";
            this.comboBoxMTGLeftCh7.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh7.TabIndex = 61;
            this.comboBoxMTGLeftCh7.Tag = "6";
            this.comboBoxMTGLeftCh7.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh6
            // 
            this.comboBoxMTGLeftCh6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh6.FormattingEnabled = true;
            this.comboBoxMTGLeftCh6.Location = new System.Drawing.Point(123, 154);
            this.comboBoxMTGLeftCh6.Name = "comboBoxMTGLeftCh6";
            this.comboBoxMTGLeftCh6.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh6.TabIndex = 60;
            this.comboBoxMTGLeftCh6.Tag = "5";
            this.comboBoxMTGLeftCh6.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh5
            // 
            this.comboBoxMTGLeftCh5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh5.FormattingEnabled = true;
            this.comboBoxMTGLeftCh5.Location = new System.Drawing.Point(123, 127);
            this.comboBoxMTGLeftCh5.Name = "comboBoxMTGLeftCh5";
            this.comboBoxMTGLeftCh5.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh5.TabIndex = 59;
            this.comboBoxMTGLeftCh5.Tag = "4";
            this.comboBoxMTGLeftCh5.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh4
            // 
            this.comboBoxMTGLeftCh4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh4.FormattingEnabled = true;
            this.comboBoxMTGLeftCh4.Location = new System.Drawing.Point(123, 100);
            this.comboBoxMTGLeftCh4.Name = "comboBoxMTGLeftCh4";
            this.comboBoxMTGLeftCh4.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh4.TabIndex = 58;
            this.comboBoxMTGLeftCh4.Tag = "3";
            this.comboBoxMTGLeftCh4.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh3
            // 
            this.comboBoxMTGLeftCh3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh3.FormattingEnabled = true;
            this.comboBoxMTGLeftCh3.Location = new System.Drawing.Point(123, 73);
            this.comboBoxMTGLeftCh3.Name = "comboBoxMTGLeftCh3";
            this.comboBoxMTGLeftCh3.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh3.TabIndex = 57;
            this.comboBoxMTGLeftCh3.Tag = "2";
            this.comboBoxMTGLeftCh3.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // comboBoxMTGLeftCh2
            // 
            this.comboBoxMTGLeftCh2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh2.FormattingEnabled = true;
            this.comboBoxMTGLeftCh2.Location = new System.Drawing.Point(123, 46);
            this.comboBoxMTGLeftCh2.Name = "comboBoxMTGLeftCh2";
            this.comboBoxMTGLeftCh2.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh2.TabIndex = 56;
            this.comboBoxMTGLeftCh2.Tag = "1";
            this.comboBoxMTGLeftCh2.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Channel Number";
            // 
            // labelMTG_CH1
            // 
            this.labelMTG_CH1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH1.AutoSize = true;
            this.labelMTG_CH1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH1.Location = new System.Drawing.Point(52, 21);
            this.labelMTG_CH1.Name = "labelMTG_CH1";
            this.labelMTG_CH1.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH1.TabIndex = 5;
            this.labelMTG_CH1.Text = "1";
            // 
            // labelMTG_CH2
            // 
            this.labelMTG_CH2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH2.AutoSize = true;
            this.labelMTG_CH2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH2.Location = new System.Drawing.Point(52, 48);
            this.labelMTG_CH2.Name = "labelMTG_CH2";
            this.labelMTG_CH2.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH2.TabIndex = 8;
            this.labelMTG_CH2.Text = "2";
            // 
            // labelMTG_CH3
            // 
            this.labelMTG_CH3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH3.AutoSize = true;
            this.labelMTG_CH3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH3.Location = new System.Drawing.Point(52, 75);
            this.labelMTG_CH3.Name = "labelMTG_CH3";
            this.labelMTG_CH3.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH3.TabIndex = 9;
            this.labelMTG_CH3.Text = "3";
            // 
            // labelMTG_CH4
            // 
            this.labelMTG_CH4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH4.AutoSize = true;
            this.labelMTG_CH4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH4.Location = new System.Drawing.Point(52, 102);
            this.labelMTG_CH4.Name = "labelMTG_CH4";
            this.labelMTG_CH4.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH4.TabIndex = 7;
            this.labelMTG_CH4.Text = "4";
            // 
            // labelMTG_CH5
            // 
            this.labelMTG_CH5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH5.AutoSize = true;
            this.labelMTG_CH5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH5.Location = new System.Drawing.Point(52, 129);
            this.labelMTG_CH5.Name = "labelMTG_CH5";
            this.labelMTG_CH5.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH5.TabIndex = 16;
            this.labelMTG_CH5.Text = "5";
            // 
            // labelMTG_CH6
            // 
            this.labelMTG_CH6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH6.AutoSize = true;
            this.labelMTG_CH6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH6.Location = new System.Drawing.Point(52, 156);
            this.labelMTG_CH6.Name = "labelMTG_CH6";
            this.labelMTG_CH6.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH6.TabIndex = 18;
            this.labelMTG_CH6.Text = "6";
            // 
            // labelMTG_CH7
            // 
            this.labelMTG_CH7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH7.AutoSize = true;
            this.labelMTG_CH7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH7.Location = new System.Drawing.Point(52, 183);
            this.labelMTG_CH7.Name = "labelMTG_CH7";
            this.labelMTG_CH7.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH7.TabIndex = 10;
            this.labelMTG_CH7.Text = "7";
            // 
            // labelMTG_CH8
            // 
            this.labelMTG_CH8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH8.AutoSize = true;
            this.labelMTG_CH8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH8.Location = new System.Drawing.Point(52, 210);
            this.labelMTG_CH8.Name = "labelMTG_CH8";
            this.labelMTG_CH8.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH8.TabIndex = 12;
            this.labelMTG_CH8.Text = "8";
            // 
            // labelMTG_CH9
            // 
            this.labelMTG_CH9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH9.AutoSize = true;
            this.labelMTG_CH9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH9.Location = new System.Drawing.Point(52, 237);
            this.labelMTG_CH9.Name = "labelMTG_CH9";
            this.labelMTG_CH9.Size = new System.Drawing.Size(15, 16);
            this.labelMTG_CH9.TabIndex = 13;
            this.labelMTG_CH9.Text = "9";
            // 
            // labelMTG_CH10
            // 
            this.labelMTG_CH10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH10.AutoSize = true;
            this.labelMTG_CH10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH10.Location = new System.Drawing.Point(49, 264);
            this.labelMTG_CH10.Name = "labelMTG_CH10";
            this.labelMTG_CH10.Size = new System.Drawing.Size(22, 16);
            this.labelMTG_CH10.TabIndex = 15;
            this.labelMTG_CH10.Text = "10";
            // 
            // labelMTG_CH11
            // 
            this.labelMTG_CH11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH11.AutoSize = true;
            this.labelMTG_CH11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH11.Location = new System.Drawing.Point(49, 291);
            this.labelMTG_CH11.Name = "labelMTG_CH11";
            this.labelMTG_CH11.Size = new System.Drawing.Size(22, 16);
            this.labelMTG_CH11.TabIndex = 17;
            this.labelMTG_CH11.Text = "11";
            // 
            // labelMTG_CH12
            // 
            this.labelMTG_CH12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH12.AutoSize = true;
            this.labelMTG_CH12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH12.Location = new System.Drawing.Point(49, 318);
            this.labelMTG_CH12.Name = "labelMTG_CH12";
            this.labelMTG_CH12.Size = new System.Drawing.Size(22, 16);
            this.labelMTG_CH12.TabIndex = 14;
            this.labelMTG_CH12.Text = "12";
            // 
            // labelMTG_CH13
            // 
            this.labelMTG_CH13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH13.AutoSize = true;
            this.labelMTG_CH13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH13.Location = new System.Drawing.Point(49, 345);
            this.labelMTG_CH13.Name = "labelMTG_CH13";
            this.labelMTG_CH13.Size = new System.Drawing.Size(22, 16);
            this.labelMTG_CH13.TabIndex = 19;
            this.labelMTG_CH13.Text = "13";
            // 
            // labelMTG_CH14
            // 
            this.labelMTG_CH14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH14.AutoSize = true;
            this.labelMTG_CH14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH14.Location = new System.Drawing.Point(49, 372);
            this.labelMTG_CH14.Name = "labelMTG_CH14";
            this.labelMTG_CH14.Size = new System.Drawing.Size(22, 16);
            this.labelMTG_CH14.TabIndex = 11;
            this.labelMTG_CH14.Text = "14";
            // 
            // labelMTG_CH15
            // 
            this.labelMTG_CH15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH15.AutoSize = true;
            this.labelMTG_CH15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH15.Location = new System.Drawing.Point(49, 399);
            this.labelMTG_CH15.Name = "labelMTG_CH15";
            this.labelMTG_CH15.Size = new System.Drawing.Size(22, 16);
            this.labelMTG_CH15.TabIndex = 6;
            this.labelMTG_CH15.Text = "15";
            // 
            // labelMTG_CH16
            // 
            this.labelMTG_CH16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMTG_CH16.AutoSize = true;
            this.labelMTG_CH16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMTG_CH16.Location = new System.Drawing.Point(49, 430);
            this.labelMTG_CH16.Name = "labelMTG_CH16";
            this.labelMTG_CH16.Size = new System.Drawing.Size(22, 16);
            this.labelMTG_CH16.TabIndex = 20;
            this.labelMTG_CH16.Text = "16";
            // 
            // comboBoxMTGLeftCh1
            // 
            this.comboBoxMTGLeftCh1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGLeftCh1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGLeftCh1.FormattingEnabled = true;
            this.comboBoxMTGLeftCh1.Location = new System.Drawing.Point(123, 19);
            this.comboBoxMTGLeftCh1.Name = "comboBoxMTGLeftCh1";
            this.comboBoxMTGLeftCh1.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGLeftCh1.TabIndex = 55;
            this.comboBoxMTGLeftCh1.Tag = "0";
            this.comboBoxMTGLeftCh1.SelectedValueChanged += new System.EventHandler(this.handleLeftChannelChanged);
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(235, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 16);
            this.label25.TabIndex = 38;
            this.label25.Text = "Left";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(490, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(39, 16);
            this.label29.TabIndex = 87;
            this.label29.Text = "Right";
            // 
            // comboBoxMTGRightCh1
            // 
            this.comboBoxMTGRightCh1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMTGRightCh1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMTGRightCh1.FormattingEnabled = true;
            this.comboBoxMTGRightCh1.Location = new System.Drawing.Point(383, 19);
            this.comboBoxMTGRightCh1.Name = "comboBoxMTGRightCh1";
            this.comboBoxMTGRightCh1.Size = new System.Drawing.Size(254, 21);
            this.comboBoxMTGRightCh1.TabIndex = 88;
            this.comboBoxMTGRightCh1.Tag = "0";
            this.comboBoxMTGRightCh1.SelectedValueChanged += new System.EventHandler(this.handleRightChannelChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.NUD_MarginElocs);
            this.tabPage3.Controls.Add(this.LBL_MarginElocs);
            this.tabPage3.Controls.Add(this.BTN_showRoute);
            this.tabPage3.Controls.Add(this.BTN_DeletePoint);
            this.tabPage3.Controls.Add(this.BTN_Go);
            this.tabPage3.Controls.Add(this.BTN_AssignPosToCmt);
            this.tabPage3.Controls.Add(this.TXTB_Location);
            this.tabPage3.Controls.Add(this.LBL_Place);
            this.tabPage3.Controls.Add(this.PanelPointInfo);
            this.tabPage3.Controls.Add(this.MC_map);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1153, 579);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "GPS";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // NUD_MarginElocs
            // 
            this.NUD_MarginElocs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.NUD_MarginElocs.Location = new System.Drawing.Point(425, 553);
            this.NUD_MarginElocs.Name = "NUD_MarginElocs";
            this.NUD_MarginElocs.Size = new System.Drawing.Size(68, 20);
            this.NUD_MarginElocs.TabIndex = 49;
            this.NUD_MarginElocs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NUD_MarginElocs.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.NUD_MarginElocs.ValueChanged += new System.EventHandler(this.NUD_MarginElocs_ValueChanged);
            // 
            // LBL_MarginElocs
            // 
            this.LBL_MarginElocs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LBL_MarginElocs.AutoSize = true;
            this.LBL_MarginElocs.Location = new System.Drawing.Point(329, 558);
            this.LBL_MarginElocs.Name = "LBL_MarginElocs";
            this.LBL_MarginElocs.Size = new System.Drawing.Size(91, 13);
            this.LBL_MarginElocs.TabIndex = 48;
            this.LBL_MarginElocs.Text = "Margin Comments";
            // 
            // BTN_showRoute
            // 
            this.BTN_showRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BTN_showRoute.Location = new System.Drawing.Point(237, 553);
            this.BTN_showRoute.Name = "BTN_showRoute";
            this.BTN_showRoute.Size = new System.Drawing.Size(75, 23);
            this.BTN_showRoute.TabIndex = 47;
            this.BTN_showRoute.Text = "Show Route";
            this.BTN_showRoute.UseVisualStyleBackColor = true;
            this.BTN_showRoute.Click += new System.EventHandler(this.BTN_showRoute_Click);
            // 
            // BTN_DeletePoint
            // 
            this.BTN_DeletePoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BTN_DeletePoint.Location = new System.Drawing.Point(156, 553);
            this.BTN_DeletePoint.Name = "BTN_DeletePoint";
            this.BTN_DeletePoint.Size = new System.Drawing.Size(75, 23);
            this.BTN_DeletePoint.TabIndex = 46;
            this.BTN_DeletePoint.Text = "Delete Point";
            this.BTN_DeletePoint.UseVisualStyleBackColor = true;
            this.BTN_DeletePoint.Click += new System.EventHandler(this.BTN_DeletePoint_Click);
            // 
            // BTN_Go
            // 
            this.BTN_Go.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BTN_Go.Location = new System.Drawing.Point(1091, 486);
            this.BTN_Go.Name = "BTN_Go";
            this.BTN_Go.Size = new System.Drawing.Size(43, 23);
            this.BTN_Go.TabIndex = 45;
            this.BTN_Go.Text = "Go";
            this.BTN_Go.UseVisualStyleBackColor = true;
            this.BTN_Go.Click += new System.EventHandler(this.BTN_Go_Click);
            // 
            // BTN_AssignPosToCmt
            // 
            this.BTN_AssignPosToCmt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BTN_AssignPosToCmt.Location = new System.Drawing.Point(3, 553);
            this.BTN_AssignPosToCmt.Name = "BTN_AssignPosToCmt";
            this.BTN_AssignPosToCmt.Size = new System.Drawing.Size(147, 23);
            this.BTN_AssignPosToCmt.TabIndex = 44;
            this.BTN_AssignPosToCmt.Text = "Assign GPS To Comment";
            this.BTN_AssignPosToCmt.UseVisualStyleBackColor = true;
            this.BTN_AssignPosToCmt.Click += new System.EventHandler(this.BTN_AssignPosToCmt_Click);
            // 
            // TXTB_Location
            // 
            this.TXTB_Location.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TXTB_Location.Location = new System.Drawing.Point(585, 551);
            this.TXTB_Location.Name = "TXTB_Location";
            this.TXTB_Location.Size = new System.Drawing.Size(301, 20);
            this.TXTB_Location.TabIndex = 43;
            this.TXTB_Location.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXTB_Location_KeyDown);
            // 
            // LBL_Place
            // 
            this.LBL_Place.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LBL_Place.AutoSize = true;
            this.LBL_Place.Location = new System.Drawing.Point(528, 555);
            this.LBL_Place.Name = "LBL_Place";
            this.LBL_Place.Size = new System.Drawing.Size(51, 13);
            this.LBL_Place.TabIndex = 42;
            this.LBL_Place.Text = "Location:";
            // 
            // PanelPointInfo
            // 
            this.PanelPointInfo.Controls.Add(this.LBL_PointData);
            this.PanelPointInfo.Location = new System.Drawing.Point(3, 6);
            this.PanelPointInfo.Name = "PanelPointInfo";
            this.PanelPointInfo.Size = new System.Drawing.Size(193, 79);
            this.PanelPointInfo.TabIndex = 41;
            // 
            // LBL_PointData
            // 
            this.LBL_PointData.AutoSize = true;
            this.LBL_PointData.Location = new System.Drawing.Point(12, 14);
            this.LBL_PointData.Name = "LBL_PointData";
            this.LBL_PointData.Size = new System.Drawing.Size(0, 13);
            this.LBL_PointData.TabIndex = 39;
            // 
            // MC_map
            // 
            this.MC_map.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MC_map.Bearing = 0F;
            this.MC_map.CanDragMap = true;
            this.MC_map.EmptyTileColor = System.Drawing.Color.Navy;
            this.MC_map.GrayScaleMode = false;
            this.MC_map.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.MC_map.LevelsKeepInMemmory = 5;
            this.MC_map.Location = new System.Drawing.Point(3, 6);
            this.MC_map.MarkersEnabled = true;
            this.MC_map.MaxZoom = 20;
            this.MC_map.MinZoom = 2;
            this.MC_map.MouseWheelZoomEnabled = true;
            this.MC_map.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionWithoutCenter;
            this.MC_map.Name = "MC_map";
            this.MC_map.NegativeMode = false;
            this.MC_map.PolygonsEnabled = true;
            this.MC_map.RetryLoadTile = 0;
            this.MC_map.RoutesEnabled = true;
            this.MC_map.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.MC_map.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.MC_map.ShowTileGridLines = false;
            this.MC_map.Size = new System.Drawing.Size(883, 542);
            this.MC_map.TabIndex = 30;
            this.MC_map.Zoom = 15D;
            this.MC_map.OnMarkerEnter += new GMap.NET.WindowsForms.MarkerEnter(this.MC_map_OnMarkerEnter);
            this.MC_map.OnMarkerLeave += new GMap.NET.WindowsForms.MarkerLeave(this.MC_map_OnMarkerLeave);
            this.MC_map.OnMapZoomChanged += new GMap.NET.MapZoomChanged(this.MC_map_OnMapZoomChanged);
            this.MC_map.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MC_map_MouseClick);
            this.MC_map.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MC_map_MouseDown);
            this.MC_map.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MC_map_MouseMove);
            this.MC_map.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MC_map_MouseUp);
            // 
            // labelSelectedStop
            // 
            this.labelSelectedStop.AutoSize = true;
            this.labelSelectedStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectedStop.Location = new System.Drawing.Point(86, 42);
            this.labelSelectedStop.Name = "labelSelectedStop";
            this.labelSelectedStop.Size = new System.Drawing.Size(0, 16);
            this.labelSelectedStop.TabIndex = 28;
            // 
            // labelSelectedRoute
            // 
            this.labelSelectedRoute.AutoSize = true;
            this.labelSelectedRoute.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectedRoute.Location = new System.Drawing.Point(86, 16);
            this.labelSelectedRoute.Name = "labelSelectedRoute";
            this.labelSelectedRoute.Size = new System.Drawing.Size(0, 16);
            this.labelSelectedRoute.TabIndex = 27;
            // 
            // openFileDialog
            // 
            this.openFileDialog.InitialDirectory = "input";
            this.openFileDialog.Multiselect = true;
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.SelectedPath = "C:\\Users";
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MoveUp,
            this.MoveDown,
            this.MoveTop,
            this.MoveBottom,
            this.RemoveItem,
            this.editElocutionToolStripMenuItem,
            this.renameToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            this.contextMenuStrip.Size = new System.Drawing.Size(148, 158);
            // 
            // MoveUp
            // 
            this.MoveUp.Name = "MoveUp";
            this.MoveUp.Size = new System.Drawing.Size(147, 22);
            this.MoveUp.Text = "UP";
            this.MoveUp.Click += new System.EventHandler(this.moveUPToolStripMenuItem_Click);
            // 
            // MoveDown
            // 
            this.MoveDown.Name = "MoveDown";
            this.MoveDown.Size = new System.Drawing.Size(147, 22);
            this.MoveDown.Text = "DOWN";
            this.MoveDown.Click += new System.EventHandler(this.MoveDown_Click);
            // 
            // MoveTop
            // 
            this.MoveTop.Name = "MoveTop";
            this.MoveTop.Size = new System.Drawing.Size(147, 22);
            this.MoveTop.Text = "TOP";
            this.MoveTop.Click += new System.EventHandler(this.MoveTop_Click);
            // 
            // MoveBottom
            // 
            this.MoveBottom.Name = "MoveBottom";
            this.MoveBottom.Size = new System.Drawing.Size(147, 22);
            this.MoveBottom.Text = "BOTTOM";
            this.MoveBottom.Click += new System.EventHandler(this.MoveBottom_Click);
            // 
            // RemoveItem
            // 
            this.RemoveItem.Name = "RemoveItem";
            this.RemoveItem.Size = new System.Drawing.Size(147, 22);
            this.RemoveItem.Text = "Remove item";
            this.RemoveItem.Click += new System.EventHandler(this.RemoveItem_Click);
            // 
            // editElocutionToolStripMenuItem
            // 
            this.editElocutionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoplayAutostopToolStripMenuItem});
            this.editElocutionToolStripMenuItem.Name = "editElocutionToolStripMenuItem";
            this.editElocutionToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.editElocutionToolStripMenuItem.Text = "Edit elocution";
            this.editElocutionToolStripMenuItem.Visible = false;
            // 
            // autoplayAutostopToolStripMenuItem
            // 
            this.autoplayAutostopToolStripMenuItem.Name = "autoplayAutostopToolStripMenuItem";
            this.autoplayAutostopToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.autoplayAutostopToolStripMenuItem.Text = "Autoplay/Autostop";
            this.autoplayAutostopToolStripMenuItem.Click += new System.EventHandler(this.autoplayAutostopToolStripMenuItem_Click);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.renameToolStripMenuItem.Text = "Edit title";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // btnNewProject
            // 
            this.btnNewProject.Location = new System.Drawing.Point(346, 3);
            this.btnNewProject.Name = "btnNewProject";
            this.btnNewProject.Size = new System.Drawing.Size(75, 23);
            this.btnNewProject.TabIndex = 28;
            this.btnNewProject.Text = "New Project";
            this.btnNewProject.UseVisualStyleBackColor = true;
            this.btnNewProject.Click += new System.EventHandler(this.btnNewProject_Click);
            // 
            // btnExportTo
            // 
            this.btnExportTo.Location = new System.Drawing.Point(260, 3);
            this.btnExportTo.Name = "btnExportTo";
            this.btnExportTo.Size = new System.Drawing.Size(75, 23);
            this.btnExportTo.TabIndex = 29;
            this.btnExportTo.Text = "Export";
            this.btnExportTo.UseVisualStyleBackColor = true;
            this.btnExportTo.Click += new System.EventHandler(this.btnExportTo_Click);
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(432, 3);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(75, 23);
            this.btnReload.TabIndex = 30;
            this.btnReload.Text = "Reload Project";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // btnFormat
            // 
            this.btnFormat.Location = new System.Drawing.Point(184, 3);
            this.btnFormat.Name = "btnFormat";
            this.btnFormat.Size = new System.Drawing.Size(70, 23);
            this.btnFormat.TabIndex = 31;
            this.btnFormat.Text = "Format";
            this.btnFormat.UseVisualStyleBackColor = true;
            this.btnFormat.Click += new System.EventHandler(this.btnFormat_Click);
            // 
            // CMS_MapMarkers
            // 
            this.CMS_MapMarkers.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.CMS_MapMarkers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedAngleToolStripMenuItem,
            this.angleErrorToolStripMenuItem,
            this.radiousToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.CMS_MapMarkers.Name = "CMS_MapMarkers";
            this.CMS_MapMarkers.Size = new System.Drawing.Size(255, 92);
            // 
            // selectedAngleToolStripMenuItem
            // 
            this.selectedAngleToolStripMenuItem.Enabled = false;
            this.selectedAngleToolStripMenuItem.Name = "selectedAngleToolStripMenuItem";
            this.selectedAngleToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.selectedAngleToolStripMenuItem.Text = "Selected angle";
            // 
            // angleErrorToolStripMenuItem
            // 
            this.angleErrorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CB_AngleError});
            this.angleErrorToolStripMenuItem.Name = "angleErrorToolStripMenuItem";
            this.angleErrorToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.angleErrorToolStripMenuItem.Text = "Allowed Direction Angle (degrees)";
            // 
            // CB_AngleError
            // 
            this.CB_AngleError.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_AngleError.Items.AddRange(new object[] {
            "20",
            "30",
            "40",
            "50",
            "70",
            "90"});
            this.CB_AngleError.Name = "CB_AngleError";
            this.CB_AngleError.Size = new System.Drawing.Size(121, 23);
            this.CB_AngleError.SelectedIndexChanged += new System.EventHandler(this.CB_AngleError_SelectedIndexChanged);
            // 
            // radiousToolStripMenuItem
            // 
            this.radiousToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CB_Radius});
            this.radiousToolStripMenuItem.Name = "radiousToolStripMenuItem";
            this.radiousToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.radiousToolStripMenuItem.Text = "Radius (meters)";
            // 
            // CB_Radius
            // 
            this.CB_Radius.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_Radius.Items.AddRange(new object[] {
            "20",
            "40",
            "60",
            "80",
            "100"});
            this.CB_Radius.Name = "CB_Radius";
            this.CB_Radius.Size = new System.Drawing.Size(121, 23);
            this.CB_Radius.SelectedIndexChanged += new System.EventHandler(this.CB_Radious_SelectedIndexChanged);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // groupBoxCurrentRouteStopComment
            // 
            this.groupBoxCurrentRouteStopComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxCurrentRouteStopComment.Controls.Add(this.labelSelectedStop);
            this.groupBoxCurrentRouteStopComment.Controls.Add(this.labelRoute);
            this.groupBoxCurrentRouteStopComment.Controls.Add(this.labelSelectedRoute);
            this.groupBoxCurrentRouteStopComment.Controls.Add(this.labelComment);
            this.groupBoxCurrentRouteStopComment.Controls.Add(this.labelSelectedComment);
            this.groupBoxCurrentRouteStopComment.Controls.Add(this.labelStop);
            this.groupBoxCurrentRouteStopComment.Location = new System.Drawing.Point(13, 497);
            this.groupBoxCurrentRouteStopComment.Name = "groupBoxCurrentRouteStopComment";
            this.groupBoxCurrentRouteStopComment.Size = new System.Drawing.Size(271, 120);
            this.groupBoxCurrentRouteStopComment.TabIndex = 33;
            this.groupBoxCurrentRouteStopComment.TabStop = false;
            // 
            // labelRoute
            // 
            this.labelRoute.AutoSize = true;
            this.labelRoute.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRoute.Location = new System.Drawing.Point(6, 16);
            this.labelRoute.Name = "labelRoute";
            this.labelRoute.Size = new System.Drawing.Size(47, 16);
            this.labelRoute.TabIndex = 31;
            this.labelRoute.Text = "Route:";
            // 
            // labelComment
            // 
            this.labelComment.AutoSize = true;
            this.labelComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelComment.Location = new System.Drawing.Point(6, 69);
            this.labelComment.Name = "labelComment";
            this.labelComment.Size = new System.Drawing.Size(68, 16);
            this.labelComment.TabIndex = 29;
            this.labelComment.Text = "Comment:";
            // 
            // labelSelectedComment
            // 
            this.labelSelectedComment.AutoSize = true;
            this.labelSelectedComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectedComment.Location = new System.Drawing.Point(86, 69);
            this.labelSelectedComment.Name = "labelSelectedComment";
            this.labelSelectedComment.Size = new System.Drawing.Size(0, 16);
            this.labelSelectedComment.TabIndex = 30;
            // 
            // labelStop
            // 
            this.labelStop.AutoSize = true;
            this.labelStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStop.Location = new System.Drawing.Point(6, 42);
            this.labelStop.Name = "labelStop";
            this.labelStop.Size = new System.Drawing.Size(39, 16);
            this.labelStop.TabIndex = 26;
            this.labelStop.Text = "Stop:";
            // 
            // BTN_ImportInfs
            // 
            this.BTN_ImportInfs.Location = new System.Drawing.Point(103, 3);
            this.BTN_ImportInfs.Name = "BTN_ImportInfs";
            this.BTN_ImportInfs.Size = new System.Drawing.Size(75, 23);
            this.BTN_ImportInfs.TabIndex = 32;
            this.BTN_ImportInfs.Text = "Import INFS";
            this.BTN_ImportInfs.UseVisualStyleBackColor = true;
            this.BTN_ImportInfs.Click += new System.EventHandler(this.BTN_ImportInfs_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 9;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.58333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.80556F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel4.Controls.Add(this.BTN_ImportGpsInf, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnClose, 8, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnSaveProject, 7, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnLoadProject, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnReload, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnNewProject, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnExportTo, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnFormat, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.BTN_ImportInfs, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(363, 623);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(817, 33);
            this.tableLayoutPanel4.TabIndex = 34;
            // 
            // BTN_ImportGpsInf
            // 
            this.BTN_ImportGpsInf.Location = new System.Drawing.Point(3, 3);
            this.BTN_ImportGpsInf.Name = "BTN_ImportGpsInf";
            this.BTN_ImportGpsInf.Size = new System.Drawing.Size(94, 23);
            this.BTN_ImportGpsInf.TabIndex = 33;
            this.BTN_ImportGpsInf.Text = "Import GPS INF";
            this.BTN_ImportGpsInf.UseVisualStyleBackColor = true;
            this.BTN_ImportGpsInf.Click += new System.EventHandler(this.BTN_ImportGpsInf_Click);
            // 
            // BTN_CollapseAll
            // 
            this.BTN_CollapseAll.Image = global::RouteMaker.Properties.Resources.collapse_all;
            this.BTN_CollapseAll.Location = new System.Drawing.Point(88, 12);
            this.BTN_CollapseAll.Name = "BTN_CollapseAll";
            this.BTN_CollapseAll.Size = new System.Drawing.Size(16, 16);
            this.BTN_CollapseAll.TabIndex = 36;
            this.toolTipExpandCollapseButtons.SetToolTip(this.BTN_CollapseAll, "Collapse All");
            this.BTN_CollapseAll.UseVisualStyleBackColor = true;
            this.BTN_CollapseAll.Click += new System.EventHandler(this.BTN_CollapseAll_Click);
            // 
            // BTN_ExpandAll
            // 
            this.BTN_ExpandAll.Image = global::RouteMaker.Properties.Resources.expand_all;
            this.BTN_ExpandAll.Location = new System.Drawing.Point(66, 12);
            this.BTN_ExpandAll.Name = "BTN_ExpandAll";
            this.BTN_ExpandAll.Size = new System.Drawing.Size(16, 16);
            this.BTN_ExpandAll.TabIndex = 35;
            this.toolTipExpandCollapseButtons.SetToolTip(this.BTN_ExpandAll, "Expand All");
            this.BTN_ExpandAll.UseVisualStyleBackColor = true;
            this.BTN_ExpandAll.Click += new System.EventHandler(this.BTN_ExpandAll_Click);
            // 
            // labelCopySavingStatus
            // 
            this.labelCopySavingStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCopySavingStatus.AutoSize = true;
            this.labelCopySavingStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCopySavingStatus.Location = new System.Drawing.Point(18, 674);
            this.labelCopySavingStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCopySavingStatus.Name = "labelCopySavingStatus";
            this.labelCopySavingStatus.Size = new System.Drawing.Size(0, 24);
            this.labelCopySavingStatus.TabIndex = 37;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 662);
            this.Controls.Add(this.labelCopySavingStatus);
            this.Controls.Add(this.BTN_CollapseAll);
            this.Controls.Add(this.BTN_ExpandAll);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.groupBoxCurrentRouteStopComment);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.labTreeContent);
            this.Controls.Add(this.treeView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLanguages)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMusicVol)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMTGChannels)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_MarginElocs)).EndInit();
            this.PanelPointInfo.ResumeLayout(false);
            this.PanelPointInfo.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.CMS_MapMarkers.ResumeLayout(false);
            this.groupBoxCurrentRouteStopComment.ResumeLayout(false);
            this.groupBoxCurrentRouteStopComment.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labOperatorName;
        private System.Windows.Forms.TextBox TXTB_OperatorName;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Label labTreeContent;
        private System.Windows.Forms.Button btnSaveProject;
        private System.Windows.Forms.Button btnLoadProject;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labNumChannel;
        private System.Windows.Forms.Label labelNumCh1;
        private System.Windows.Forms.Label labelNumCh2;
        private System.Windows.Forms.Label labelNumCh3;
        private System.Windows.Forms.Label labelNumCh4;
        private System.Windows.Forms.Label labelNumCh5;
        private System.Windows.Forms.Label labelNumCh6;
        private System.Windows.Forms.Label labelNumCh7;
        private System.Windows.Forms.Label labelNumCh8;
        private System.Windows.Forms.Label labelNumCh9;
        private System.Windows.Forms.Label labelNumCh10;
        private System.Windows.Forms.Label labelNumCh11;
        private System.Windows.Forms.Label labelNumCh12;
        private System.Windows.Forms.Label labelNumCh13;
        private System.Windows.Forms.Label labelNumCh14;
        private System.Windows.Forms.Label labelNumCh15;
        private System.Windows.Forms.Label labelNumCh16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button btnAddRoute;
        private System.Windows.Forms.TextBox textBoxRouteName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnAddStop;
        private System.Windows.Forms.TextBox textBoxStopName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnAddMusictoStop;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox textBoxElocutionName;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnAddEloc;
        private System.Windows.Forms.CheckBox checkBoxAutoplay;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cmbBoxModuleType;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown numLanguages;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem RemoveItem;
        private System.Windows.Forms.ToolStripMenuItem MoveUp;
        private System.Windows.Forms.ToolStripMenuItem MoveDown;
        private System.Windows.Forms.ToolStripMenuItem editElocutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoplayAutostopToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown16;
        private System.Windows.Forms.NumericUpDown numericUpDown15;
        private System.Windows.Forms.NumericUpDown numericUpDown14;
        private System.Windows.Forms.NumericUpDown numericUpDown13;
        private System.Windows.Forms.NumericUpDown numericUpDown12;
        private System.Windows.Forms.NumericUpDown numericUpDown11;
        private System.Windows.Forms.NumericUpDown numericUpDown10;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button btnAddMusicToRoute;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStripMenuItem MoveTop;
        private System.Windows.Forms.ToolStripMenuItem MoveBottom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownMusicVol;
        private System.Windows.Forms.Label labelSelectedStop;
        private System.Windows.Forms.Label labelSelectedRoute;
        private System.Windows.Forms.Button btnNewProject;
        private System.Windows.Forms.Label labelProjectName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnExportTo;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Button btnFormat;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button BTN_AssignPosToCmt;
        private System.Windows.Forms.TextBox TXTB_Location;
        private System.Windows.Forms.Label LBL_Place;
        private System.Windows.Forms.Panel PanelPointInfo;
        private System.Windows.Forms.Label LBL_PointData;
        private GMap.NET.WindowsForms.GMapControl MC_map;
        private System.Windows.Forms.ContextMenuStrip CMS_MapMarkers;
        private System.Windows.Forms.ToolStripMenuItem selectedAngleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem angleErrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox CB_AngleError;
        private System.Windows.Forms.ToolStripMenuItem radiousToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox CB_Radius;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxCurrentRouteStopComment;
        private System.Windows.Forms.Label labelRoute;
        private System.Windows.Forms.Label labelComment;
        private System.Windows.Forms.Label labelSelectedComment;
        private System.Windows.Forms.Label labelStop;
        private System.Windows.Forms.Button BTN_Go;
        private System.Windows.Forms.Button BTN_DeletePoint;
        private System.Windows.Forms.Button BTN_ImportInfs;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button BTN_showRoute;
        private System.Windows.Forms.NumericUpDown NUD_MarginElocs;
        private System.Windows.Forms.Label LBL_MarginElocs;
        private System.Windows.Forms.Button BTN_ExpandAll;
        private System.Windows.Forms.ToolTip toolTipExpandCollapseButtons;
        private System.Windows.Forms.Button BTN_CollapseAll;
        private System.Windows.Forms.Label labelCopySavingStatus;
        private System.Windows.Forms.Button BTN_ImportGpsInf;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh16;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh15;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh14;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh13;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh12;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh11;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh10;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh9;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh8;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelMTG_CH1;
        private System.Windows.Forms.Label labelMTG_CH2;
        private System.Windows.Forms.Label labelMTG_CH3;
        private System.Windows.Forms.Label labelMTG_CH4;
        private System.Windows.Forms.Label labelMTG_CH5;
        private System.Windows.Forms.Label labelMTG_CH6;
        private System.Windows.Forms.Label labelMTG_CH7;
        private System.Windows.Forms.Label labelMTG_CH8;
        private System.Windows.Forms.Label labelMTG_CH9;
        private System.Windows.Forms.Label labelMTG_CH10;
        private System.Windows.Forms.Label labelMTG_CH11;
        private System.Windows.Forms.Label labelMTG_CH12;
        private System.Windows.Forms.Label labelMTG_CH13;
        private System.Windows.Forms.Label labelMTG_CH14;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelMTG_CH15;
        private System.Windows.Forms.Label labelMTG_CH16;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown numMTGChannels;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh16;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh15;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh14;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh13;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh12;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh11;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh10;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh9;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh8;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh7;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh6;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh5;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh4;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh3;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh2;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh6;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh5;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh4;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh3;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh2;
        private System.Windows.Forms.ComboBox comboBoxMTGLeftCh1;
        private System.Windows.Forms.ComboBox comboBoxMTGRightCh1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
    }
}

