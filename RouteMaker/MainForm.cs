﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;

using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET;

using RouteMaker.Model;
using System.Runtime.InteropServices;
using System.Threading;

namespace RouteMaker
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            Text = "RouteMaker - " + Ctes.VERSION; 

            MC_map.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMaps.Instance.Mode = AccessMode.ServerOnly;
            MC_map.SetPositionByKeywords("Barcelona");

            mMarkersOverlay = new GMapOverlay(Ctes.MARKERS_OVERLAY);
            MC_map.Overlays.Add(mMarkersOverlay);

            mDirOverlay = new GMapOverlay(Ctes.MARKERS_DIRECTION_OVERLAY);
            MC_map.Overlays.Add(mDirOverlay);

            mRouteOverlay = new GMapOverlay(Ctes.POLYGONS_OVERLAY);
            MC_map.Overlays.Add(mRouteOverlay);
        }

        #region remember scroll position
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int GetScrollPos(IntPtr hWnd, int nBar);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        private const int SB_HORZ = 0x0;
        private const int SB_VERT = 0x1;

        private Point GetTreeViewScrollPos(TreeView aTreeView)
        {
            return new Point(
                GetScrollPos(aTreeView.Handle, SB_HORZ),
                GetScrollPos(aTreeView.Handle, SB_VERT));
        }

        private void SetTreeViewScrollPos(TreeView treeView, Point scrollPosition)
        {
            SetScrollPos((IntPtr)treeView.Handle, SB_HORZ, scrollPosition.X, true);
            SetScrollPos((IntPtr)treeView.Handle, SB_VERT, scrollPosition.Y, true);
        }
        #endregion
        
        private void btnClose_Click(object sender, EventArgs e)
        {   
            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ProjectMngr.getInstance().isProjSaved())
            {
                if (MessageBox.Show("Close without saving ?",
                                    "The project hasn't been saved",
                                    MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        public static int mSelectedModuleType = -1;
        private void btnSaveProject_Click(object sender, EventArgs e)
        {
            string lProjName = ProjectMngr.getInstance().getProjName();
            if(string.IsNullOrEmpty(lProjName))
            {
                MessageBox.Show("A project must be considered");
                return;
            }

            if (cmbBoxModuleType.SelectedIndex == -1)
            {
                MessageBox.Show("A System Model must be selected");
                return;
            }

            saveChannelOrderInProj();
            saveChannelGainInProj();

            Cursor.Current = Cursors.WaitCursor;
            labelCopySavingStatus.Text = "Saving project...";

            mSelectedModuleType = cmbBoxModuleType.SelectedIndex;

            string lErrorMessage = String.Empty;
            if (!ProjectMngr.getInstance().checkMissingMusics(ref lErrorMessage))
            {
                MessageBox.Show(lErrorMessage);
                labelCopySavingStatus.Text = String.Empty;
                return;
            }

            Thread lThCopy = new Thread(() =>
            {
                ProjectMngr.getInstance().saveProjectIntoFolder("output\\" + lProjName,
                                        TXTB_OperatorName.Text,
                                        Decimal.ToInt32(numLanguages.Value));
            });
            lThCopy.Start();
            lThCopy.Join();

            Cursor.Current = Cursors.Default; // restore the cursor
            labelCopySavingStatus.Text = string.Empty;
        }
        
        // the following object is necessary to allow select a tree node
        // in treeView after the users clicks on a marker in the map.
        private Dictionary<Elocution, TreeNode> mElocsWithGps = new Dictionary<Elocution, TreeNode>();

        private void updateElocInTreeView(Elocution aEloc)
        {
            // get the route-stop-eloc index
            Tuple<int, int, int> lRSE_index = 
                ProjectMngr.getInstance().getCurrentIndexes(mSelectedRoute, 
                                                            mSelectedStop, 
                                                            mSelectedComment);
            int lIndexRoute = lRSE_index.Item1;
            int lIndexStop = lRSE_index.Item2;
            int lIndexEloc = lRSE_index.Item3;

            Stop lStop = ProjectMngr.getInstance().getStop(mSelectedRoute, mSelectedStop);

            if (lIndexRoute != -1 && lIndexStop != -1 && lIndexEloc != -1 && lStop != null)
            {
                TreeNode lElocNode = treeView.Nodes[lIndexRoute].
                                              Nodes[lIndexStop].
                                              Nodes[lStop.Musics.Count + lIndexEloc]; // jumping the music nodes
                if (lElocNode != null)
                {
                    lElocNode.Name = "commentary " +
                                                (aEloc.IsAutoplay ? "(Auto)" : string.Empty)
                                                + ": " +
                                                aEloc.ElocutionName;

                    lElocNode.Name = aEloc.ElocutionName;

                    if (aEloc.GpsLat != 0 && aEloc.GpsLong != 0)
                    { // this eloc has a GPS position assigned
                        lElocNode.Nodes.Clear();

                        string lStrLat = string.Format("{0:0.0000}", aEloc.GpsLat);
                        string lStrLon = string.Format("{0:0.0000}", aEloc.GpsLong);
                        lElocNode.Nodes.Add("GPS: lat " + lStrLat + "    lon " + lStrLon);

                        if (!mElocsWithGps.ContainsKey(aEloc))
                        {
                            mElocsWithGps.Add(aEloc, lElocNode);
                        }

                        lElocNode.Expand();
                    }
                    else if (aEloc.GpsLat == 0 && aEloc.GpsLong == 0)
                    {
                        if (mElocsWithGps.ContainsKey(aEloc))
                        {
                            mElocsWithGps[aEloc].Nodes.Clear();
                            mElocsWithGps.Remove(aEloc);
                        }
                    }

                    if (aEloc.ElocFile != string.Empty)
                    {
                        lElocNode.Nodes.Add(aEloc.ElocFile);
                    }
                }
            }            
        }

        private Dictionary<string, bool> mTreeExpandState
            = new Dictionary<string, bool>();

        private void saveNodeState(TreeNode aTreeNode)
        {
            if ( ! mTreeExpandState.ContainsKey(aTreeNode.Text))
            {
                mTreeExpandState.Add(aTreeNode.Text, aTreeNode.IsExpanded);
            }
            foreach (TreeNode lTN in aTreeNode.Nodes)
            {
                saveNodeState(lTN);
            }            
        }
        
        private void restoreNodeState(TreeNode aTreeNode)
        {
            if (mTreeExpandState.ContainsKey(aTreeNode.Text))
            {
                if (mTreeExpandState[aTreeNode.Text])
                {
                    aTreeNode.Expand();
                }
            }

            foreach(TreeNode lTN in aTreeNode.Nodes)
            {
                restoreNodeState(lTN);
            }
        }
        
        /// <summary>
        /// Updates the whole tree view
        /// </summary>
        private void updateTreeView()
        {
            Point ScrollPos = GetTreeViewScrollPos(treeView); // copy the scroll position
            
            treeView.BeginUpdate();

            // save extanded/collapsed status of each node
            mTreeExpandState.Clear();
            foreach (TreeNode lTN in treeView.Nodes)
            {
                saveNodeState(lTN);
            }

            treeView.Nodes.Clear();

            mElocsWithGps.Clear();
            
            List<Route> lAllRoutes = ProjectMngr.getInstance().getAllRotues();
            foreach (Route lRoute in lAllRoutes)
            {
                TreeNode lRouteNode = treeView.Nodes.Add("route: " + lRoute.RouteName);
                lRouteNode.Name = lRoute.RouteName;

                foreach (Stop lStop in lRoute.Stops)
                {
                    TreeNode lStopNode = lRouteNode.Nodes.Add("stop: " + lStop.StopName);
                    lStopNode.Name = lStop.StopName;

                    foreach (String lMusic in lStop.Musics)
                    {
                        TreeNode lMusicNode = lStopNode.Nodes.Add("music: " + lMusic);
                        lMusicNode.Name = lMusic;
                    }

                    foreach(Elocution lElocution in lStop.Elocutions)
                    {
                        TreeNode lElocNode =
                            lStopNode.Nodes.Add("commentary " + 
                                                (lElocution.IsAutoplay ? "(Auto)" : string.Empty)
                                                + ": " + 
                                                lElocution.ElocutionName);

                        lElocNode.Name = lElocution.ElocutionName;
                        
                        if (lElocution.GpsLat != 0 && lElocution.GpsLong != 0)
                        { // this eloc has a GPS position assigned
                            string lStrLat = string.Format("{0:0.0000}", lElocution.GpsLat);
                            string lStrLon = string.Format("{0:0.0000}", lElocution.GpsLong);
                            lElocNode.Nodes.Add("GPS: lat " + lStrLat + "    lon " + lStrLon);

                            mElocsWithGps.Add(lElocution, lElocNode);
                        }

                        if (!string.IsNullOrEmpty(lElocution.ElocFile))
                        {
                            lElocNode.Nodes.Add(lElocution.ElocFile);
                        }
                    }
                }                
            }               
            
            // restore the expand/collapse tree node state
            foreach(TreeNode lTN in treeView.Nodes)
            {
                restoreNodeState(lTN);
            }

            SetTreeViewScrollPos(treeView, ScrollPos); // restore scroll position

            treeView.EndUpdate();
        }

        private void BTN_ExpandAll_Click(object sender, EventArgs e)
        {
            treeView.ExpandAll();
        }

        private void BTN_CollapseAll_Click(object sender, EventArgs e)
        {
            treeView.CollapseAll();
        }

        private void btnAddRoute_Click(object sender, EventArgs e)
        {
            if (textBoxRouteName.Text == string.Empty)
            {
                MessageBox.Show("Route name cannot be empty");
                return;
            }

            ProjectMngr.getInstance().addNewRoute(textBoxRouteName.Text);
            updateTreeView();

            textBoxRouteName.Text = string.Empty;
        }

        private void btnAddStop_Click(object sender, EventArgs e)
        {
            if (textBoxStopName.Text == string.Empty)
            {
                MessageBox.Show("Stop name cannot be empty");
                return;
            }

            if (mSelectedRoute == string.Empty)
            {
                MessageBox.Show("A route must be selected");
                return;
            }
            
            ProjectMngr.getInstance().addNewStopToRoute(mSelectedRoute,
                                                        textBoxStopName.Text);
            updateTreeView();

            textBoxStopName.Text = string.Empty;
        }

        private void btnAddMusictoStop_Click(object sender, EventArgs e)
        {
            if (mSelectedRoute == string.Empty)
            {
                MessageBox.Show("A route must be selected");
                return;
            }

            if (mSelectedStop == string.Empty)
            {
                MessageBox.Show("A stop must be selected");
                return;
            }
            
            if (!Directory.Exists(Ctes.MUSIC_DIR()))
            {
                MessageBox.Show("Folder " + Ctes.MUSIC_DIR() + " must exist");
                return;
            }

            openFileDialog.FileName = String.Empty;
            openFileDialog.InitialDirectory = Ctes.MUSIC_DIR();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string[] result = openFileDialog.FileNames;

                if (result.Length != 0)
                {
                    foreach (string lFile in result)
                    {
                        FileInfo lFileInfo = new FileInfo(lFile);
                        if (!ProjectMngr.getInstance().addMusicToStop(mSelectedRoute,
                                                                      mSelectedStop,
                                                                      lFileInfo.Name))
                        {
                            return;
                        }
                    }

                    updateTreeView();
                }
            }
        }

        private void btnAddMusicToRoute_Click(object sender, EventArgs e)
        {
            if (mSelectedRoute == string.Empty)
            {
                MessageBox.Show("A route must be selected");
                return;
            }
            
            if (!Directory.Exists(Ctes.MUSIC_DIR()))
            {
                MessageBox.Show("Folder " + Ctes.MUSIC_DIR() + " must exist");
                return;
            }

            openFileDialog.FileName = String.Empty;
            openFileDialog.InitialDirectory = Ctes.MUSIC_DIR();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string[] result = openFileDialog.FileNames;
                if (result.Length != 0)
                {
                    foreach (string lMusicFileName in result)
                    {
                        FileInfo lFileInfo = new FileInfo(lMusicFileName);
                        if (!ProjectMngr.getInstance().addMusicToRoute(mSelectedRoute,
                                                                       lFileInfo.Name))
                        {
                            return;
                        }
                    }

                    updateTreeView();
                }
            }
        }

        private void btnAddEloc_Click(object sender, EventArgs e)
        {
            if (textBoxElocutionName.Text == string.Empty)
            {
                MessageBox.Show("A commentary name must be supplied");
                return;
            }
            
            if (mSelectedStop == string.Empty)
            {
                MessageBox.Show("A stop node must be selected");
                return;
            }

            if (!Directory.Exists(Ctes.COMMENTARIES_DIR()))
            {
                MessageBox.Show("Folder " + Ctes.COMMENTARIES_DIR() + " must exist");
                return;
            }
            
            if (ProjectMngr.getInstance().checkCommentExists(mSelectedRoute, 
                                                             mSelectedStop,
                                                             textBoxElocutionName.Text))
            {
                MessageBox.Show("The commentary already existed");
                return;
            }

            //openFileDialog.InitialDirectory = Ctes.COMMENTARIES_DIR;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string lFileName = openFileDialog.FileName;
                if (lFileName == string.Empty)
                {
                    MessageBox.Show("A commentary file name must be selected");
                    return;
                }

                FileInfo lFileInfo = new FileInfo(lFileName);

                string lDirName = lFileInfo.Directory.Name;
                if (lDirName.All(c => !char.IsDigit(c)))
                { // the directory does not contain any digit value. It should be for example: 01English; 02Spanish; etc.
                    MessageBox.Show("The directory " + lDirName + " should contain a digit");
                    return;
                }
                
                if (ProjectMngr.getInstance().addElocutionToStop(mSelectedRoute,
                                                             mSelectedStop,
                                                             textBoxElocutionName.Text,
                                                             lFileInfo.Name,
                                                             checkBoxAutoplay.Checked))
                {
                    updateTreeView();
                    textBoxElocutionName.Text = string.Empty;
                }
            }
        }
                
        public string getOperatorName()
        {
            return TXTB_OperatorName.Text;
        }

        private TreeNode m_SelectedNode; // needed for renaming node purposes

        private string mSelectedRoute;
        private string mSelectedStop;
        private string mSelectedComment;
        private string mSelectedMusic;

        private void treeView_MouseClick(object sender, MouseEventArgs e)
        {
            // Point where the mouse is clicked.
            Point p = new Point(e.X, e.Y);

            // Get the node that the user has clicked.
            TreeNode node = treeView.GetNodeAt(p);
            if (node != null)
            {
                m_SelectedNode = treeView.SelectedNode = node;

                if (e.Button == MouseButtons.Right)
                {
                    // only show the edit elocution tool strip for the elocution node
                    editElocutionToolStripMenuItem.Visible =
                        (node.Level == 2 && node.Text.Contains("commentary"));

                    renameToolStripMenuItem.Visible = (node.Level >= 0 && node.Level <=2);

                    contextMenuStrip.Show(treeView, p);
                    treeView.SelectedNode.ContextMenuStrip = contextMenuStrip;               
                }                
            }
        }
        
        // only an elocution or music node can be removed by this method
        private void RemoveItem_Click(object sender, EventArgs e)
        {
            if (mSelectedRoute != string.Empty && 
                mSelectedStop == string.Empty &&
                mSelectedComment == string.Empty) // route
            {
                ProjectMngr.getInstance().removeRoute(mSelectedRoute);
            }
            else if (mSelectedRoute != string.Empty &&
                     mSelectedStop != string.Empty &&
                     mSelectedComment == string.Empty &&
                     mSelectedMusic == string.Empty) // stop
            {
                ProjectMngr.getInstance().removeStop(mSelectedRoute, mSelectedStop);
            }
            else if (mSelectedRoute != string.Empty &&
                     mSelectedStop != string.Empty &&
                     (mSelectedComment != string.Empty ||
                      mSelectedMusic != string.Empty)) // elocution or music
            {
                bool lIsElocution = mSelectedMusic == string.Empty;
                
                if (lIsElocution)
                {
                    ProjectMngr.getInstance().removeElocution(mSelectedRoute, 
                                                              mSelectedStop, 
                                                              mSelectedComment);
                }
                else
                {
                    ProjectMngr.getInstance().removeMusic(mSelectedRoute,
                                                          mSelectedStop,
                                                          mSelectedMusic);
                }
            }

            updateTreeView();
        }

        private void autoplayAutostopToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            ProjectMngr.getInstance().toggleAutoplayAutostop(mSelectedRoute, 
                                                             mSelectedStop,
                                                             mSelectedComment); 
            updateTreeView();
        }
        
        private void moveUPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            up_or_Down(true /*up*/);
        }

        private void MoveDown_Click(object sender, EventArgs e)
        {
            up_or_Down(false /*down*/);
        }

        private void up_or_Down(bool abUp)
        {
            if (mSelectedRoute != string.Empty &&
                mSelectedStop != string.Empty &&
                mSelectedComment == string.Empty) // stop
            {
                ProjectMngr.getInstance().moveStop_up_or_down(mSelectedRoute, mSelectedStop, abUp);
            }
            else if (mSelectedRoute != string.Empty &&
                mSelectedStop != string.Empty &&
                mSelectedComment != string.Empty) // elocution or music
            {
                ProjectMngr.getInstance().moveEloc_up_or_down(mSelectedRoute,
                                                              mSelectedStop,
                                                              mSelectedComment,
                                                              abUp);
            }

            updateTreeView();
        }

        private void restoreChannelOrderGain()
        {
            int[] lChnOrd = ProjectMngr.getInstance().getChannelOrderAsVector();

            comboBox1.SelectedIndex = lChnOrd[0] % comboBox1.Items.Count;
            comboBox2.SelectedIndex = lChnOrd[1] % comboBox2.Items.Count;
            comboBox3.SelectedIndex = lChnOrd[2] % comboBox3.Items.Count;
            comboBox4.SelectedIndex = lChnOrd[3] % comboBox4.Items.Count;
            comboBox5.SelectedIndex = lChnOrd[4] % comboBox5.Items.Count;
            comboBox6.SelectedIndex = lChnOrd[5] % comboBox6.Items.Count;
            comboBox7.SelectedIndex = lChnOrd[6] % comboBox7.Items.Count;
            comboBox8.SelectedIndex = lChnOrd[7] % comboBox8.Items.Count;
            comboBox9.SelectedIndex = lChnOrd[8] % comboBox9.Items.Count;
            comboBox10.SelectedIndex = lChnOrd[9] % comboBox10.Items.Count;
            comboBox11.SelectedIndex = lChnOrd[10] % comboBox11.Items.Count;
            comboBox12.SelectedIndex = lChnOrd[11] % comboBox12.Items.Count;
            comboBox13.SelectedIndex = lChnOrd[12] % comboBox13.Items.Count;
            comboBox14.SelectedIndex = lChnOrd[13] % comboBox14.Items.Count;
            comboBox15.SelectedIndex = lChnOrd[14] % comboBox15.Items.Count;
            comboBox16.SelectedIndex = lChnOrd[15] % comboBox16.Items.Count;

            int[] lChnGain = ProjectMngr.getInstance().getChannelGainAsVector();
            numericUpDown1.Value = lChnGain[0];
            numericUpDown2.Value = lChnGain[1];
            numericUpDown3.Value = lChnGain[2];
            numericUpDown4.Value = lChnGain[3];
            numericUpDown5.Value = lChnGain[4];
            numericUpDown6.Value = lChnGain[5];
            numericUpDown7.Value = lChnGain[6];
            numericUpDown8.Value = lChnGain[7];
            numericUpDown9.Value = lChnGain[8];
            numericUpDown10.Value = lChnGain[9];
            numericUpDown11.Value = lChnGain[10];
            numericUpDown12.Value = lChnGain[11];
            numericUpDown13.Value = lChnGain[12];
            numericUpDown14.Value = lChnGain[13];
            numericUpDown15.Value = lChnGain[14];
            numericUpDown16.Value = lChnGain[15];            
        }
        
        private void saveChannelOrderInProj()
        {
            int[] lChn = new int[16];

            lChn[0] = comboBox1.SelectedIndex;
            lChn[1] = comboBox2.SelectedIndex;
            lChn[2] = comboBox3.SelectedIndex;
            lChn[3] = comboBox4.SelectedIndex;
            lChn[4] = comboBox5.SelectedIndex;
            lChn[5] = comboBox6.SelectedIndex;
            lChn[6] = comboBox7.SelectedIndex;
            lChn[7] = comboBox8.SelectedIndex;
            lChn[8] = comboBox9.SelectedIndex;
            lChn[9] = comboBox10.SelectedIndex;
            lChn[10] = comboBox11.SelectedIndex;
            lChn[11] = comboBox12.SelectedIndex;
            lChn[12] = comboBox13.SelectedIndex;
            lChn[13] = comboBox14.SelectedIndex;
            lChn[14] = comboBox15.SelectedIndex;
            lChn[15] = comboBox16.SelectedIndex;

            ProjectMngr.getInstance().setChannelOrder(lChn);     
        }
          
        private void saveChannelGainInProj()
        {
            ProjectMngr.getInstance().setChannelGain(Convert.ToInt32(numericUpDown1.Value),
                                                    Convert.ToInt32(numericUpDown2.Text),
                                                    Convert.ToInt32(numericUpDown3.Text),
                                                    Convert.ToInt32(numericUpDown4.Text),
                                                    Convert.ToInt32(numericUpDown5.Text),
                                                    Convert.ToInt32(numericUpDown6.Text),
                                                    Convert.ToInt32(numericUpDown7.Text),
                                                    Convert.ToInt32(numericUpDown8.Text),
                                                    Convert.ToInt32(numericUpDown9.Text),
                                                    Convert.ToInt32(numericUpDown10.Text),
                                                    Convert.ToInt32(numericUpDown11.Text),
                                                    Convert.ToInt32(numericUpDown12.Text),
                                                    Convert.ToInt32(numericUpDown13.Text),
                                                    Convert.ToInt32(numericUpDown14.Text),
                                                    Convert.ToInt32(numericUpDown15.Text),
                                                    Convert.ToInt32(numericUpDown16.Text));
        }

        private void adjust_MTG_ComboChannelsVisibility()
        {
            comboBoxMTGRightCh1.Visible = comboBoxMTGLeftCh1.Visible = labelMTG_CH1.Visible = (numMTGChannels.Value >= 1);
            comboBoxMTGRightCh2.Visible = comboBoxMTGLeftCh2.Visible = labelMTG_CH2.Visible = (numMTGChannels.Value >= 2);
            comboBoxMTGRightCh3.Visible = comboBoxMTGLeftCh3.Visible = labelMTG_CH3.Visible = (numMTGChannels.Value >= 3);
            comboBoxMTGRightCh4.Visible = comboBoxMTGLeftCh4.Visible = labelMTG_CH4.Visible = (numMTGChannels.Value >= 4);
            comboBoxMTGRightCh5.Visible = comboBoxMTGLeftCh5.Visible = labelMTG_CH5.Visible = (numMTGChannels.Value >= 5);
            comboBoxMTGRightCh6.Visible = comboBoxMTGLeftCh6.Visible = labelMTG_CH6.Visible = (numMTGChannels.Value >= 6);
            comboBoxMTGRightCh7.Visible = comboBoxMTGLeftCh7.Visible = labelMTG_CH7.Visible = (numMTGChannels.Value >= 7);
            comboBoxMTGRightCh8.Visible = comboBoxMTGLeftCh8.Visible = labelMTG_CH8.Visible = (numMTGChannels.Value >= 8);
            comboBoxMTGRightCh9.Visible = comboBoxMTGLeftCh9.Visible = labelMTG_CH9.Visible = (numMTGChannels.Value >= 9);
            comboBoxMTGRightCh10.Visible = comboBoxMTGLeftCh10.Visible = labelMTG_CH10.Visible = (numMTGChannels.Value >= 10);
            comboBoxMTGRightCh11.Visible = comboBoxMTGLeftCh11.Visible = labelMTG_CH11.Visible = (numMTGChannels.Value >= 11);
            comboBoxMTGRightCh12.Visible = comboBoxMTGLeftCh12.Visible = labelMTG_CH12.Visible = (numMTGChannels.Value >= 12);
            comboBoxMTGRightCh13.Visible = comboBoxMTGLeftCh13.Visible = labelMTG_CH13.Visible = (numMTGChannels.Value >= 13);
            comboBoxMTGRightCh14.Visible = comboBoxMTGLeftCh14.Visible = labelMTG_CH14.Visible = (numMTGChannels.Value >= 14);
            comboBoxMTGRightCh15.Visible = comboBoxMTGLeftCh15.Visible = labelMTG_CH15.Visible = (numMTGChannels.Value >= 15);
            comboBoxMTGRightCh16.Visible = comboBoxMTGLeftCh16.Visible = labelMTG_CH16.Visible = (numMTGChannels.Value >= 16);

            ProjectMngr.getInstance().setNumPhisicalChannels(Convert.ToInt32(numMTGChannels.Value));
        }

        private void numMTGChannels_ValueChanged(object sender, EventArgs e)
        {
            adjust_MTG_ComboChannelsVisibility();
        }

        private void numLanguages_ValueChanged(object sender, EventArgs e)
        {
            ProjectMngr.getInstance().setNumLanguages(Convert.ToInt32(numLanguages.Value));

            // Labels
            labelNumCh1.Visible = (numLanguages.Value >= 1);
            labelNumCh2.Visible = (numLanguages.Value >= 2);
            labelNumCh3.Visible = (numLanguages.Value >= 3);
            labelNumCh4.Visible = (numLanguages.Value >= 4);
            labelNumCh5.Visible = (numLanguages.Value >= 5);
            labelNumCh6.Visible = (numLanguages.Value >= 6);
            labelNumCh7.Visible = (numLanguages.Value >= 7);
            labelNumCh8.Visible = (numLanguages.Value >= 8);
            labelNumCh9.Visible = (numLanguages.Value >= 9);
            labelNumCh10.Visible = (numLanguages.Value >= 10);
            labelNumCh11.Visible = (numLanguages.Value >= 11);
            labelNumCh12.Visible = (numLanguages.Value >= 12);
            labelNumCh13.Visible = (numLanguages.Value >= 13);
            labelNumCh14.Visible = (numLanguages.Value >= 14);
            labelNumCh15.Visible = (numLanguages.Value >= 15);
            labelNumCh16.Visible = (numLanguages.Value >= 16);

            // comboboxes
            comboBox1.Visible = (numLanguages.Value >= 1);
            comboBox2.Visible = (numLanguages.Value >= 2);
            comboBox3.Visible = (numLanguages.Value >= 3);
            comboBox4.Visible = (numLanguages.Value >= 4);
            comboBox5.Visible = (numLanguages.Value >= 5);
            comboBox6.Visible = (numLanguages.Value >= 6);
            comboBox7.Visible = (numLanguages.Value >= 7);
            comboBox8.Visible = (numLanguages.Value >= 8);
            comboBox9.Visible = (numLanguages.Value >= 9);
            comboBox10.Visible = (numLanguages.Value >= 10);
            comboBox11.Visible = (numLanguages.Value >= 11);
            comboBox12.Visible = (numLanguages.Value >= 12);
            comboBox13.Visible = (numLanguages.Value >= 13);
            comboBox14.Visible = (numLanguages.Value >= 14);
            comboBox15.Visible = (numLanguages.Value >= 15);
            comboBox16.Visible = (numLanguages.Value >= 16);

            // up-down components
            numericUpDown1.Visible = (numLanguages.Value >= 1);
            numericUpDown2.Visible = (numLanguages.Value >= 2);
            numericUpDown3.Visible = (numLanguages.Value >= 3);
            numericUpDown4.Visible = (numLanguages.Value >= 4);
            numericUpDown5.Visible = (numLanguages.Value >= 5);
            numericUpDown6.Visible = (numLanguages.Value >= 6);
            numericUpDown7.Visible = (numLanguages.Value >= 7);
            numericUpDown8.Visible = (numLanguages.Value >= 8);
            numericUpDown9.Visible = (numLanguages.Value >= 9);
            numericUpDown10.Visible = (numLanguages.Value >= 10);
            numericUpDown11.Visible = (numLanguages.Value >= 11);
            numericUpDown12.Visible = (numLanguages.Value >= 12);
            numericUpDown13.Visible = (numLanguages.Value >= 13);
            numericUpDown14.Visible = (numLanguages.Value >= 14);
            numericUpDown15.Visible = (numLanguages.Value >= 15);
            numericUpDown16.Visible = (numLanguages.Value >= 16);
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_SelectedNode.Level < 0 || m_SelectedNode.Level > 2)
                return;

            // first of all, remove the beginning ("stop :" or "route :") so that
            // the user only edits the node's name without this tag.
            string lTxt = m_SelectedNode.Text;
            m_SelectedNode.Text = lTxt.Substring(lTxt.IndexOf(": ") + 2);

            m_SelectedNode.BeginEdit();
        }
                
        private void treeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Label != null)
            {
                if (e.Label.Length > 0)
                {
                    if (e.Label.IndexOfAny(new char[] { '@', '.', ',', '!' }) == -1)
                    {
                        if (e.Label != "")
                        {                                
                            mNewTxtSelectedNode = "";

                            switch(e.Node.Level)
                            {
                                case 0 /*route*/:
                                    {
                                    string lPrevName = e.Node.Name;                                        
                                    string lNewName = e.Label;

                                    mNewTxtSelectedNode = "route: " + e.Label;

                                    ProjectMngr.getInstance().modifyRouteName(lPrevName, 
                                                                              lNewName);
                                    }
                                    break;
                                case 1 /*stop*/:
                                    {                                     
                                    string lPrevStopName = e.Node.Name;
                                    string lNewName = e.Label;

                                    string lRouteName = e.Node.Parent.Name;

                                    mNewTxtSelectedNode = "stop: " + e.Label;

                                    ProjectMngr.getInstance().modifyStopName(lRouteName,
                                                                             lPrevStopName,
                                                                             lNewName);
                                    }
                                    break;
                                case 2 /*commentary*/:
                                default:                                   
                                    {
                                        string lRouteName = e.Node.Parent.Parent.Name;
                                        string lStopName = e.Node.Parent.Name;
                                        string lPrevCommentName = e.Node.Name;

                                        string lNewCommentName = e.Label;

                                        mNewTxtSelectedNode = "commentary: " + e.Label;

                                        ProjectMngr.getInstance().modifyCommentaryName(lRouteName,
                                                                                       lStopName,
                                                                                       lPrevCommentName,
                                                                                       lNewCommentName);
                                    }
                                    break;
                            }
                            
                            e.Node.EndEdit(false);

                            this.BeginInvoke(new Action(() => afterAfterEdit(e.Node)));                            
                        }
                    }
                    else
                    {
                        /* Cancel the label edit action, inform the user, and 
                           place the node in edit mode again. */
                        e.CancelEdit = true;
                        MessageBox.Show("Invalid tree node label.\n" +
                           "The invalid characters are: '@','.', ',', '!'",
                           "Node Label Edit");
                        e.Node.BeginEdit();
                    }
                }
                else
                {
                    /* Cancel the label edit action, inform the user, and 
                       place the node in edit mode again. */
                    e.CancelEdit = true;
                    MessageBox.Show("Invalid tree node label.\nThe label cannot be blank",
                       "Node Label Edit");
                    e.Node.BeginEdit();
                }
            }

        }

        string mNewTxtSelectedNode = "";
        private void afterAfterEdit(TreeNode node)
        {
            node.Text = mNewTxtSelectedNode;
            // the 'Name' attibute is used to identify the node. It's important to also update its value.
            node.Name = mNewTxtSelectedNode.Substring(mNewTxtSelectedNode.IndexOf(':') + 2);
        }

        private void MoveTop_Click(object sender, EventArgs e)
        {
            top_or_Bottom(true /*lbTop*/);
        }

        private void MoveBottom_Click(object sender, EventArgs e)
        {
            top_or_Bottom(false /*lbTop*/);
        }

        private void top_or_Bottom(bool lbTop)
        {
            if (mSelectedRoute != string.Empty &&
                mSelectedStop != string.Empty &&
                mSelectedComment == string.Empty) // stop
            {
                ProjectMngr.getInstance().moveStop_top_or_bottom(mSelectedRoute, mSelectedStop, lbTop);
            }
            else if (mSelectedRoute != string.Empty &&
                     mSelectedStop != string.Empty &&
                     mSelectedComment != string.Empty) // elocution or music
            {
                ProjectMngr.getInstance().moveEloc_top_or_bottom(mSelectedRoute,
                                                                 mSelectedStop,
                                                                 mSelectedComment,
                                                                 lbTop);
            }

            updateTreeView();
        }

        private void numericUpDownMusicVol_ValueChanged(object sender, EventArgs e)
        {
            ProjectMngr.getInstance().setMusicGain(Convert.ToInt32(numericUpDownMusicVol.Value));
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            mSelectedMusic = string.Empty;

            if (treeView.SelectedNode.Level == 0)
            {
                labelSelectedRoute.Text = treeView.SelectedNode.Name;
                labelSelectedStop.Text = string.Empty;
                labelSelectedComment.Text = string.Empty;
            }
            else if (treeView.SelectedNode.Level == 1)
            {
                labelSelectedRoute.Text = treeView.SelectedNode.Parent.Name;
                labelSelectedStop.Text = treeView.SelectedNode.Name; 
                labelSelectedComment.Text = string.Empty;
            }
            else if (treeView.SelectedNode.Level == 2)
            {
                labelSelectedRoute.Text = treeView.SelectedNode.Parent.Parent.Name;
                labelSelectedStop.Text = treeView.SelectedNode.Parent.Name;

                if (treeView.SelectedNode.Text.Contains("commentary"))
                {
                    labelSelectedComment.Text = treeView.SelectedNode.Name;
                }
                else // the selected node is a music node
                {
                    labelSelectedComment.Text = string.Empty;
                    mSelectedMusic = treeView.SelectedNode.Name;
                }
            }
            else if (treeView.SelectedNode.Level == 3)
            {
                labelSelectedRoute.Text = treeView.SelectedNode.Parent.Parent.Parent.Name;
                labelSelectedStop.Text = treeView.SelectedNode.Parent.Parent.Name;
                labelSelectedComment.Text = treeView.SelectedNode.Parent.Name;
            }

            mSelectedRoute = labelSelectedRoute.Text;
            mSelectedStop = labelSelectedStop.Text;
            mSelectedComment = labelSelectedComment.Text;

            if (treeView.SelectedNode.Level >= 2 && 
                mSelectedMusic == string.Empty)
            { // a comment node has been selected and no music node is selected.
                showGpsPosition();
            }
        }
               
        private void btnNewProject_Click(object sender, EventArgs e)
        {
            if (!ProjectMngr.getInstance().isProjSaved())
            {
                MessageBox.Show("The previous project is not saved");
                return;
            }

            NewProjForm lNewProjForm = new NewProjForm();
            lNewProjForm.ShowDialog();
            labelProjectName.Text = lNewProjForm.ProjectName;
        }

        private void btnLoadProject_Click(object sender, EventArgs e)
        {
            if (!ProjectMngr.getInstance().isProjSaved())
            {
                MessageBox.Show("The previous project is not saved");
                return;
            }

            //openFileDialog.Filter = ".jsn | project files";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Ctes.CURRENT_PROJECT_FILE = openFileDialog.FileName;
                if (loadProject())
                {
                    MessageBox.Show("Project loaded");
                }
            }
        }

        string[] mAvailableLanguages;
        private bool loadProject()
        {
            string lProjName = Path.GetFileNameWithoutExtension(Ctes.CURRENT_PROJECT_FILE);
            ProjectMngr.getInstance().loadProjectFromFile(Ctes.CURRENT_PROJECT_FILE, lProjName);

            adjust_MTG_ComboChannelsVisibility();

            labelProjectName.Text = lProjName;
            ProjectMngr.getInstance().setProjectName(lProjName);

            if (!Directory.Exists(Ctes.COMMENTARIES_DIR()))
            {
                MessageBox.Show("The folder " + Ctes.COMMENTARIES_DIR() + " should exist within the RouteMaker's folder");
                return false;
            }

            if (!Directory.Exists("output"))
            {
                MessageBox.Show("The folder 'output' should exist within the RouteMaker's folder");
                return false;
            }

            mAvailableLanguages = Directory.GetDirectories(Ctes.COMMENTARIES_DIR());
            if (mAvailableLanguages.Length == 0) 
            {
                Directory.CreateDirectory(Ctes.COMMENTARIES_DIR() + "00 LANGUAGE");
                mAvailableLanguages = Directory.GetDirectories(Ctes.COMMENTARIES_DIR());
            }

            #region adding MTG languages
            List<string>[] lMTGLangs = new List<string>[32];

            for (int i = 0; i < 32; i++)
            {
                lMTGLangs[i] = new List<string>();
                foreach (string lFolderName in mAvailableLanguages)
                {
                    lMTGLangs[i].Add(lFolderName.Substring(lFolderName.LastIndexOf(Ctes.COMMENTARIES_DIR()) + 
                        Ctes.COMMENTARIES_DIR().Length));
                }
            }

            // each comboBox element must have its own datasource object
            comboBoxMTGLeftCh1.DataSource = lMTGLangs[0];
            comboBoxMTGLeftCh2.DataSource = lMTGLangs[1];
            comboBoxMTGLeftCh3.DataSource = lMTGLangs[2];
            comboBoxMTGLeftCh4.DataSource = lMTGLangs[3];
            comboBoxMTGLeftCh5.DataSource = lMTGLangs[4];
            comboBoxMTGLeftCh6.DataSource = lMTGLangs[5];
            comboBoxMTGLeftCh7.DataSource = lMTGLangs[6];
            comboBoxMTGLeftCh8.DataSource = lMTGLangs[7];
            comboBoxMTGLeftCh9.DataSource = lMTGLangs[8];
            comboBoxMTGLeftCh10.DataSource = lMTGLangs[9];
            comboBoxMTGLeftCh11.DataSource = lMTGLangs[10];
            comboBoxMTGLeftCh12.DataSource = lMTGLangs[11];
            comboBoxMTGLeftCh13.DataSource = lMTGLangs[12];
            comboBoxMTGLeftCh14.DataSource = lMTGLangs[13];
            comboBoxMTGLeftCh15.DataSource = lMTGLangs[14];
            comboBoxMTGLeftCh16.DataSource = lMTGLangs[15];

            comboBoxMTGRightCh1.DataSource = lMTGLangs[16];
            comboBoxMTGRightCh2.DataSource = lMTGLangs[17];
            comboBoxMTGRightCh3.DataSource = lMTGLangs[18];
            comboBoxMTGRightCh4.DataSource = lMTGLangs[19];
            comboBoxMTGRightCh5.DataSource = lMTGLangs[20];
            comboBoxMTGRightCh6.DataSource = lMTGLangs[21];
            comboBoxMTGRightCh7.DataSource = lMTGLangs[22];
            comboBoxMTGRightCh8.DataSource = lMTGLangs[23];
            comboBoxMTGRightCh9.DataSource = lMTGLangs[24];
            comboBoxMTGRightCh10.DataSource = lMTGLangs[25];
            comboBoxMTGRightCh11.DataSource = lMTGLangs[26];
            comboBoxMTGRightCh12.DataSource = lMTGLangs[27];
            comboBoxMTGRightCh13.DataSource = lMTGLangs[28];
            comboBoxMTGRightCh14.DataSource = lMTGLangs[29];
            comboBoxMTGRightCh15.DataSource = lMTGLangs[30];
            comboBoxMTGRightCh16.DataSource = lMTGLangs[31];
            
            #endregion

            List<string>[] lLangs = new List<string>[16];

            for (int i = 0; i < 16; i++)
            {
                lLangs[i] = new List<string>();
                foreach (string lFolderName in mAvailableLanguages)
                {
                    lLangs[i].Add(lFolderName.Substring(lFolderName.LastIndexOf(Ctes.COMMENTARIES_DIR()) + Ctes.COMMENTARIES_DIR().Length + 3));
                }
            }
            
            // each combo component should have its own datasource object 
            // so that each one can change its values without impacting the other combos values.
            comboBox1.DataSource = lLangs[0];
            comboBox2.DataSource = lLangs[1];
            comboBox3.DataSource = lLangs[2];
            comboBox4.DataSource = lLangs[3];
            comboBox5.DataSource = lLangs[4];
            comboBox6.DataSource = lLangs[5];
            comboBox7.DataSource = lLangs[6];
            comboBox8.DataSource = lLangs[7];
            comboBox9.DataSource = lLangs[8];
            comboBox10.DataSource = lLangs[9];
            comboBox11.DataSource = lLangs[10];
            comboBox12.DataSource = lLangs[11];
            comboBox13.DataSource = lLangs[12];
            comboBox14.DataSource = lLangs[13];
            comboBox15.DataSource = lLangs[14];
            comboBox16.DataSource = lLangs[15];
            
            numLanguages.Value = ProjectMngr.getInstance().getNumLanguages();
            updateTreeView();
            restoreChannelOrderGain();
            numericUpDownMusicVol.Value = ProjectMngr.getInstance().getMusicGain();

            cmbBoxModuleType.SelectedIndex = ProjectMngr.getInstance().getSelectedModuleIndex();

            int[] lChnOrder = new int[16];
            for (int i = 0; i < 16; i++)
            {
                lChnOrder[i] = i;
            }

            ProjectMngr.getInstance().setChannelOrder(lChnOrder);

            comboBox1.SelectedIndex = lChnOrder[0] % comboBox1.Items.Count;
            comboBox2.SelectedIndex = lChnOrder[1] % comboBox2.Items.Count;
            comboBox3.SelectedIndex = lChnOrder[2] % comboBox3.Items.Count;
            comboBox4.SelectedIndex = lChnOrder[3] % comboBox4.Items.Count;
            comboBox5.SelectedIndex = lChnOrder[4] % comboBox5.Items.Count;
            comboBox6.SelectedIndex = lChnOrder[5] % comboBox6.Items.Count;
            comboBox7.SelectedIndex = lChnOrder[6] % comboBox7.Items.Count;
            comboBox8.SelectedIndex = lChnOrder[7] % comboBox8.Items.Count;
            comboBox9.SelectedIndex = lChnOrder[8] % comboBox9.Items.Count;
            comboBox10.SelectedIndex = lChnOrder[9] % comboBox10.Items.Count;
            comboBox11.SelectedIndex = lChnOrder[10] % comboBox11.Items.Count;
            comboBox12.SelectedIndex = lChnOrder[11] % comboBox12.Items.Count;
            comboBox13.SelectedIndex = lChnOrder[12] % comboBox13.Items.Count;
            comboBox14.SelectedIndex = lChnOrder[13] % comboBox14.Items.Count;
            comboBox15.SelectedIndex = lChnOrder[14] % comboBox15.Items.Count;
            comboBox16.SelectedIndex = lChnOrder[15] % comboBox16.Items.Count;

            TXTB_OperatorName.Text = ProjectMngr.getInstance().getOperatorName();

            labelSelectedRoute.Text = labelSelectedStop.Text =
            labelSelectedComment.Text = mSelectedStop = mSelectedRoute =
            mSelectedComment = string.Empty;

            NUD_MarginElocs.Value = ProjectMngr.getInstance().getMarginElocs();

            return true;
        }

        private void btnExportTo_Click(object sender, EventArgs e)
        {
            string lProjName = ProjectMngr.getInstance().getProjName();
            if (string.IsNullOrEmpty(lProjName))
            {
                MessageBox.Show("A project must be loaded");
                return;
            }

            new ExportToForm().Show();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            loadProject();
        }

        private void btnFormat_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process secondProc = new System.Diagnostics.Process();
                secondProc.StartInfo.FileName = "format_tool.exe";
                secondProc.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception running format tool [" + ex.Message + "]. Run this app as admin");
            }
        }

        private void cmbBoxModuleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lProjName = ProjectMngr.getInstance().getProjName();
            if(!string.IsNullOrEmpty(lProjName))
            {
                ProjectMngr.getInstance().setSelectedModuleIndex(cmbBoxModuleType.SelectedIndex);
            }
        }

        /// <summary>
        /// Just imports the POSGPS.inf file info and leaves the other info untouched.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
         
        private void BTN_ImportGpsInf_Click(object sender, EventArgs e)
        {
            // clear all previous GPS data
            updateInfoFromInfs(true);
        }

        private void BTN_ImportInfs_Click(object sender, EventArgs e)
        {
            updateInfoFromInfs();
        }

        /// <summary>
        /// Updates the project metadata from the .INF files
        /// </summary>
        /// <param name="bJustGpsInfo">If true, just import the gps data from the POSGPS.inf file</param>
        private void updateInfoFromInfs(bool bJustGpsInfo = false)
        {
            string lProjName = ProjectMngr.getInstance().getProjName();
            if (string.IsNullOrEmpty(lProjName))
            {
                MessageBox.Show("A project should be created before importing .INF files");
                return;
            }

            if (!ProjectMngr.getInstance().isProjectEmpty())
            {
                if (MessageBox.Show("The project has data. Do you want to override it ?",
                                    "The project is not empty",
                                    MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
            }

            if (bJustGpsInfo) // update just GPS info
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (ProjectMngr.getInstance().
                        loadGpsDataFromInf(openFileDialog.FileName))
                    {
                        updateTreeView();
                        MessageBox.Show("GPS info update from 'POSGPS.INF' file");
                    }
                }
            }
            else
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    if (ProjectMngr.getInstance().
                       loadProjectFromInfDir(folderBrowserDialog.SelectedPath))
                    {
                        updateTreeView();
                        TXTB_OperatorName.Text = ProjectMngr.getInstance().getOperatorName();

                        MessageBox.Show("Project loaded from INF");
                    }
                }
            }
        }

        #region Map

        private PointLatLng mSelectedGpsPos;
        private int mSelectedAngle = -1;
        private int mSelectedAngleError;
        private int mSelectedRadious = -1;

        private bool mIsDraggingMarker = false;

        private GMapOverlay mMarkersOverlay = null;
        private GMapOverlay mDirOverlay = null;
        private GMapOverlay mRouteOverlay = null;

        /// <summary>
        /// Marker under the cursor. For drap and drop operations in markers.
        /// </summary>
        private GMapMarker mMarkerUnder;

        private void updatePointLabelInfo()
        {
            string lText = string.Empty;
            if (mSelectedGpsPos != null)
            {
                lText += "GPS Pos: " + Math.Round(mSelectedGpsPos.Lat, 7) +
                    "   " + Math.Round(mSelectedGpsPos.Lng, 7) + " " + Environment.NewLine;
            }

            if (mSelectedAngle != -1)
            {
                lText += mSelectedAngle + " +/- " + mSelectedAngleError + " degrees" + Environment.NewLine;
            }

            if (mSelectedRadious != -1)
            {
                lText += mSelectedRadious + " meters";
            }

            LBL_PointData.Text = lText;
        }

        private void MC_map_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                // adding a new position marker
                if (!mIsDraggingMarker && mMarkerUnder == null)
                {
                    double lLon = MC_map.FromLocalToLatLng(e.X, e.Y).Lng;
                    double lLat = MC_map.FromLocalToLatLng(e.X, e.Y).Lat;

                    mSelectedGpsPos = new PointLatLng(lLat, lLon);
                    GMarkerGoogle marker = new GMarkerGoogle(mSelectedGpsPos, GMarkerGoogleType.red_dot);

                    // only one marker should be visible at any time (in edition mode)
                    mMarkersOverlay.Markers.Clear();
                    mMarkersOverlay.Markers.Add(marker);

                    mDirOverlay.Markers.Clear();
                    
                    mRouteOverlay.Polygons.Clear();

                    showDirectionMarkers(mSelectedGpsPos);
                }

                // clicking on a direction marker
                else if (mMarkerUnder != null)
                {
                    if (mMarkerUnder.Overlay.Id == Ctes.MARKERS_DIRECTION_OVERLAY)
                    {
                        mSelectedAngle = (int)mMarkerUnder.Tag; // in tag we stored the direction/angle.

                        // select the radious and angle error.                    
                        CMS_MapMarkers.Visible = true;
                        CB_Radius.SelectedItem = "20";
                        CB_AngleError.SelectedItem = "40";
                        CMS_MapMarkers.Show(Cursor.Position);

                        selectedAngleToolStripMenuItem.Text = "Selected angle: " +
                                    Convert.ToString(mSelectedAngle) + " degrees";
                    }
                    else if (mMarkerUnder.Overlay.Id == Ctes.MARKERS_OVERLAY)
                    {
                        // select the corresponding comment node in treeView
                        
                        KeyValuePair<Elocution, TreeNode> lMarkerTreeNodeEloc = 
                            mElocsWithGps.FirstOrDefault(el => el.Key.GpsLat == mMarkerUnder.Position.Lat &&
                                                      el.Key.GpsLong == mMarkerUnder.Position.Lng);
                        
                        if (lMarkerTreeNodeEloc.Key != null && lMarkerTreeNodeEloc.Value != null)
                        {
                            treeView.SelectedNode = lMarkerTreeNodeEloc.Value; // TreeNode
                            treeView.SelectedNode.EnsureVisible();
                            treeView.SelectedNode.ForeColor = Color.Blue;

                            mSelectedGpsPos = new PointLatLng(mMarkerUnder.Position.Lat, 
                                                              mMarkerUnder.Position.Lng);

                            mSelectedAngle = lMarkerTreeNodeEloc.Key.direction;
                            mSelectedAngleError = lMarkerTreeNodeEloc.Key.angleError;
                            mSelectedRadious = lMarkerTreeNodeEloc.Key.radio;
                        }
                    }
                }

                updatePointLabelInfo();
            }
        }

        private void BTN_Go_Click(object sender, EventArgs e)
        {
            goMapToKeywordPosition();
        }

        private void TXTB_Location_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                goMapToKeywordPosition();
            }
        }

        private void goMapToKeywordPosition()
        {
            MC_map.MapProvider = GMap.NET.MapProviders.BingMapProvider.Instance;
            GMaps.Instance.Mode = AccessMode.ServerOnly;
            MC_map.SetPositionByKeywords(TXTB_Location.Text);
        }

        /// <summary>
        /// Assign the GPS position to the currently selected elocution.
        /// </summary>        
        private void BTN_AssignPosToCmt_Click(object sender, EventArgs e)
        {
            if (mSelectedGpsPos != null)
            {
                if (mSelectedRoute == string.Empty)
                {
                    MessageBox.Show("A route should be selected");
                    return;
                }

                if (mSelectedStop == string.Empty)
                {
                    MessageBox.Show("A stop should be selected");
                    return;
                }

                if (mSelectedComment == string.Empty)
                {
                    MessageBox.Show("A comment should be selected");
                    return;
                }

                Elocution lEloc = ProjectMngr.getInstance().setGpsToElocution(mSelectedRoute,
                                      mSelectedStop,
                                      mSelectedComment,
                                      mSelectedGpsPos,
                                      mSelectedAngle,
                                      mSelectedAngleError,
                                      mSelectedRadious);
                if (lEloc != null)
                {
                    updateElocInTreeView(lEloc);
                }
            }
        }

        private void MC_map_OnMarkerEnter(GMapMarker item)
        {
            if (!mIsDraggingMarker)
            {
                mMarkerUnder = item;
            }
        }

        private void MC_map_OnMarkerLeave(GMapMarker item)
        {
            if (!mIsDraggingMarker)
            {
                mMarkerUnder = null;
            }
        }

        private void MC_map_MouseDown(object sender, MouseEventArgs e)
        {
            if (mMarkerUnder != null && mMarkerUnder.Overlay.Id == Ctes.MARKERS_OVERLAY)
            {
                mIsDraggingMarker = true;

                mDirOverlay.Markers.Clear(); // clear the dir markers when starting to drag.
            }
        }

        private void MC_map_MouseMove(object sender, MouseEventArgs e)
        {
            if (mIsDraggingMarker && mMarkerUnder != null)
            {
                double lLon = MC_map.FromLocalToLatLng(e.X, e.Y).Lng;
                double lLat = MC_map.FromLocalToLatLng(e.X, e.Y).Lat;

                mSelectedGpsPos = mMarkerUnder.Position = new PointLatLng(lLat, lLon);
            }
        }

        private void MC_map_MouseUp(object sender, MouseEventArgs e)
        {
            if (mIsDraggingMarker)
            {
                showDirectionMarkers(mSelectedGpsPos);
            }

            mIsDraggingMarker = false;
        }

        /// <summary>
        /// Shows a marker with the position of selected Comment if it has GPS position.
        /// </summary>
        private void showGpsPosition()
        {
            if (mSelectedComment == string.Empty)
            {
                MessageBox.Show("A commentary should be selected");
                return;
            }

            Elocution lEloc = ProjectMngr.getInstance().getElocution(mSelectedRoute,
                                                                     mSelectedStop,
                                                                     mSelectedComment);
            if (lEloc != null && lEloc.GpsLat != 0 && lEloc.GpsLong != 0)
            {
                mRouteOverlay.Polygons.Clear();
                mMarkersOverlay.Markers.Clear();
                mDirOverlay.Markers.Clear();

                mSelectedGpsPos = new PointLatLng(lEloc.GpsLat, lEloc.GpsLong);
                GMarkerGoogle marker = new GMarkerGoogle(mSelectedGpsPos, GMarkerGoogleType.red_dot);

                // only one marker should be visible at any time (in edition mode)
                mMarkersOverlay.Markers.Clear();
                mMarkersOverlay.Markers.Add(marker);

                MC_map.Position = mSelectedGpsPos;
            }            
        }

        /// <summary>
        /// Adds direction overlays aroung the point.
        /// </summary>        
        private void showDirectionMarkers(PointLatLng aMarkerPosition)
        {
            GPoint lCenter = MC_map.FromLatLngToLocal(aMarkerPosition);

            // adding 24 markers separated 15 degrees. For direction purposes.                
            int lRadious = 150; // radious in pixels.                
            for (int i = 0; i < 24; i++)
            {
                double lAngle = (15 * Math.PI * i) / 180; // 15 degrees of separation
                PointLatLng lMarkerPoint =
                    MC_map.FromLocalToLatLng((int)lCenter.X + (int)(Math.Sin(lAngle) * lRadious),
                                             (int)lCenter.Y - (int)(Math.Cos(lAngle) * lRadious));

                GMarkerGoogle lMarker = new GMarkerGoogle(lMarkerPoint, GMarkerGoogleType.orange);
                lMarker.Tag = (int)(15 * i); // the tag will be used for getting the direction in OnClick marker..
                mDirOverlay.Markers.Add(lMarker);
            }
        }
        
        private void CB_AngleError_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CB_AngleError.SelectedItem != null)
            {
                mSelectedAngleError = Convert.ToInt32(CB_AngleError.SelectedItem);
                updatePointLabelInfo();
            }
        }

        private void CB_Radious_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CB_Radius.SelectedItem != null)
            {
                mSelectedRadious = Convert.ToInt32(CB_Radius.SelectedItem);
                updatePointLabelInfo();
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CMS_MapMarkers.Visible = false;
            updatePointLabelInfo();
        }

        private void MC_map_OnMapZoomChanged()
        {
            // update the direction markers so that they always are properly separated
            if (mSelectedGpsPos != null)
            {
                mDirOverlay.Markers.Clear();
                showDirectionMarkers(mSelectedGpsPos);
            }
        }
        
        private void BTN_DeletePoint_Click(object sender, EventArgs e)
        {
            if (mSelectedRoute == string.Empty &&
                mSelectedStop == string.Empty &&
                mSelectedComment == string.Empty)
            {
                MessageBox.Show("A commentary should be selected");
                return;
            }

            Elocution lEloc = ProjectMngr.getInstance().getElocution(mSelectedRoute, 
                                                                     mSelectedStop,
                                                                     mSelectedComment);
            if (lEloc != null)
            {
                if (lEloc.GpsLat != 0 && lEloc.GpsLong != 0)
                {
                    lEloc.GpsLat = lEloc.GpsLong = 0;

                    mMarkersOverlay.Markers.Clear();
                    mDirOverlay.Markers.Clear();

                    if (mRouteOverlay.Polygons.Count != 0)
                    { // the route was shown. Then, show again after removing
                        showCurrentRoute();
                    }

                    updateElocInTreeView(lEloc);
                }
            }
        }

        private void BTN_showRoute_Click(object sender, EventArgs e)
        {
            if (mSelectedRoute == string.Empty)
            {
                MessageBox.Show("A Route should be selected");
                return;
            }

            showCurrentRoute();            
        }

        private void showCurrentRoute()
        {
            Route lRoute = ProjectMngr.getInstance().getAllRotues().Find(r => r.RouteName == labelSelectedRoute.Text);
            if (lRoute != null)
            {
                mMarkersOverlay.Markers.Clear();
                mDirOverlay.Markers.Clear();
                mRouteOverlay.Polygons.Clear();

                List<PointLatLng> points = new List<PointLatLng>();

                foreach (Stop lStop in lRoute.Stops)
                {
                    foreach (Elocution lEloc in lStop.Elocutions)
                    {
                        if (lEloc.GpsLat != 0 && lEloc.GpsLong != 0)
                        {
                            PointLatLng lPoint = new PointLatLng(lEloc.GpsLat, lEloc.GpsLong);
                            points.Add(lPoint);

                            GMarkerGoogle marker = new GMarkerGoogle(lPoint, GMarkerGoogleType.red_dot);

                            mMarkersOverlay.Markers.Add(marker);
                        }
                    }
                }

                GMapPolygon polygon = new GMapPolygon(points, "route");

                polygon.Fill = new SolidBrush(Color.FromArgb(0, Color.Red));
                polygon.Stroke = new Pen(Color.Red, 1);
                mRouteOverlay.Polygons.Add(polygon);
            }
        }

        private void NUD_MarginElocs_ValueChanged(object sender, EventArgs e)
        {
            ProjectMngr.getInstance().setMarginElocs(Convert.ToInt32(NUD_MarginElocs.Value));
        }


        #endregion

        #region MTG
        private void handleLeftChannelChanged(object sender, EventArgs e)
        {
            ComboBox lSender = (ComboBox)sender;            
            // the sender's tag contains the index of the combobox (0, 1, ..)
            ProjectMngr.getInstance().setLanguageToLeftChannel(
                Convert.ToInt32(lSender.Tag), 
                Convert.ToString(lSender.SelectedValue));            
        }

        private void handleRightChannelChanged(object sender, EventArgs e)
        {
            ComboBox lSender = (ComboBox)sender;            
            // the sender's tag contains the index of the combobox (0, 1, ..)
            ProjectMngr.getInstance().setLanguageToRightChannel(
                Convert.ToInt32(lSender.Tag),
                Convert.ToString(lSender.SelectedValue));            
        }
        #endregion

    }
}
