﻿using System;
using System.Collections.Generic;

namespace RouteMaker.Model
{
    public class ElocutionBase
    {
        public bool IsAutoplay; // if not autoplay, then it's autostop and vice-versa.
        public string ElocutionName;

        #region GPS 

        public double GpsLong;
        public double GpsLat;
        public int direction;
        public int angleError;
        public int radio;

        public void resetGpsInfo()
        {
            GpsLat = GpsLong = 0;
            direction = angleError = radio = 0;
        }

        #endregion
    }

    public class Elocution : ElocutionBase
    {
        public string ElocFile;
    }

    /// <summary>
    /// Objects of this class will be written in the MTG JSON file
    /// </summary>
    public class ElocutionMTG : ElocutionBase
    {
        public List<string> ElocutionFiles = new List<string>();
    }
}
