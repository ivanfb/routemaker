﻿using System;
using System.Collections.Generic;

namespace RouteMaker.Model
{
    [Serializable()]
    public class Project
    {        
        public string OperatorName;
        public List<Route> Routes = new List<Route>();
        public int[] ChannelOrder = new int[16];
        public int[] ChannelGain = new int[16];
        public int GeneralGain;
        public int MusicGain;
        public int NumLanguages = 4;
        public string ProjectName;
        public int SelectedModuleIndex;

        #region GPS
        public int GpsOn; // 1 - on; 0 - off
        public int ComGps;
        public int ComControl;
        public int MarginElocutions; // threshold to consider that the system is "lost"
        #endregion

        public int NumPhisicalChannels;
        public string[] langLeftChannel = new string[16];
        public string[] langRightChannel = new string[16];
    }

    /// <summary>
    /// Class that will contain the whole structure for the JSON file that will be
    /// parsed b the MTG system
    /// </summary>
    [Serializable()]
    public class MTG_project
    {
        public string RouteMakerVersion; // version of the app that generated the project
        public List<RouteMTG> Routes = new List<RouteMTG>();        
    }
}
