﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteMaker.Model
{
    public class RouteBase
    {
        public string RouteName;
    }

    public class Route : RouteBase
    {        
        public List<Stop> Stops = new List<Stop>();
    }

    public class RouteMTG : RouteBase
    {
        public List<StopMTG> Stops = new List<StopMTG>();
    }
}
