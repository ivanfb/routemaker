﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteMaker.Model
{
    public class StopBase
    {
        public string StopName;
        public List<String> Musics = new List<String>();
    }

    public class Stop : StopBase 
    {       
        public List<Elocution> Elocutions = new List<Elocution>();
    }

    public class StopMTG : StopBase
    {
        public List<ElocutionMTG> Elocutions = new List<ElocutionMTG>();
    }
}
