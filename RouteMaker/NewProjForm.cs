﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteMaker
{
    public partial class NewProjForm : Form
    {
        private string mProjName;

        public NewProjForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (TXTB_ProjName.Text == "")
            {
                MessageBox.Show("Project name cannot be empty");
                return;
            }

            if (ProjectMngr.getInstance().projectExists(TXTB_ProjName.Text))
            {
                MessageBox.Show("A project with this name already exists");
                return;
            }

            ProjectMngr.getInstance().createNewProj(TXTB_ProjName.Text);

            MessageBox.Show("Project created. Now copy the commentaries and music files and press the 'reload' button");

            mProjName = TXTB_ProjName.Text;
            Close();            
        }

        public string ProjectName
        {
            get
            {
                return mProjName;
            }
        }

        private void NewProjForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = TXTB_ProjName;
        }

        private void TXTB_ProjName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                btnOK_Click(this, new EventArgs());
            }
        }
    }
}
