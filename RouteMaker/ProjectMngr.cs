﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Windows.Forms;
using System.IO;

using Newtonsoft.Json;

using RouteMaker.Model;
using GMap.NET;

namespace RouteMaker
{
    static class Swapper
    {
        public static void Swap<T>(this List<T> list, int index1, int index2)
        {
            T temp = list[index1];
            list[index1] = list[index2];
            list[index2] = temp;
        }
    }

    class ProjectMngr
    {
        private bool mProjectSaved = true; // establish project as saved from the beginning

        private Project mProject;

        // keep an instance of the main form, setted at the beginning, in order to access UI values
        private MainForm mMainFormInstance; 

        // singleton
        private static ProjectMngr mInstance;
        public static ProjectMngr getInstance()
        {            
            if (mInstance == null)
            {
                mInstance = new ProjectMngr();
            }
            return mInstance;            
        }

        private ProjectMngr() 
        { 
            mProject = new Project(); // the current project.
            for (int i = 0; i < 16; i++)
            {
                mProject.ChannelGain[i] = 1;
                mProject.ChannelOrder[i] = 1;
            }
        }

        public void loadProjectFromFile(string aFileName,
                                        string aProjName)
        {
            try
            {
                string lJson = File.ReadAllText(aFileName);                
                mProject = JsonConvert.DeserializeObject<Project>(lJson);
                mProject.ProjectName = aProjName;
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception recovering " + e.Message);
            }
        }
        
        public bool loadGpsDataFromInf(string aInfFile)
        {
            // clear all previous GPS info
            mProject.Routes.ForEach(r =>
                r.Stops.ForEach(s =>
                    s.Elocutions.ForEach(e =>
                       e.resetGpsInfo()))
                       );

            #region POSGP.INF
            if (aInfFile.IndexOf("POSGPS", StringComparison.OrdinalIgnoreCase) < 0)
            {
                MessageBox.Show("wrong position gps file name (" + aInfFile + ")" + Environment.NewLine +
                    "It should contain the string 'POSGPS'");
                return false;
            }

            StreamReader lFilePosGps = new StreamReader(aInfFile);
            if (lFilePosGps.EndOfStream)
            {
                MessageBox.Show("The file " + aInfFile + " is empty");
                lFilePosGps.Close();
                return false;
            }

            mProject.GpsOn = Int32.Parse(lFilePosGps.ReadLine());
            mProject.ComGps = Int32.Parse(lFilePosGps.ReadLine());
            mProject.ComControl = Int32.Parse(lFilePosGps.ReadLine());
            mProject.MarginElocutions = Int32.Parse(lFilePosGps.ReadLine());

            string lPosGpsLine;
            while (!lFilePosGps.EndOfStream &&
                   (lPosGpsLine = lFilePosGps.ReadLine()) != "####")
            {
                if (lPosGpsLine.Trim() != string.Empty)
                {
                    string[] lTokens = lPosGpsLine.Split(',');
                    if (lTokens.Length != 14)
                    {
                        lFilePosGps.Close();
                        MessageBox.Show("Incorret posgps line [" + lPosGpsLine + "]");
                        return false;
                    }

                    int lNumRoutePosGps = Int32.Parse(lTokens[0]);
                    int lNumStopPosGps = Int32.Parse(lTokens[1]);
                    int lNumElocPosGps = Int32.Parse(lTokens[2]);
                    int lRadious = Int32.Parse(lTokens[3]);
                    int lDirection = Int32.Parse(lTokens[4]);
                    int lDirectionError = Int32.Parse(lTokens[5]);

                    // lat. decimanl degrees = degrees + minutes/60 + seconds/3600
                    double lLat = Double.Parse(lTokens[6]) +
                                  (Double.Parse(lTokens[7]) / 60) +
                                  (Double.Parse(lTokens[8].Replace('.', ',')) / 3600);

                    char lNS = lTokens[9][0];
                    if (lNS == 'S')
                    {
                        lLat *= -1;
                    }

                    // lon
                    double lLon = Double.Parse(lTokens[10]) +
                                  (Double.Parse(lTokens[11]) / 60) +
                                  (Double.Parse(lTokens[12].Replace('.', ',')) / 3600);

                    char lWE = lTokens[13][0];
                    if (lWE == 'W')
                    {
                        lLon *= -1;
                    }

                    if (!setGpsToElocution(lNumRoutePosGps,
                                             lNumStopPosGps,
                                             lNumElocPosGps,
                                             new PointLatLng(lLat, lLon),
                                             lDirection,
                                             lDirectionError,
                                             lRadious))
                    {
                        MessageBox.Show("Couldn't add gps position from posgps.inf");
                        lFilePosGps.Close();
                        return false;
                    }
                }
            }

            lFilePosGps.Close();
            #endregion

            mProjectSaved = false;

            return true;
        }

        /// <summary>
        /// Loads an recreates the project from an already existing post-production
        /// INF folder (RECORR.INF, *PAR*.INF, POSGPS.INF; etc)
        /// </summary>
        /// <returns>True if loaded correctly</returns>
        public bool loadProjectFromInfDir(string aInfDir)
        {
            #region TEXTOS.INF
            if (!File.Exists(aInfDir + "\\TEXTOS.INF"))
            {
                MessageBox.Show("TEXTOS.INF should exist in " + aInfDir);
                return false;
            }
            
            StreamReader lFileTextos = new StreamReader(aInfDir + "\\TEXTOS.INF");
            lFileTextos.ReadLine();
            mProject.OperatorName = lFileTextos.ReadLine(); // the operator name is in second line
            lFileTextos.Close();
            #endregion

            #region NOMRUTES.INF RECORRxx.INF xxPARyy.INF xxPMyy.INF
            if (!File.Exists(aInfDir + "\\NOMRUTES.INF"))
            {
                MessageBox.Show("NOMRUTES.INF should exist in " + aInfDir);
                return false;
            }

            mProject.Routes.Clear();

            StreamReader lFileNomRutes = new StreamReader(aInfDir + "\\NOMRUTES.INF");

            int lNumRoute = 1;
            string lRecorrFileName;
            while ( ! lFileNomRutes.EndOfStream)
            {
                Route lRoute = new Route();
                lRoute.RouteName = lFileNomRutes.ReadLine();

                lRecorrFileName = string.Format("\\RECORR{0:00}.INF", lNumRoute);
                
                if ( ! File.Exists(aInfDir + lRecorrFileName))
                {
                    MessageBox.Show(aInfDir + lRecorrFileName + " does not exist");
                    lFileNomRutes.Close();
                    return false;
                }

                StreamReader lRecorrFile = new StreamReader(aInfDir + lRecorrFileName);
                int lNumStop = 1, lTotalNumOfStops;

                if (lRecorrFile.EndOfStream)
                {
                    MessageBox.Show(aInfDir + lRecorrFileName + " is empty");
                    lFileNomRutes.Close();
                    lRecorrFile.Close();
                    return false;
                }

                lTotalNumOfStops = Int32.Parse(lRecorrFile.ReadLine());

                while ( ! lRecorrFile.EndOfStream && lNumStop <= lTotalNumOfStops)
                {
                    Stop lStop = new Stop();

                    lStop.StopName = lRecorrFile.ReadLine();

                    string lParFileName = string.Format("\\{0:00}PAR{1:00}.INF", lNumRoute, lNumStop);

                    if (!File.Exists(aInfDir + lParFileName))
                    {
                        lFileNomRutes.Close();
                        lRecorrFile.Close();
                        return false;
                    }
                    
                    StreamReader lParFile = new StreamReader(aInfDir + lParFileName);
                    int lNumComment = 1, lTotalNumComments;

                    if (lParFile.EndOfStream)
                    {
                        MessageBox.Show(aInfDir + lParFileName + " is empty");
                        lFileNomRutes.Close();
                        lRecorrFile.Close();
                        lParFile.Close();
                        return false;
                    }

                    lTotalNumComments = Int32.Parse(lParFile.ReadLine());

                    while(!lParFile.EndOfStream && lNumComment <= lTotalNumComments)
                    {
                        string lElocLine = lParFile.ReadLine();
                        if (lElocLine.Trim() != string.Empty)
                        {
                            Elocution lEloc = new Elocution();
                            lEloc.IsAutoplay = lElocLine[0] == '0';
                            lEloc.ElocutionName = lElocLine.Substring(lElocLine.IndexOf(' ') + 1);

                            lStop.Elocutions.Add(lEloc);
                        }
                        lNumComment++;
                    }

                    lParFile.Close();

                    string lMusicFileName = string.Format("\\{0:00}PM{1:00}.INF", lNumRoute, lNumStop);
                    if (File.Exists(aInfDir + lMusicFileName))
                    {
                        StreamReader lMusicFile = new StreamReader(aInfDir + lMusicFileName);

                        int lNumMusic = 0, lTotalNumMusics = Int32.Parse(lMusicFile.ReadLine());
                        while(!lMusicFile.EndOfStream && lNumMusic < lTotalNumMusics)
                        {
                            lStop.Musics.Add(lMusicFile.ReadLine());
                            lNumMusic++;
                        }
                        lMusicFile.Close();
                    }

                    lRoute.Stops.Add(lStop);
                    lNumStop++;
                }

                lRecorrFile.Close();

                mProject.Routes.Add(lRoute);
                lNumRoute++;
            }

            lFileNomRutes.Close();
            #endregion

            #region POSGP.INF
            if (!File.Exists(aInfDir + "\\POSGPS.INF"))
            {
                MessageBox.Show("POSGPS.INF should exist in " + aInfDir);
                return false;
            }

            StreamReader lFilePosGps = new StreamReader(aInfDir + "\\POSGPS.INF");

            if (lFilePosGps.EndOfStream)
            {
                MessageBox.Show("The file " + aInfDir + "\\POSGPS.INF is empty");
                lFilePosGps.Close();
                return false;
            }

            mProject.GpsOn = Int32.Parse(lFilePosGps.ReadLine());
            mProject.ComGps = Int32.Parse(lFilePosGps.ReadLine());
            mProject.ComControl = Int32.Parse(lFilePosGps.ReadLine());
            mProject.MarginElocutions = Int32.Parse(lFilePosGps.ReadLine());

            string lPosGpsLine;
            while (!lFilePosGps.EndOfStream &&
                   (lPosGpsLine = lFilePosGps.ReadLine()) != "####")
            {
                if (lPosGpsLine.Trim() != string.Empty)
                {
                    string[] lTokens = lPosGpsLine.Split(',');
                    if (lTokens.Length != 14)
                    {
                        lFilePosGps.Close();
                        MessageBox.Show("Incorret posgps line [" + lPosGpsLine + "]");
                        return false;
                    }

                    int lNumRoutePosGps = Int32.Parse(lTokens[0]);
                    int lNumStopPosGps = Int32.Parse(lTokens[1]);
                    int lNumElocPosGps = Int32.Parse(lTokens[2]);
                    int lRadious = Int32.Parse(lTokens[3]);
                    int lDirection = Int32.Parse(lTokens[4]);
                    int lDirectionError = Int32.Parse(lTokens[5]);

                    // lat. decimanl degrees = degrees + minutes/60 + seconds/3600
                    double lLat = Double.Parse(lTokens[6]) + 
                                  (Double.Parse(lTokens[7]) / 60) + 
                                  (Double.Parse(lTokens[8].Replace('.',',')) / 3600);

                    char lNS = lTokens[9][0];
                    if (lNS == 'S')
                    {
                        lLat *= -1;
                    }

                    // lon
                    double lLon = Double.Parse(lTokens[10]) +
                                  (Double.Parse(lTokens[11]) / 60) +
                                  (Double.Parse(lTokens[12].Replace('.', ',')) / 3600);

                    char lWE = lTokens[13][0];
                    if (lWE == 'W')
                    {
                        lLon *= -1;
                    }

                    if ( ! setGpsToElocution(lNumRoutePosGps,
                                             lNumStopPosGps,
                                             lNumElocPosGps,
                                             new PointLatLng(lLat, lLon),
                                             lDirection,
                                             lDirectionError,
                                             lRadious))
                    {
                        MessageBox.Show("Couldn't add gps position from posgps.inf");
                        lFilePosGps.Close();
                        return false;
                    }
                }                
            }

            lFilePosGps.Close();
            #endregion

            mProjectSaved = false;

            return true;
        }

        public bool projectExists(string aProjName)
        {
            if (File.Exists("input\\" + aProjName + "\\" + aProjName + ".jsn"))
            {
                return true;
            }
            return false;
        }

        public void createNewProj(string aNewProjName)
        {
            Directory.CreateDirectory("input\\" + aNewProjName);
            Directory.CreateDirectory("input\\" + aNewProjName + "\\Commentaries");
            Directory.CreateDirectory("input\\" + aNewProjName + "\\Music");
            Directory.CreateDirectory("input\\" + aNewProjName + "\\GPS");

            Ctes.CURRENT_PROJECT_FILE = "input\\" + aNewProjName + "\\" + aNewProjName + ".jsn";

            mProject = new Project();
            mProject.ProjectName = aNewProjName;

            // saving the project file
            string lJson = JsonConvert.SerializeObject(mProject);
            StreamWriter file = new StreamWriter(Ctes.CURRENT_PROJECT_FILE);
            file.Write(lJson);
            file.Close();

            mProjectSaved = false;
        }
        
        /**
         * Guarda la estructura del projecte així com 
         * els fitxers de locucions, musiques, ... al directori passat com
         * a parametre.
         * 
         * @pre: the folder exists.
         * */
        public void saveProjectIntoFolder(string aFolder, 
                                          string aOperatorName,
                                          int aNumLanguages)
        {
            try
            {
                mProject.OperatorName = aOperatorName;

                // Cleaning all content if project folder already exists
                if (Directory.Exists(aFolder))
                {
                    DirectoryInfo di = new DirectoryInfo(aFolder);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }

                Directory.CreateDirectory(aFolder);

                Directory.CreateDirectory(aFolder + "\\MTG");

                Directory.CreateDirectory(aFolder + "\\CF1");
                Directory.CreateDirectory(aFolder + "\\CF1\\MESSAGE");
                
                Directory.CreateDirectory(aFolder + "\\CF2");
                Directory.CreateDirectory(aFolder + "\\CF2\\MESSAGE");
                
                Directory.CreateDirectory(aFolder + "\\CF1\\MUSIC");
                Directory.CreateDirectory(aFolder + "\\CF2\\MUSIC");

                // creating the whole folder/files structure from the current project
                Directory.CreateDirectory(aFolder + "\\CF_ext");
                Directory.CreateDirectory(aFolder + "\\CF_ext\\VOC");

                Directory.CreateDirectory(aFolder + "\\USB");
                Directory.CreateDirectory(aFolder + "\\USB\\VOC");

                moveAutoexecs_and_MTP10(aFolder);
                
                // moving/renaming the elocutions files of the project
                if (moveMusicsToFolder(aFolder + "\\CF1\\MUSIC") &&
                    moveMusicsToFolder(aFolder + "\\CF2\\MUSIC") &&
                    moveElocsToFolder(aFolder + "\\CF1\\") &&
                    moveElocsToFolder(aFolder + "\\CF2\\"))
                {
                    // saving the project file
                    string lJson = JsonConvert.SerializeObject(mProject);
                    StreamWriter file = new StreamWriter(Ctes.CURRENT_PROJECT_FILE);
                    file.Write(lJson);
                    file.Close();
                
                    createAllVOC_files(aFolder + "\\CF_ext\\VOC");
                    createAllVOC_files(aFolder + "\\USB\\VOC");

                    mProjectSaved = true;

                    MessageBox.Show("Project saved");
                }
                else
                {
                    mProjectSaved = false;
                }                
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception saving " + e.Message);
            }
        }

        private void moveAutoexecs_and_MTP10(string aFolder)
        {
            // Copying the MTP10
            if (!File.Exists("data\\MTP10.EXE"))
            {
                MessageBox.Show("data\\MTP10.EXE does not exist");
                return;
            }
            File.Copy("data\\MTP10.EXE",
                      aFolder + "\\CF_ext\\VOC\\MTP10.EXE", true /*override*/);
            File.Copy("data\\MTP10.EXE",
                     aFolder + "\\USB\\VOC\\MTP10.EXE", true /*override*/);
 
            // autoexec's
            if (!File.Exists("data\\AUTOEXEC_for_CF.BAT"))
            {
                MessageBox.Show("data\\AUTOEXEC_for_CF.BAT does not exist");
                return;
            }
            File.Copy("data\\AUTOEXEC_for_CF.BAT",
                      aFolder + "\\CF_ext\\AUTOEXEC.BAT", true /*override*/);

            if (!File.Exists("data\\auto_for_USB.bat"))
            {
                MessageBox.Show("data\\auto_for_USB.bat does not exist");
                return;
            }
            File.Copy("data\\auto_for_USB.bat",
                      aFolder + "\\USB\\auto.bat", true /*override*/);

            if (!File.Exists("data\\AUTOEXEC_for_USB.BAT"))
            {
                MessageBox.Show("data\\AUTOEXEC_for_USB.BAT does not exist");
                return;
            }
            File.Copy("data\\AUTOEXEC_for_USB.BAT",
                      aFolder + "\\USB\\autoexec.bat", true /*override*/);

            // FONT
            if (!File.Exists("data\\HELVB.FON"))
            {
                MessageBox.Show("data\\HELVB.FON does not exist");
                return;
            }
            File.Copy("data\\HELVB.FON",
                      aFolder + "\\CF_ext\\VOC\\HELVB.FON", true /*override*/);
            File.Copy("data\\HELVB.FON",
                      aFolder + "\\USB\\VOC\\HELVB.FON", true /*override*/);              
        }

        public bool checkMissingMusics(ref string aMessage)
        {
            bool lRet = true;
            aMessage = "Missing music file for route(s): ";
            string lRoutesWithoutMusic = string.Empty;

            foreach (Route lRoute in mProject.Routes)
            {
                bool lRouteHasMusic = false;
                foreach (Stop lStop in lRoute.Stops)
                {
                    lRouteHasMusic = lStop.Musics.Count != 0;
                }

                if (!lRouteHasMusic)
                {
                    lRet = false;
                    lRoutesWithoutMusic += " " + lRoute.RouteName + " ";                   
                }
            }

            aMessage += lRoutesWithoutMusic;
            aMessage += Environment.NewLine;
            aMessage += "Please add music to any stop of those routes.";

            return lRet;
        }

        private bool moveMusicsToFolder(string aDestFolder)
        {
            HashSet<string> lMusicFiles = new HashSet<string>();

            foreach (Route lRoute in mProject.Routes)
            {
                foreach (Stop lStop in lRoute.Stops)
                {
                    foreach (string lMusicFile in lStop.Musics)
                    {
                        lMusicFiles.Add(lMusicFile);                        
                    }
                }
            }
                        
            int lNumMusic = 1;
            foreach (string lMusicFileName in lMusicFiles)
            {
                string lSrcMusicFile = Ctes.MUSIC_DIR() + lMusicFileName;
                if (File.Exists(lSrcMusicFile))
                {
                    string lFileName = string.Format("MUSIC{0:000}.WAV", lNumMusic++);
                    File.Copy(lSrcMusicFile, aDestFolder + "\\" + lFileName, true /*override*/);
                }
            }

            return true;
        }

        /** Returns the list of elocutions' file names according to the filename
         *  given as a parameter and the number of channels. */
        private List<string> getElocFileNames(string aFileName)
        {
            List<string> lRet = new List<string>();

            int lNumLangs = getNumLanguages();
            var lDirs = Directory.GetDirectories(Ctes.COMMENTARIES_DIR());

            foreach (string lFolder in lDirs)
            {
                string lSubfolderNum = lFolder.Substring(lFolder.LastIndexOf("\\") + 1);
                int lNumSubFolder = Convert.ToInt32(lSubfolderNum.Substring(0, 2));

                string lRelativeFolder = lFolder.Substring(lFolder.IndexOf("input"));

                if (lRet.Count < lNumLangs)
                {
                    lRet.Add(lRelativeFolder + "\\" + aFileName);
                }
            }

            return lRet;
        }

        /// <summary>
        /// Moves elocutions to specific folder and checks that all needed files are available.
        /// 
        /// </summary>
        /// <param name="aDestFolder"></param>
        /// <returns></returns>
        private bool moveElocsToFolder(string aDestFolder)
        {
            string lMissingFiles = Environment.NewLine;

            int lNumRuta = 1, lNumIdioma = 0;
            foreach (Route lRoute in mProject.Routes)
            {
                int lNumStop = 1;
                string lRouteDir = string.Format("{0}\\MESSAGE\\RUTA{1}", aDestFolder, lNumRuta);
                Directory.CreateDirectory(lRouteDir);
                foreach (Stop lStop in lRoute.Stops)
                {
                    int lNumEloc = 1;
                    string lStopDir = lRouteDir + "\\" + string.Format("PARADA{0:00}", lNumStop);
                    Directory.CreateDirectory(lStopDir);
                    foreach (Elocution lEloc in lStop.Elocutions)
                    {
                        lNumIdioma = 0; // restart for every elocution

                        if (!string.IsNullOrEmpty(lEloc.ElocFile))
                        {
                            foreach (String lFileName in getElocFileNames(lEloc.ElocFile))
                            {
                                string lDestFile = lStopDir +
                                                   "\\" +
                                                   string.Format("{0:00}{1:00}{2:00}{3:00}.WAV",
                                                   lNumRuta, lNumStop, lNumEloc, lNumIdioma);

                                if (!File.Exists(lFileName))
                                {
                                    lMissingFiles += lFileName + Environment.NewLine;
                                }
                                else
                                {
                                    File.Copy(lFileName, lDestFile, true /*override*/);
                                }

                                lNumIdioma++;
                            }
                        }

                        lNumEloc++;
                    }

                    lNumStop++;
                }

                lNumRuta++;
            }

            if (lMissingFiles != Environment.NewLine)
            {
                MessageBox.Show("Missing files: " + lMissingFiles);
                return false;
            }
            return true;
        }
        
        private void createAllVOC_files(string aVOC_Dir)
        {
            // NOMRUTES.INF
            StreamWriter file = new StreamWriter(aVOC_Dir + "\\NOMRUTES.INF");                
            foreach (Route lRoute in mProject.Routes)
            {
                file.WriteLine(lRoute.RouteName);
            }            
            file.Close();
            
            //  ROUTE.INF
            file = new StreamWriter(aVOC_Dir + "\\ROUTE.INF");            
            file.Write("1"); // set the first route as the current route            
            file.Close();
            
            // CANALS.INF
            file = new StreamWriter(aVOC_Dir + "\\CANALS.INF");
            file.Write("1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1");
            file.Close();
            
            // GAIN.INF
            file = new StreamWriter(aVOC_Dir + "\\GAIN.INF");
            file.Write("00 " + getChannelGain() + " " + string.Format("{0:00}", mProject.MusicGain));
            file.Close();
            
            // ASIGNA_C.INF
            file = new StreamWriter(aVOC_Dir + "\\ASIGNA_C.INF");
            file.WriteLine(getChannelOrderForAsigna());
            file.Close();

            // ASIGNA_D.INF
            switch (MainForm.mSelectedModuleType)
            {
                case 1 /*MTP08CM16B*/:
                    if (!File.Exists("data\\ASIGNA_d_for_MTP08CM16B.INF"))
                    {
                        MessageBox.Show("data\\ASIGNA_d_for_MTP08CM16B.INF does not exist");
                        return;
                    }
                    File.Copy("data\\ASIGNA_d_for_MTP08CM16B.INF",
                              aVOC_Dir + "\\ASIGNA_D.INF", true /*override*/);

                    break;
                default:                    
                    if (!File.Exists("data\\ASIGNA_d_for_NOT_MTP08CM16B.INF"))
                    {
                        MessageBox.Show("data\\ASIGNA_d_for_NOT_MTP08CM16B.INF does not exist");
                        return;
                    }
                    File.Copy("data\\ASIGNA_d_for_NOT_MTP08CM16B.INF",
                              aVOC_Dir + "\\ASIGNA_D.INF", true /*override*/);
                    break;
            }

            // NOMRUTES.INF
            file = new StreamWriter(aVOC_Dir + "\\NOMRUTES.INF");
            int lNumRuta = 1;            
            foreach (Route lRoute in mProject.Routes)
            {
                file.WriteLine(lRoute.RouteName);

                int lNumStop = 1;

                // RECORRXX.INF                                
                StreamWriter lFileRecorr = 
                    new StreamWriter(
                        string.Format(aVOC_Dir + "\\RECORR{0:00}.INF", lNumRuta));

                lFileRecorr.WriteLine(string.Format("{0:00}", lRoute.Stops.Count));
                foreach (Stop lStop in lRoute.Stops)
                {
                    lFileRecorr.WriteLine(lStop.StopName);

                    // XXPARYY.INF
                    StreamWriter lFileParEloc =
                        new StreamWriter(
                            string.Format(aVOC_Dir + "\\{0:00}PAR{1:00}.INF", 
                                          lNumRuta, lNumStop));
                    lFileParEloc.WriteLine(string.Format("{0:00}", lStop.Elocutions.Count));
                    int lNumEloc = 1;
                    foreach (Elocution lEloc in lStop.Elocutions)
                    {
                        string lElocLine;

                        lElocLine = lEloc.IsAutoplay ? "0" : "1";
                        lElocLine += "0"; // attribute priority is no longer needed
                        lElocLine += "01";
                        lElocLine += string.Format("{0:00} ", lNumEloc);
                        lElocLine += lEloc.ElocutionName;

                        lFileParEloc.WriteLine(lElocLine);

                        lNumEloc++;
                    }
                    lFileParEloc.Close();

                    // XXPMYY.INF
                    StreamWriter lFileMusicNames =
                        new StreamWriter(
                            string.Format(aVOC_Dir + "\\{0:00}PM{1:00}.INF",
                                          lNumRuta, lNumStop));

                    int lNumMusica = 1;
                    lFileMusicNames.WriteLine(string.Format("{0:00}", lStop.Musics.Count));
                    foreach (string lMusic in lStop.Musics)
                    {
                        string lMusicName = string.Format("MUSIC{0:000}", lNumMusica++);
                        lFileMusicNames.WriteLine(lMusicName);                        
                    }

                    lFileMusicNames.Close();

                    lNumStop++;
                }
                lFileRecorr.Close();

                lNumRuta++;
            }
            file.Close();

            // TEXTOS.INF
            string lTxt = "PRINCIPAL MENU" + Environment.NewLine +
              mMainFormInstance.getOperatorName() + Environment.NewLine +
              "VOLUME CONTROL" + Environment.NewLine +
              "PLAY/PAUSE" + Environment.NewLine +
              "CHANGE ROUTE" + Environment.NewLine +
              "File not found" + Environment.NewLine +
              "Error Modifying Volume" + Environment.NewLine +
              "Ready" + Environment.NewLine +
              "ROUTE" + Environment.NewLine +
              "Play" + Environment.NewLine +
              "Pause" + Environment.NewLine +
              "6042" + Environment.NewLine +
              "Error reading serial bus" + Environment.NewLine +
              "CHANGE LANGUAGE" + Environment.NewLine +
              "GAIN OF CHANNELS" + Environment.NewLine +
              "Time rem." + Environment.NewLine +
              "Error Command" + Environment.NewLine +
              "GENERAL GAIN" + Environment.NewLine +
              "Error path" + Environment.NewLine +
              "Sound Test" + Environment.NewLine +
              "MUSIC GAIN" + Environment.NewLine +
              "NOVIDEO" + Environment.NewLine;

            file = new StreamWriter(aVOC_Dir + "\\TEXTOS.INF");
            file.WriteLine(lTxt);
            file.Close();

            // creating empty VIDEOS.INF
            lTxt = String.Empty;
            file = new StreamWriter(aVOC_Dir + "\\VIDEOS.INF");
            file.WriteLine(lTxt);
            file.Close();

            // copy all the .INF files into input...
            foreach (string lfileName in Directory.GetFiles(aVOC_Dir))
            {
                FileInfo lFileInfo = new FileInfo(lfileName);
                if (lFileInfo.Name.EndsWith("INF"))
                {
                    File.Copy(lfileName,
                          aVOC_Dir + "\\..\\..\\..\\..\\input\\" + mProject.ProjectName + "\\GPS\\" + lFileInfo.Name, true /*override*/);
                }
            }

            // copy the POSGPS.INF if it exists
            writePosGpsInf(aVOC_Dir + "\\POSGPS.INF");
            
            // copy project file (JSON) to output folder
            File.Copy(Ctes.CURRENT_PROJECT_FILE, aVOC_Dir + "\\project.json", true /*override*/);
        }

        public void addNewRoute(string aRouteName)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName) {
                    MessageBox.Show("The route '" + 
                        aRouteName + "' already exists");
                    return;
                }
            }            

            Route lR = new Route();
            lR.RouteName = aRouteName;
            mProject.Routes.Add(lR);

            mProjectSaved = false;
        }

        /*** Adds the stop name to the especific route **/
        public void addNewStopToRoute(string aRouteName, string aStopName)
        {
            bool lExistsRoute = false;
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            MessageBox.Show("The stop '" +
                                    aStopName + "' already exists in route '" + aRouteName + "'");
                            return;
                        }
                    }

                    Stop lS = new Stop();
                    lS.StopName = aStopName;
                    lRoute.Stops.Add(lS);

                    lExistsRoute = true;
                    break;                    
                }
            }

            if (!lExistsRoute)
            {
                System.Windows.Forms.MessageBox.Show("The route " + 
                        aRouteName + " doesn't exist");
                return;
            }

            mProjectSaved = false;
        }

        public bool checkCommentExists(string aRouteName, string aStopName, string aElocutionName)
        {
            if (mProject.Routes.Any(r => r.RouteName == aRouteName &&
                                    r.Stops.Any(s => s.StopName == aStopName &&
                                    s.Elocutions.Any(e => e.ElocutionName == aElocutionName))))
            {               
                return true;
            }
            return false;
        }

        public bool addElocutionToStop(string aRouteName,
                                       string aStopName,
                                       string aElocutionName,
                                       string aFileName,
                                       bool aAutoplay)
        {
            Elocution lElo = new Elocution();
            lElo.ElocutionName = aElocutionName;
            lElo.IsAutoplay = aAutoplay;
            lElo.ElocFile = aFileName;
            
            return addMusic_Or_ElocutionToStop(aRouteName, aStopName, string.Empty, lElo /*Elocution lElo*/);
        }

        public bool addMusicToRoute(string aRouteName,
                                    string aMusicFileName)
        {            
            foreach(Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if ( ! addMusic_Or_ElocutionToStop(aRouteName,
                                                    lStop.StopName,
                                                    aMusicFileName,
                                                    null /*Elocution lElo*/))
                        {
                            return false;
                        }
                    }
                }
            }
            
            return true;
        }

        public bool addMusicToStop(string aRouteName,
                                   string aStopName,
                                   string aMusicFileName)
        {
            return addMusic_Or_ElocutionToStop(aRouteName, aStopName, aMusicFileName, 
                                               null /*Elocution lElo*/);
        }

        /** If lElo != null, adds a new elocution to the stop-route.
         * Adds a music file name elsewere.
         * */
        private bool addMusic_Or_ElocutionToStop(string aRouteName,
                                                 string aStopName,
                                                 string aFileMusicName, 
                                                 Elocution aElo)
        {
            bool lExistsRoute = false;
            bool lExistsStop = false;
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            lExistsStop = true; // we've found the stop 

                            if (aElo != null)
                            {                                
                                lStop.Elocutions.Add(aElo);
                            }
                            else
                            {
                                lStop.Musics.Add(aFileMusicName);
                            }
                        }
                    }

                    lExistsRoute = true;
                    break;
                }
            }

            if (!lExistsRoute)
            {
                MessageBox.Show("The route '" + aRouteName + "' doesn't exist");
                return false;
            }

            if (!lExistsStop)
            {
                MessageBox.Show("The stop '" + aStopName + "' doesn't exist");
                return false;
            }

            mProjectSaved = false;

            return true;
        }

        public void removeRoute(string aRouteName)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    mProject.Routes.Remove(lRoute);
                    mProjectSaved = false;
                    break;
                }
            }
        }

        public void moveEloc_up_or_down(string aRouteName,
                                        string aStopName,
                                        string aElocution,
                                        bool abUp)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            // first of all, find out the elocution's index.
                            int lIndex = 0;
                            foreach (Elocution lEloc in lStop.Elocutions)
                            {
                                if (lEloc.ElocutionName == aElocution)
                                {
                                    break;
                                }
                                lIndex++;
                            }

                            if (abUp)
                            {
                                if (lIndex > 0)
                                {
                                    Swapper.Swap<Elocution>(lStop.Elocutions, lIndex, lIndex - 1);
                                    mProjectSaved = false;
                                }
                            }
                            else
                            {
                                if (lIndex < (lStop.Elocutions.Count - 1))
                                {
                                    Swapper.Swap<Elocution>(lStop.Elocutions, lIndex, lIndex + 1);
                                    mProjectSaved = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        public void moveEloc_top_or_bottom(string aRouteName,
                                        string aStopName,
                                        string aElocution, bool lbTop)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            // first of all, find out the elocution's index.
                            int lIndex = 0;
                            Elocution lElocToMove = new Elocution();
                            foreach (Elocution lEloc in lStop.Elocutions)
                            {
                                if (lEloc.ElocutionName == aElocution)
                                {
                                    lElocToMove = lEloc;
                                    lStop.Elocutions.RemoveAt(lIndex);

                                    if (lbTop)
                                    {
                                        lStop.Elocutions.Insert(0, lElocToMove);        
                                    }
                                    else
                                    {
                                        lStop.Elocutions.Add(lElocToMove);
                                    }

                                    mProjectSaved = false;                                
                                    break;
                                }
                                lIndex++;
                            }
                        }
                    }
                }
            }
        }

        public void moveStop_up_or_down(string aRouteName, string aStopName, bool abUp)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    // first of all, find out the stop's index.
                    int lIndex = 0;
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            break;
                        }
                        lIndex++;
                    }

                    if (abUp)
                    {
                        if (lIndex > 0)
                        {
                            Swapper.Swap<Stop>(lRoute.Stops, lIndex, lIndex - 1);
                            mProjectSaved = false;
                        }
                    }
                    else
                    {
                        if (lIndex < (lRoute.Stops.Count - 1))
                        {
                            Swapper.Swap<Stop>(lRoute.Stops, lIndex, lIndex + 1);
                            mProjectSaved = false;
                        }
                    }
                }
            }
        }

        public void moveStop_top_or_bottom(string aRouteName, string aStopName, bool abTop)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    // first of all, find out the stop to move                    
                    int lIndex = 0;
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            Stop lStopToMove = new Stop();
                            lStopToMove = lStop;

                            lRoute.Stops.RemoveAt(lIndex);
                            if (abTop)
                            {
                                lRoute.Stops.Insert(0, lStopToMove);
                            }
                            else
                            {
                                lRoute.Stops.Add(lStopToMove);
                            }
                            
                            mProjectSaved = false;
                            break;
                        }

                        lIndex++;
                    }                    
                }
            }
        }
        
        public void removeStop(string aRouteName, string aStopName)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            lRoute.Stops.Remove(lStop);
                            mProjectSaved = false;
                            break;
                        }
                    }
                }
            }
        }

        public void removeElocution(string aRouteName, string aStopName, string aElocName)
        {
            Stop lStop = getStop(aRouteName, aStopName);
            if (lStop != null)
            {
                foreach (Elocution lEloc in lStop.Elocutions)
                {
                    if (lEloc.ElocutionName == aElocName)
                    {
                        lStop.Elocutions.Remove(lEloc);
                        mProjectSaved = false;
                        break; // because the collection has been modified
                    }
                }
            }
        }

        public void removeMusic(string aRouteName, string aStopName, string aMusicName)
        {
            Stop lStop = getStop(aRouteName, aStopName);
            if (lStop != null)
            {
                foreach (string lMusic in lStop.Musics)
                {
                    if (lMusic == aMusicName)
                    {
                        lStop.Musics.Remove(lMusic);
                        mProjectSaved = false;
                        break;
                    }
                }
            }
        }
        
        public List<Route> getAllRotues()
        {
            return mProject.Routes;
        }

        public bool isProjSaved()
        {
            return mProjectSaved;
        }

        public void setNumLanguages(int aNumLanguages)
        {
            mProject.NumLanguages = aNumLanguages;
        }

        public void setMainFormInstance(MainForm lMainForm)
        {
            mMainFormInstance = lMainForm;
        }

        public void setChannelGain(int lCh1, int lCh2,
                                   int lCh3, int lCh4,
                                   int lCh5, int lCh6,
                                   int lCh7, int lCh8,
                                   int lCh9, int lCh10,
                                   int lCh11, int lCh12,
                                   int lCh13, int lCh14,
                                   int lCh15, int lCh16)
        {
            mProject.ChannelGain[0] = lCh1; mProject.ChannelGain[1] = lCh2;
            mProject.ChannelGain[2] = lCh3; mProject.ChannelGain[3] = lCh4;
            mProject.ChannelGain[4] = lCh5; mProject.ChannelGain[5] = lCh6;
            mProject.ChannelGain[6] = lCh7; mProject.ChannelGain[7] = lCh8;
            mProject.ChannelGain[8] = lCh9; mProject.ChannelGain[9] = lCh10;
            mProject.ChannelGain[10] = lCh11; mProject.ChannelGain[11] = lCh12;
            mProject.ChannelGain[12] = lCh13; mProject.ChannelGain[13] = lCh14;
            mProject.ChannelGain[14] = lCh15; mProject.ChannelGain[15] = lCh16;
        }

        public void setChannelOrder(int []lChn)
        {
            mProject.ChannelOrder = lChn;
        }

        /**
         * Handles the special treatment of the 'asigna_c.ing' file according
         * to the selected module.
         * */
        public string getChannelOrderForAsigna()
        {
            switch(MainForm.mSelectedModuleType)
            {
                case 0:
                case 1:
                case 2: /*MTP08CM de 8/16 canales con módulos compatibles*/
                    //12 04 11 03 05 13 06 14 08 00 15 07 01 09 02 10
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelOrder[12],
                                         mProject.ChannelOrder[4],
                                         mProject.ChannelOrder[11],
                                         mProject.ChannelOrder[3],
                                         mProject.ChannelOrder[5],
                                         mProject.ChannelOrder[13],
                                         mProject.ChannelOrder[6],
                                         mProject.ChannelOrder[14],
                                         mProject.ChannelOrder[8],
                                         mProject.ChannelOrder[0],
                                         mProject.ChannelOrder[15],
                                         mProject.ChannelOrder[7],
                                         mProject.ChannelOrder[1],
                                         mProject.ChannelOrder[9],
                                         mProject.ChannelOrder[2],
                                         mProject.ChannelOrder[10]
                                        );

                case 3 /*MTP08CM de 16 canales con módulos Nuevos*/:
                    //03 02 01 00 04 05 06 07 11 10 09 08 12 13 14 15
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelOrder[3],
                                         mProject.ChannelOrder[2],
                                         mProject.ChannelOrder[1],
                                         mProject.ChannelOrder[0],
                                         mProject.ChannelOrder[4],
                                         mProject.ChannelOrder[5],
                                         mProject.ChannelOrder[6],
                                         mProject.ChannelOrder[7],
                                         mProject.ChannelOrder[11],
                                         mProject.ChannelOrder[10],
                                         mProject.ChannelOrder[9],
                                         mProject.ChannelOrder[8],
                                         mProject.ChannelOrder[12],
                                         mProject.ChannelOrder[13],
                                         mProject.ChannelOrder[14],
                                         mProject.ChannelOrder[15]                                         
                                        );
                case 4 /*MTP08CM de 8 canales con módulos Nuevos*/:
                    //03 02 01 00 04 05 06 07 03 02 01 00 04 05 06 07
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelOrder[3],                                         
                                         mProject.ChannelOrder[2],                                         
                                         mProject.ChannelOrder[1],                                         
                                         mProject.ChannelOrder[0],                                         
                                         mProject.ChannelOrder[4],                                         
                                         mProject.ChannelOrder[5],                                         
                                         mProject.ChannelOrder[6],                                         
                                         mProject.ChannelOrder[7],                                         
                                         mProject.ChannelOrder[3],                                         
                                         mProject.ChannelOrder[2],                                         
                                         mProject.ChannelOrder[1],                                         
                                         mProject.ChannelOrder[0],                                         
                                         mProject.ChannelOrder[4],                                         
                                         mProject.ChannelOrder[5],                                         
                                         mProject.ChannelOrder[6],                                         
                                         mProject.ChannelOrder[7]                                        
                                        );
                case 5 /*MTP08TX de 8 canales*/:
                    //00 00 01 01 02 02 03 03 04 04 05 05 06 06 07 07
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelOrder[0],
                                         mProject.ChannelOrder[0],
                                         mProject.ChannelOrder[1],
                                         mProject.ChannelOrder[1],
                                         mProject.ChannelOrder[2],
                                         mProject.ChannelOrder[2],
                                         mProject.ChannelOrder[3],
                                         mProject.ChannelOrder[3],
                                         mProject.ChannelOrder[4],
                                         mProject.ChannelOrder[4],
                                         mProject.ChannelOrder[5],
                                         mProject.ChannelOrder[5],
                                         mProject.ChannelOrder[6],
                                         mProject.ChannelOrder[6],
                                         mProject.ChannelOrder[7],
                                         mProject.ChannelOrder[7]
                                        );
                case 6 /*MTP08TX de 16 canales*/:
                    //08 00 09 01 10 02 11 03 12 04 13 05 14 06 15 07
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelOrder[8],
                                         mProject.ChannelOrder[0],
                                         mProject.ChannelOrder[9],
                                         mProject.ChannelOrder[1],
                                         mProject.ChannelOrder[10],
                                         mProject.ChannelOrder[2],
                                         mProject.ChannelOrder[11],
                                         mProject.ChannelOrder[3],
                                         mProject.ChannelOrder[12],
                                         mProject.ChannelOrder[4],
                                         mProject.ChannelOrder[13],
                                         mProject.ChannelOrder[5],
                                         mProject.ChannelOrder[14],
                                         mProject.ChannelOrder[6],
                                         mProject.ChannelOrder[15],
                                         mProject.ChannelOrder[7]
                                        );
            }

            return "";
        }

        public int[] getChannelOrderAsVector()
        {
            return mProject.ChannelOrder;
        }

        public string getChannelGain()
        {
            switch (MainForm.mSelectedModuleType)
            {
                case 0:
                case 1:
                case 2: /*MTP08CM de 8/16 canales con módulos compatibles*/
                    //12 04 11 03 05 13 06 14 08 00 15 07 01 09 02 10
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelGain[12],
                                         mProject.ChannelGain[4],
                                         mProject.ChannelGain[11],
                                         mProject.ChannelGain[3],
                                         mProject.ChannelGain[5],
                                         mProject.ChannelGain[13],
                                         mProject.ChannelGain[6],
                                         mProject.ChannelGain[14],
                                         mProject.ChannelGain[8],
                                         mProject.ChannelGain[0],
                                         mProject.ChannelGain[15],
                                         mProject.ChannelGain[7],
                                         mProject.ChannelGain[1],
                                         mProject.ChannelGain[9],
                                         mProject.ChannelGain[2],
                                         mProject.ChannelGain[10]
                                        );

                case 3 /*MTP08CM de 16 canales con módulos Nuevos*/:
                    //03 02 01 00 04 05 06 07 11 10 09 08 12 13 14 15
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelGain[3],
                                         mProject.ChannelGain[2],
                                         mProject.ChannelGain[1],
                                         mProject.ChannelGain[0],
                                         mProject.ChannelGain[4],
                                         mProject.ChannelGain[5],
                                         mProject.ChannelGain[6],
                                         mProject.ChannelGain[7],
                                         mProject.ChannelGain[11],
                                         mProject.ChannelGain[10],
                                         mProject.ChannelGain[9],
                                         mProject.ChannelGain[8],
                                         mProject.ChannelGain[12],
                                         mProject.ChannelGain[13],
                                         mProject.ChannelGain[14],
                                         mProject.ChannelGain[15]
                                        );
                case 4 /*MTP08CM de 8 canales con módulos Nuevos*/:
                    //03 02 01 00 04 05 06 07 03 02 01 00 04 05 06 07
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelGain[3],
                                         mProject.ChannelGain[2],
                                         mProject.ChannelGain[1],
                                         mProject.ChannelGain[0],
                                         mProject.ChannelGain[4],
                                         mProject.ChannelGain[5],
                                         mProject.ChannelGain[6],
                                         mProject.ChannelGain[7],
                                         mProject.ChannelGain[3],
                                         mProject.ChannelGain[2],
                                         mProject.ChannelGain[1],
                                         mProject.ChannelGain[0],
                                         mProject.ChannelGain[4],
                                         mProject.ChannelGain[5],
                                         mProject.ChannelGain[6],
                                         mProject.ChannelGain[7]
                                        );
                case 5 /*MTP08TX de 8 canales*/:
                    //00 00 01 01 02 02 03 03 04 04 05 05 06 06 07 07
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelGain[0],
                                         mProject.ChannelGain[0],
                                         mProject.ChannelGain[1],
                                         mProject.ChannelGain[1],
                                         mProject.ChannelGain[2],
                                         mProject.ChannelGain[2],
                                         mProject.ChannelGain[3],
                                         mProject.ChannelGain[3],
                                         mProject.ChannelGain[4],
                                         mProject.ChannelGain[4],
                                         mProject.ChannelGain[5],
                                         mProject.ChannelGain[5],
                                         mProject.ChannelGain[6],
                                         mProject.ChannelGain[6],
                                         mProject.ChannelGain[7],
                                         mProject.ChannelGain[7]
                                        );
                case 6 /*MTP08TX de 16 canales*/:
                    //08 00 09 01 10 02 11 03 12 04 13 05 14 06 15 07
                    return string.Format("{0:00} {1:00} {2:00} {3:00} {4:00} {5:00} " +
                                         "{6:00} {7:00} {8:00} {9:00} {10:00} {11:00} " +
                                         "{12:00} {13:00} {14:00} {15:00}",
                                         mProject.ChannelGain[8],
                                         mProject.ChannelGain[0],
                                         mProject.ChannelGain[9],
                                         mProject.ChannelGain[1],
                                         mProject.ChannelGain[10],
                                         mProject.ChannelGain[2],
                                         mProject.ChannelGain[11],
                                         mProject.ChannelGain[3],
                                         mProject.ChannelGain[12],
                                         mProject.ChannelGain[4],
                                         mProject.ChannelGain[13],
                                         mProject.ChannelGain[5],
                                         mProject.ChannelGain[14],
                                         mProject.ChannelGain[6],
                                         mProject.ChannelGain[15],
                                         mProject.ChannelGain[7]
                                        );
            }

            return "";
        }

        public int[] getChannelGainAsVector()
        {
            return mProject.ChannelGain;
        }

        public void toggleAutoplayAutostop(string aRouteName,
                                           string aStopName,
                                           string aElocutionName)
        {
            Elocution lEloc = getElocution(aRouteName, aStopName, aElocutionName);
            if (lEloc != null)
            {
                lEloc.IsAutoplay = !lEloc.IsAutoplay;
                mProjectSaved = false;
            }            
        }

        /// <summary>
        /// Sets the Gps info obtained from posgps.inf file
        /// example of line: 1,1,2,25,90,150,40,24,54.4672,N,3,41,36.1693,W
        /// </summary>        
        private bool setGpsToElocution(int aNumRoute, 
                                       int aNumStop,
                                       int aNumEloc,
                                       PointLatLng aPosGps,
                                       int aDirection,
                                       int aDirectionError,
                                       int aRadious)
        {
            if (aNumRoute <= 0 || aNumRoute > mProject.Routes.Count)
            {
                MessageBox.Show("The route " + aNumRoute + " is out of bounds");
                return false;
            }

            Route lRoute = mProject.Routes[aNumRoute - 1];

            if (aNumStop <= 0 || aNumStop > lRoute.Stops.Count)
            {
                MessageBox.Show("The stop " + aNumStop + 
                                " is out of bounds for route " + aNumRoute);
                return false;
            }

            Stop lStop = lRoute.Stops[aNumStop - 1];

            if (aNumEloc <= 0 || aNumEloc > lStop.Elocutions.Count)
            {
                MessageBox.Show("The elocution " + aNumEloc +
                                " is out of bounds for stop "  + aNumStop +
                                " and route " + aNumRoute);
                return false;
            }

            Elocution lEloc = lStop.Elocutions[aNumEloc - 1];

            lEloc.GpsLat = aPosGps.Lat;
            lEloc.GpsLong = aPosGps.Lng;
            lEloc.direction = aDirection;
            lEloc.angleError = aDirectionError;
            lEloc.radio = aRadious;

            mProjectSaved = false;

            return true;
        }

        public Elocution setGpsToElocution(string aRouteName,
                                     string aStopName,
                                     string aElocName,
                                     PointLatLng aPosGps,
                                     int aDirection,
                                     int aDirectionError,
                                     int aRadious)
        {
            Elocution lEloc = getElocution(aRouteName, aStopName, aElocName);
            if (lEloc != null)
            {
                lEloc.GpsLat = aPosGps.Lat;
                lEloc.GpsLong = aPosGps.Lng;
                lEloc.direction = aDirection;
                lEloc.angleError = aDirectionError;
                lEloc.radio = aRadious;

                mProjectSaved = false;
            }
            return lEloc;        
        }
        
        public int getNumLanguages()
        {
            if (mProject.NumLanguages == 0)
                return 4;

            return mProject.NumLanguages;
        }

        public void modifyRouteName(string aPrevName, string aNewName)
        {
            // first of all, look whether the new route name already exists
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aNewName)
                {
                    MessageBox.Show("The route '" + aNewName + "' already exist");
                    return;                    
                }
            }

            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aPrevName)
                {
                    lRoute.RouteName = aNewName;
                    mProjectSaved = false;
                    break;
                }
            }
        }

        public void modifyStopName(string aRouteName,
                                   string aPrevStopName,
                                   string aNewStopName)
        {
            // first of all, look whether the new stop name already exists
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aNewStopName)
                        {
                            MessageBox.Show("The stop '" + aNewStopName + "' already exists in route '" + aRouteName + "'");
                            return;
                        }
                    }
                }
            }

            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aPrevStopName)
                        {
                            lStop.StopName = aNewStopName;
                            mProjectSaved = false;
                            return;
                        }
                    }
                }
            }
        }

        public void modifyCommentaryName(string aRouteName,
                                         string aStopName,
                                         string aPrevCommentName,
                                         string aNewCommentName)
        {
            foreach (Route lRoute in mProject.Routes)
            {
                if (lRoute.RouteName == aRouteName)
                {
                    foreach (Stop lStop in lRoute.Stops)
                    {
                        if (lStop.StopName == aStopName)
                        {
                            foreach(Elocution lEloc in lStop.Elocutions)
                            {
                                if (lEloc.ElocutionName == aPrevCommentName)
                                {
                                    lEloc.ElocutionName = aNewCommentName;
                                    mProjectSaved = false;
                                    return;
                                }
                            }                            
                        }
                    }                    
                }
            }
        }

        /// <summary>
        /// Consider a project empty if it does not contain any route
        /// </summary>
        public bool isProjectEmpty()
        {
            return mProject.Routes.Count == 0; 
        }

        public Stop getStop(string aRouteName, string aStopName)
        {
            Route lRoute = mProject.Routes.FirstOrDefault(r => r.RouteName == aRouteName);
            if (lRoute != null)
            {
                Stop lStop = lRoute.Stops.FirstOrDefault(s => s.StopName == aStopName);
                if (lStop != null)
                {
                    return lStop;
                }
            }
            return null;
        }

        public Elocution getElocution(string aRouteName, string aStopName, string aElocName)
        {
            Stop lStop = getStop(aRouteName, aStopName);
            if (lStop != null)
            { 
                Elocution lEloc = lStop.Elocutions.FirstOrDefault(e => e.ElocutionName == aElocName);
                if (lEloc != null)
                {
                    return lEloc;
                }
            }
            
            return null;
        }

        private void writePosGpsInf(string aDestFfile)
        {
            StreamWriter lPosGpsFile = new StreamWriter(aDestFfile);
            
            lPosGpsFile.WriteLine("1" /* mProject.GpsOn */); // these three lines where already hard-coded in Qt project.
            lPosGpsFile.WriteLine("1" /* mProject.ComGps */);
            lPosGpsFile.WriteLine("4" /* mProject.ComControl */);
            lPosGpsFile.WriteLine(mProject.MarginElocutions);

            int lNumRoute = 1;
            foreach(Route lRoute in mProject.Routes)
            {
                int lNumStop = 1;
                foreach(Stop lStop in lRoute.Stops)
                {
                    int lNumEloc = 1;
                    foreach (Elocution lEloc in lStop.Elocutions)
                    {
                        if (lEloc.GpsLat != 0 && lEloc.GpsLong != 0)
                        {
                            int lLatDeg = Convert.ToInt32(Math.Floor(Math.Abs(lEloc.GpsLat)));
                            int lLatMin = Convert.ToInt32(Math.Floor((Math.Abs(lEloc.GpsLat) - lLatDeg) * 60));
                            double lLatSec = (Math.Abs(lEloc.GpsLat) - lLatDeg - ((double) lLatMin / 60)) * 3600;

                            string lStrLatSec = string.Format("{0:0.0000}", lLatSec);

                            char lNS = lEloc.GpsLat >= 0 ? 'N':'S';

                            int lLonDeg = Convert.ToInt32(Math.Floor(Math.Abs(lEloc.GpsLong)));
                            int lLonMin = Convert.ToInt32(Math.Floor((Math.Abs(lEloc.GpsLong) - lLonDeg) * 60));
                            double lLonSec = (Math.Abs(lEloc.GpsLong) - lLonDeg - ((double) lLonMin / 60)) * 3600;

                            string lStrLonSec = string.Format("{0:0.0000}", lLonSec);

                            char lWE = lEloc.GpsLong >= 0 ? 'E' : 'W';

                            string lLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}", 
                                                         lNumRoute, 
                                                         lNumStop,
                                                         lNumEloc,
                                                         lEloc.radio,
                                                         lEloc.direction,
                                                         lEloc.angleError,
                                                         lLatDeg,
                                                         lLatMin,
                                                         lStrLatSec.Replace(',','.'),
                                                         lNS,
                                                         lLonDeg,
                                                         lLonMin,
                                                         lStrLonSec.Replace(',', '.'),
                                                         lWE);
                            lPosGpsFile.WriteLine(lLine);
                        }

                        lNumEloc++;
                    }
                    lNumStop++;
                }
                lNumRoute++;
            }

            lPosGpsFile.WriteLine("####");
            lPosGpsFile.Close();
        }

        public void setMusicGain(int lMusicGain)
        {
            mProject.MusicGain = lMusicGain;
        }

        public int getMusicGain()
        {
            return mProject.MusicGain;
        }

        public void setProjectName(string aProjName)
        {
            mProject.ProjectName = aProjName;
        }

        public string getProjName()
        {
            return mProject.ProjectName;
        }

        public void setSelectedModuleIndex(int aSelModIndex)
        {
            mProject.SelectedModuleIndex = aSelModIndex;
        }

        public int getSelectedModuleIndex()
        {
            return mProject.SelectedModuleIndex;
        }

        public string getOperatorName()
        {
            return mProject.OperatorName;
        }

        public void setMarginElocs(int aMargin)
        {
            mProject.MarginElocutions = aMargin;
        }

        public int getMarginElocs()
        {
            return mProject.MarginElocutions;
        }
        
        public Tuple<int, int, int> getCurrentIndexes(string aRoute, string aStop, string aEloc)
        {            
            int lRouteIndex = mProject.Routes.FindIndex(r => r.RouteName == aRoute);
            int lStopIndex = -1;
            int lElocIndex = -1;

            if (lRouteIndex != -1)
            {
                Route lRoute = mProject.Routes[lRouteIndex];

                lStopIndex = lRoute.Stops.FindIndex(s => s.StopName == aStop);
                if(lStopIndex != -1)
                {
                    lElocIndex = lRoute.Stops[lStopIndex].Elocutions.FindIndex(e => e.ElocutionName == aEloc);
                }
            }
            
            return new Tuple<int, int, int> (lRouteIndex, lStopIndex, lElocIndex);
        }

        #region MTG
        public void setLanguageToLeftChannel(int aIndex, string aLanguage)
        {
            if (aIndex < 0 || aIndex > mProject.langLeftChannel.Length)
            {
                MessageBox.Show("Wrong index " + aIndex + " when setting left language channel");
                return;
            }
            mProject.langLeftChannel[aIndex] = aLanguage;
        }

        public void setLanguageToRightChannel(int aIndex, string aLanguage)
        {
            if (aIndex < 0 || aIndex > mProject.langRightChannel.Length)
            {
                MessageBox.Show("Wrong index " + aIndex + " when setting right language channel");
                return;
            }
            mProject.langRightChannel[aIndex] = aLanguage;
        }

        public void populateMTG(string aDestFolder)
        {
            // lists for music files
            List<string> lOriginMusicFiles = new List<string>();
            List<string> lDestMusicFiles = new List<string>();

            // lists for handling elocucions conversions (two wav files are merged into one mp3)
            List<string> lOriginLeftChannelFiles = new List<string>();
            List<string> lOriginRightChannelFiles = new List<string>();
            List<string> lDestMp3Files = new List<string>();

            MTG_project lMtgProj = new MTG_project();
            lMtgProj.RouteMakerVersion = Ctes.VERSION;

            int lRouteIndex = 0;
            foreach (Route lRoute in ProjectMngr.getInstance().getAllRotues())
            {
                RouteMTG lRouteMTG = new RouteMTG();
                lRouteMTG.RouteName = lRoute.RouteName;

                int lStopIndex = 0;
                foreach (Stop lStop in lRoute.Stops)
                {
                    StopMTG lStopMTG = new StopMTG();
                    lStopMTG.StopName = lStop.StopName;
                    lStop.Musics.ForEach(m =>
                        {
                            string lFileName = Path.GetFileName(m);

                            lOriginMusicFiles.Add(Ctes.MUSIC_DIR() + m);
                            lDestMusicFiles.Add(aDestFolder + "\\music\\" + lFileName + ".mp3");
                            lStopMTG.Musics.Add("/home/pi/ME_bus/data/music/" + lFileName + ".mp3");
                        }
                    );

                    int lElocIndex = 0;
                    foreach (Elocution lEloc in lStop.Elocutions)
                    {
                        ElocutionMTG lElocMTG = new ElocutionMTG();

                        lElocMTG.IsAutoplay = lEloc.IsAutoplay;
                        lElocMTG.ElocutionName = lEloc.ElocutionName;
                        lElocMTG.GpsLong = lEloc.GpsLong;
                        lElocMTG.GpsLat = lEloc.GpsLat;
                        lElocMTG.direction = lEloc.direction;
                        lElocMTG.angleError = lEloc.angleError;
                        lElocMTG.radio = lEloc.radio;

                        for (int lCh = 0; lCh < mProject.NumPhisicalChannels; lCh++)
                        {
                            lOriginLeftChannelFiles.Add(Ctes.COMMENTARIES_DIR() +
                                mProject.langLeftChannel[lCh] + "\\" + lEloc.ElocFile);

                            lOriginRightChannelFiles.Add(Ctes.COMMENTARIES_DIR() +
                                mProject.langRightChannel[lCh] + "\\" + lEloc.ElocFile);

                            int lLeftChIndexLang = Convert.ToInt32(
                                mProject.langLeftChannel[lCh].Substring(0, 2));
                            int lRightChIndexLang = Convert.ToInt32(
                                mProject.langRightChannel[lCh].Substring(0, 2));

                            // ROUTE + STOP + ELOC + LEFT_LANG + RIGHT_LANG
                            string lDestMp3FileName =
                                string.Format(aDestFolder +
                                "\\commentaries\\{0:00}{1:00}{2:00}{3:00}{4:00}.mp3",
                                lRouteIndex, lStopIndex, lElocIndex, lLeftChIndexLang, lRightChIndexLang);

                            lDestMp3Files.Add(lDestMp3FileName);
                            // forward slash needed because it'll be parsed by unix machine
                            lElocMTG.ElocutionFiles.Add(
                                string.Format("/home/pi/ME_bus/data/commentaries/{0:00}{1:00}{2:00}{3:00}{4:00}.mp3",
                                lRouteIndex, lStopIndex, lElocIndex, lLeftChIndexLang, lRightChIndexLang));
                        }
                        lStopMTG.Elocutions.Add(lElocMTG);

                        lElocIndex++;
                    }

                    lStopIndex++;

                    lRouteMTG.Stops.Add(lStopMTG);
                }

                lRouteIndex++;

                lMtgProj.Routes.Add(lRouteMTG);
            }

            // Convert the wav copy music files
            Sound.convertWavsToMp3(lOriginMusicFiles, lDestMusicFiles);

            // copy and merge eloc files
            Sound.mergeWavs(lOriginLeftChannelFiles, lOriginRightChannelFiles, lDestMp3Files);

            // create the json project file
            if (File.Exists(aDestFolder + "\\project.json"))
            {
                File.Delete(aDestFolder + "\\project.json");
            }
            string lJson = JsonConvert.SerializeObject(lMtgProj);
            StreamWriter file = new StreamWriter(aDestFolder + "\\project.json");
            file.Write(lJson);
            file.Close();

            // copy the posgps.inf file
            writePosGpsInf(aDestFolder + "\\posgps.inf");
        }

        public void setNumPhisicalChannels(int aNumPhChan)
        {
            mProject.NumPhisicalChannels = aNumPhChan;
        }
        #endregion
    }
}
