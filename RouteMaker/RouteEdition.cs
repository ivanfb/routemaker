﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteMaker
{
    public partial class RouteEdition : Form
    {
        public RouteEdition()
        {
            InitializeComponent();
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtBoxRouteName.Text == "")
            {
                MessageBox.Show("The route name cannot be empty");
                return;
            }

            ProjectMngr.getInstance().addNewRoute(txtBoxRouteName.Text);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void btnNewStop_Click(object sender, EventArgs e)
        {
            if (txtBoxRouteName.Text == "")
            {
                MessageBox.Show("The route name cannot be empty");
                return;
            }

            StopEdition lStopEdit = new StopEdition();
            
            if (lStopEdit.ShowDialog() == DialogResult.OK)
            {
                ProjectMngr.getInstance().addNewStopToRoute(lStopEdit.StopName,
                                                        txtBoxRouteName.Text);
            }
        }
    }
}
