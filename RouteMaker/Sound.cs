﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RouteMaker
{
    class Sound
    {
        private const int MAX_NUM_PROC = 4; // maximum number of simultaneous processes 
        private static Process[] soxPool = new Process[MAX_NUM_PROC];
        private static int mNumRunning = 0, mCurrFileIndex;

        public static void mergeWavs(List<string> aOrigLeft, 
            List<string> aOrigRight, List<string> aDestFiles) 
        {
            if (aOrigLeft.Count != aOrigRight.Count || 
                aOrigLeft.Count != aDestFiles.Count ||
                aOrigRight.Count != aDestFiles.Count)
            {
                throw new Exception(string.Format("Different lists lengths {0} {1} {2}", 
                    aOrigLeft.Count, aOrigRight.Count, aDestFiles.Count));
            }

            mCurrFileIndex = 0;
            int lNumTimesAllStopped = 0; // to control the exit condition

            while (lNumTimesAllStopped < 10 &&
                mCurrFileIndex < aDestFiles.Count)
            {
                if (mNumRunning < MAX_NUM_PROC)
                {
                    if (!File.Exists(aDestFiles[mCurrFileIndex]))
                    {
                        string lArguments = " -M \"" + 
                            aOrigLeft[mCurrFileIndex] + "\" \"" + 
                            aOrigRight[mCurrFileIndex] + "\" -r 44100 " + 
                            aDestFiles[mCurrFileIndex];

                        runSox(lArguments);
                    }

                    lNumTimesAllStopped = 0;
                    mCurrFileIndex++;
                }
                else
                {
                    Thread.Sleep(300);

                    if (soxPool.All(sox => sox.HasExited))
                    { // Start measuring the time all process has end.
                        // After a while all processes had ended we'll 
                        // consider that the job is done and exit the loop
                        lNumTimesAllStopped++;
                    }
                }
            }
        }

        public static void convertWavsToMp3(List<string> aOrigWavs,
            List<string> aDestMp3)
        {
            if (aOrigWavs.Count != aDestMp3.Count)
            {
                throw new Exception(string.Format("Different lists lengths {0} {1} in Wav to Mp3 conversion",
                  aOrigWavs.Count, aDestMp3.Count));
            }

            mCurrFileIndex = 0;
            int lNumTimesAllStopped = 0; // to control the exit condition

            while (lNumTimesAllStopped < 10 &&
                mCurrFileIndex < aDestMp3.Count)
            {
                if (mNumRunning < MAX_NUM_PROC)
                {
                    if (!File.Exists(aDestMp3[mCurrFileIndex]))
                    {
                        string lArguments = "\"" +
                            aOrigWavs[mCurrFileIndex] + "\" -r 44100 -c 2 -b 16 \"" +
                            aDestMp3[mCurrFileIndex] + "\"";

                        runSox(lArguments);
                    }

                    lNumTimesAllStopped = 0;
                    mCurrFileIndex++;
                }
                else
                {
                    Thread.Sleep(300);

                    if (soxPool.All(sox => sox.HasExited))
                    { // Start measuring the time all process has end.
                        // After a while all processes had ended we'll 
                        // consider that the job is done and exit the loop
                        lNumTimesAllStopped++;
                    }
                }
            }
        }

        /// <summary>
        /// Combines two mono wav files into one stereo mp3 file. It calls 'sox' external program to
        /// perform the mix.
        /// 
        /// Example of command to combine two wav files into 
        ///  
        /// ./sox.exe -M -c 1 business.wav -c 1 play_it.wav out.mp3
        ///
        /// </summary>       
        private static void runSox(String aArguments)
        {
            Process p = null;
            for (int i = 0; i < MAX_NUM_PROC; i++)
            {
                if (soxPool[i] == null)
                {
                    soxPool[i] = new Process();
                    soxPool[i].Exited += new EventHandler(soxProc_Exited);
                    soxPool[i].StartInfo.FileName = @"sox-14.4.2\sox.exe";
                    soxPool[i].StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    soxPool[i].EnableRaisingEvents = true;
                    soxPool[i].StartInfo.Arguments = aArguments;
                    soxPool[i].Start();

                    mNumRunning++;
                }

                // This check can throw an exception if the process wasn't started when checking
                if (soxPool[i].HasExited) 
                {
                    soxPool[i].StartInfo.Arguments = aArguments;
                    soxPool[i].Start();

                    mNumRunning++;
                }
            }
        }
        
        private static void soxProc_Exited(object sender, EventArgs e)
        {
            mNumRunning--;
        }
    }
}
