﻿namespace RouteMaker
{
    partial class StopEdition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtBoxStopName = new System.Windows.Forms.TextBox();
            this.groupBoxMusic = new System.Windows.Forms.GroupBox();
            this.btnAddMusic = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.labelStopName = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxMusic.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(250, 332);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(331, 332);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtBoxStopName
            // 
            this.txtBoxStopName.Location = new System.Drawing.Point(83, 10);
            this.txtBoxStopName.Name = "txtBoxStopName";
            this.txtBoxStopName.Size = new System.Drawing.Size(323, 20);
            this.txtBoxStopName.TabIndex = 2;
            // 
            // groupBoxMusic
            // 
            this.groupBoxMusic.Controls.Add(this.btnAddMusic);
            this.groupBoxMusic.Controls.Add(this.listView1);
            this.groupBoxMusic.Location = new System.Drawing.Point(16, 36);
            this.groupBoxMusic.Name = "groupBoxMusic";
            this.groupBoxMusic.Size = new System.Drawing.Size(390, 290);
            this.groupBoxMusic.TabIndex = 3;
            this.groupBoxMusic.TabStop = false;
            this.groupBoxMusic.Text = "Music";
            // 
            // btnAddMusic
            // 
            this.btnAddMusic.Location = new System.Drawing.Point(309, 254);
            this.btnAddMusic.Name = "btnAddMusic";
            this.btnAddMusic.Size = new System.Drawing.Size(75, 23);
            this.btnAddMusic.TabIndex = 1;
            this.btnAddMusic.Text = "+";
            this.btnAddMusic.UseVisualStyleBackColor = true;
            this.btnAddMusic.Click += new System.EventHandler(this.btnAddMusic_Click);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(6, 19);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(378, 229);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // labelStopName
            // 
            this.labelStopName.AutoSize = true;
            this.labelStopName.Location = new System.Drawing.Point(13, 13);
            this.labelStopName.Name = "labelStopName";
            this.labelStopName.Size = new System.Drawing.Size(64, 13);
            this.labelStopName.TabIndex = 4;
            this.labelStopName.Text = "Stop name: ";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog";
            // 
            // StopEdition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 366);
            this.Controls.Add(this.labelStopName);
            this.Controls.Add(this.groupBoxMusic);
            this.Controls.Add(this.txtBoxStopName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Name = "StopEdition";
            this.Text = "StopEdition";
            this.groupBoxMusic.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtBoxStopName;
        private System.Windows.Forms.GroupBox groupBoxMusic;
        private System.Windows.Forms.Label labelStopName;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnAddMusic;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}