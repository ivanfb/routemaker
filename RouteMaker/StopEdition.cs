﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteMaker
{
    public partial class StopEdition : Form
    {
        public String StopName;

        public StopEdition()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtBoxStopName.Text == "")
            {
                MessageBox.Show("The stop name cannot be empty");
                return;
            }

            StopName = txtBoxStopName.Text;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void btnAddMusic_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = ".WAV,.txt | music files";
            openFileDialog.ShowDialog();
        }
    }
}
