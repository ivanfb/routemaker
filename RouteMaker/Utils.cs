﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteMaker
{
    static class Utils
    {
        public static void CopyFolder(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (DirectoryInfo dir in source.GetDirectories())
                CopyFolder(dir, target.CreateSubdirectory(dir.Name));
            foreach (FileInfo file in source.GetFiles())
                File.Copy(Path.Combine(source.FullName, file.Name),
                          Path.Combine(target.FullName, file.Name),
                          true /*override*/);                
        }

        public static void CopyAll_INF_FilesInFolder(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (FileInfo file in source.GetFiles())
            {
                if (file.Extension == ".INF")
                {
                    File.Copy(Path.Combine(source.FullName, file.Name),
                              Path.Combine(target.FullName, file.Name), 
                              true /*override*/);                    
                }
            }
        }
    }
}
